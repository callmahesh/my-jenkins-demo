# Multi-stage
# 1) Node image for building frontend assets
# 2) nginx stage to serve frontend assets

# pull the official base node image
FROM node:16.16.0-alpine as build

# set the working dir for container
WORKDIR /app

# copy the package json file first
COPY package.json ./
COPY package-lock.json ./

# install app dependencies
RUN npm i --silent

# copy other project files from current directory to working dir in image
COPY . ./

# Get build argument and set environment variable
# ARG NODE_ENV=staging
# ENV REACT_APP_DEBUG=localhost

# build the folder
RUN npm run build

# pull the official nginx image
FROM nginx:stable-alpine

# Copy static assets from builder stage
COPY --from=build /app/build /usr/share/nginx/html

RUN rm /etc/nginx/conf.d/default.conf
COPY --from=build /app/nginx/default.conf /etc/nginx/conf.d/

EXPOSE 80

# Containers run nginx with global directives and daemon off
CMD ["nginx", "-g", "daemon off;"]