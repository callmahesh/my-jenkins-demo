import './App.css';
import { BrowserRouter, Routes, Route, useLocation, useNavigate } from "react-router-dom";
import SimpleLogin from './components/login/SimpleLogin';
import VisitDetails from './components/visit/VisitDetails';
import EcgReportLayout from './layout/EcgReportLayout';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import MyProfileLayout from './layout/MyProfileLayout';
import SelectTableByRole from './components/table/SelectTableByRole';
import { ModalContextProvider } from "./utils/utilsOfModal/ModalContext";
import ModalManager from "./utils/utilsOfModal/ModalManager";
import ChangePassword from './components/resetpassword/ChangePassword';
import ForgotPassword from './components/resetpassword/ForgotPassword';
import EcrfLogin from './components/login/EcrfLogin'

function App() {

  let jwt_token = sessionStorage.getItem('eCRF_token');
  let user_id = sessionStorage.getItem('eCRF_userId');

  /* Redux Action Dispatch Hooks */
  const dispatch = useDispatch();

  /* Redux State Selector Hooks */
  const userDetails = useSelector((state) => state.employee.myProfile.value);

  // console.log('APP -> ', jwt_token, ', uID: ', user_id, ', uDet: ', userDetails);
  //  console.log('APP ->2 ', (userDetails.id === user_id) )

  // React.useEffect(()=>{
  //   if(user_id && (userDetails.id !== user_id)){
  //     dispatch(getMyProfile(2,jwt_token));
  //   }   
  // },[])
  return (
    <ModalContextProvider>
      <ModalManager />
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <BrowserRouter basename={process.env.REACT_APP_ROUTER_BASE || '/'} >

        <div className="impact-main-app-content">
          <Routes>

            {/* <Route path="home" element={ <BasicTable /> } /> */}
            <Route path="home" element={<SelectTableByRole />} />
            {/* <Route path="patient" element={ <BasicTable />} /> */}
            <Route path="login" element={<EcrfLogin />} />
            <Route path="loginNew" element={<SimpleLogin />} />
            <Route path="visit/:visitId/:uName" element={<VisitDetails />} />
            {/* <Route path="button" element={ <EcrfHeader />} /> */}
            <Route path="report/:visitId/:patId/:uName" element={<EcgReportLayout />} />
            {/* <Route path="header" element={ <PatientDetails />} /> */}
            <Route path="profile" element={<MyProfileLayout />} />

            <Route path="auth/resetPassword/:resetToken" element={<ForgotPassword />} />

            <Route path="forgotPassword/:resetToken" element={<ChangePassword />} />

            <Route path="*" element={<EcrfLogin />} />

          </Routes>
          {/* <ImpactFooter /> */}
        </div>
      </BrowserRouter>
    </div>
    </ModalContextProvider>
  );
}

export default App;