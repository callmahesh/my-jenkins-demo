import React from 'react'
import { createTheme } from '@mui/material/styles';
// import 'typeface-roboto'
import Typography from '@mui/material/Typography';
import { useParams } from 'react-router-dom';

import FormPasswordReset from './FormPasswordReset'
import './changePassword.scss'

const theme = createTheme({
  palette: {
    type: 'dark',
  },
})



const ForgotPassword = (props) => {
  const { resetToken } = useParams();


  return (
    <div style={{marginTop:'2em'}} className="lean-reset-password-root">
      <Typography variant="title" className='title'>
        Password Reset
      </Typography>
      
      <FormPasswordReset userEmail={props.userEmail} resetToken={resetToken} />
    </div>
   

  );
}

export default ForgotPassword;