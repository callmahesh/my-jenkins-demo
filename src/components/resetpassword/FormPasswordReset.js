import React, { Component } from 'react'
import { Formik } from 'formik'
import { object, ref, string } from 'yup'
import Input from '@mui/material/Input'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'
import FormHelperText from '@mui/material/FormHelperText'
import Button from '@mui/material/Button'
import Paper from '@mui/material/Paper'
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';

import Spinner from './Spinner'
import Alert from './Alert'
import { useSelector , useDispatch } from 'react-redux';
import { resetPassword } from '../../redux/apiCall/authAPIs';

import { useNavigate } from 'react-router-dom';
// import { URL_POST_RESET_PASSWORD, } from "../WM/config";


// export default class FormPasswordReset extends Component {
const FormPasswordReset = (props) => {
  let URL_POST_RESET_PASSWORD=''

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const isSuccess = useSelector((state) => state.auth.resetPassword.isSuccess);

  let [PassChangeSuccess, setPassChangeSuccess] = React.useState(false)
  let [ShowOldPwd, setShowOldPwd] = React.useState(false)
  let [ShowNewPwd, setShowNewPwd] = React.useState(false)
  let [ALertTextColor, setALertTextColor] = React.useState(false)
  let [AlertMessage, setAlertMessage] = React.useState('Your password was changed successfully');

  const handleClickShowOldPwd = () => {
    setShowOldPwd(!ShowOldPwd)
  };
  const handleClickShowNewPwd = () => {
    setShowNewPwd(!ShowNewPwd)
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };


  //----- Destructure Props -----
  // let { resetPwdAction, resetPwd } = props

  // let { isLoading: isLoadingResetPwd, data: ResetPwdData, pagination, error: errorResetPwd, isError: isErrorResetPwd } = resetPwd

  const _handleModalClose = () => {
    // this.setState(() => ({
    //   passChangeSuccess: false,
    // }))
    setPassChangeSuccess(false);
    // console.log('isSuccess',isSuccess);
    // if(isSuccess){
    //   // window.location='/login';
    //   navigate('/login');
    // }
  }

  const _renderModal = () => {
    const onClick = () => {
      // this.setState(() => ({ passChangeSuccess: false }))
      setPassChangeSuccess(false)
      // console.log('isSuccess',isSuccess);
      if (isSuccess) {
        // window.location='/login';
        navigate('/login');
      }
    }
    return (
      <Alert
        // isOpen={this.state.passChangeSuccess}
        // onClose={this._handleClose}
        isOpen={PassChangeSuccess}
        onClose={_handleModalClose}
        handleSubmit={onClick}
        title="Password Reset"
        text={AlertMessage}
        submitButtonText="Close"
        textColor={ALertTextColor}
      />
    )
  }

  const _handleSubmit = ({
    currentPass,
    newPass,
    confirmPass,
    setSubmitting,
    resetForm,
  }) => {
    // console.log('Submit-->0', currentPass, newPass, confirmPass, props.userEmail)
    // console.log('Submit-->1', { "id": props.userEmail, "currentPassword": currentPass, "newPassword": newPass })

    let reqObj = {"confirmPassword": newPass, "password": confirmPass};

    dispatch(resetPassword(reqObj, props.resetToken, setSubmitting, setAlertMessage, setPassChangeSuccess, resetForm));

  }

  return (
    <Formik
      initialValues={{ currentPass: '', newPass: '', confirmPass: '' }}

      validationSchema={object().shape({
        // currentPass: string().required('Current password is required'),
        newPass: string().min(8, 'Please Enter at least 8 characters')
                        .max(16, 'Please Enter at most 16 characters')
                        .required('New password is required')
                        .matches(
                          /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/,
                          "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
                        ),
        confirmPass: string()
          .oneOf([ref('newPass')], 'Passwords do not match')
          .required('Password is required'),
      })}

      onSubmit={({ currentPass, newPass, confirmPass }, { setSubmitting, resetForm }) => {
        // console.log('onSubmit->', currentPass, newPass)
        _handleSubmit({ currentPass, newPass, confirmPass, setSubmitting, resetForm })
      }
      }

      render={props => {
        const { values, touched, errors, handleChange, handleBlur, handleSubmit, isValid, isSubmitting } = props
        return isSubmitting ? (
          <Spinner />
        ) : (
            <Paper className="lean-reset-password-form lean-reset-password-form--wrapper" elevation={10}>
              <form className="lean-reset-password-form" onSubmit={handleSubmit}>
                <span style={{padding:'1em'}} />
                <FormControl
                  fullWidth
                  margin="dense"
                  error={Boolean(touched.newPass && errors.newPass)}
                >
                  <InputLabel
                    htmlFor="password-new"
                    error={Boolean(touched.newPass && errors.newPass)}
                  >
                    {'New Password'}
                  </InputLabel>
                  <Input
                    id="password-new"
                    name="newPass"
                    // type="password"
                    type={ShowNewPwd ? 'text' : 'password'}
                    value={values.newPass}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={Boolean(touched.newPass && errors.newPass)}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowNewPwd}
                          // onMouseDown={handleMouseDownNewPwd}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {ShowNewPwd ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  <FormHelperText
                    error={Boolean(touched.newPass && errors.newPass)}
                  >
                    {touched.newPass && errors.newPass ? errors.newPass : ''}
                  </FormHelperText>
                </FormControl>
                <FormControl
                  fullWidth
                  margin="dense"
                  error={Boolean(touched.confirmPass && errors.confirmPass)}
                >
                  <InputLabel
                    htmlFor="password-confirm"
                    error={Boolean(touched.confirmPass && errors.confirmPass)}
                  >
                    {'Confirm Password'}
                  </InputLabel>
                  <Input
                    id="password-confirm"
                    name="confirmPass"
                    type="password"
                    value={values.confirmPass}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={Boolean(touched.confirmPass && errors.confirmPass)}
                  />
                  <FormHelperText
                    error={Boolean(touched.confirmPass && errors.confirmPass)}
                  >
                    {touched.confirmPass && errors.confirmPass
                      ? errors.confirmPass
                      : ''}
                  </FormHelperText>
                </FormControl>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={Boolean(!isValid || isSubmitting)}
                  style={{ margin: '16px' }}
                  className='lean-btn-primary lean-login-button'
                >
                  {'Reset'}
                </Button>
              </form>
              {_renderModal()}
            </Paper>
          )
      }}
    />
  )

}

// const mapStateToProps = state => {
//   let { resetPwd } = state;
//   return { resetPwd };
// };

// export default connect(mapStateToProps, { resetPwdAction})(FormPasswordReset)
export default (FormPasswordReset)
