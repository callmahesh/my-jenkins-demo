import React from 'react'
import { createTheme } from '@mui/material/styles';
// import 'typeface-roboto'
import Typography from '@mui/material/Typography';
import { useParams } from 'react-router-dom';

import FormPasswordReset from './FormPasswordReset'
import './changePassword.scss'
import EcrfHeader from '../header/EcrfHeader';
import { margin } from '@mui/system';

const theme = createTheme({
  palette: {
    type: 'dark',
  },
})



const ChangePassword = (props) => {
  const { resetToken } = useParams();


  return (
    <div className="lean-reset-password-root">
      <EcrfHeader />
      <Typography style={{marginTop:'5em'}} variant="title" className='title'>
        Password Reset
      </Typography>
      <FormPasswordReset userEmail={props.userEmail} resetToken={resetToken} />
    </div>

  );
}

export default ChangePassword;