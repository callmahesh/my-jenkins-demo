import React, { useContext } from 'react';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
// import MoreVertIcon from '@mui/icons-material/MoreVert';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import pi from '../../assets/images/pi.svg';
import rpi from '../../assets/images/regionalpi.svg';
import inv from '../../assets/images/investigator.svg';
import coinv from '../../assets/images/coinv.svg';
// import pdficon from '../../assets/images/pdfIcon.svg';
import './CommentCard.scss'
import { ModalContext } from '../../utils/utilsOfModal/ModalContext';
import MorePopover from './MorePopover';



export default function CommentsCard(props) {
    const [expanded, setExpanded] = React.useState(false);
    const [open, setOpen] = React.useState(false);

    const { setCurrentModal } = useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });    

    let { displayMore, visitId : rxdVisitId, resetComment, commentShared } = props;
    let { userName, role:userRole, reviewComment, ek12Determination, ek12Diagnosis } = props.data;

    let img;

    if(userRole && userRole === 'REGIONAL_INVESTIGATOR'){
        img = rpi;
    }
    else if(userRole && userRole === 'PRINCIPAL_INVESTIGATOR'){
        img = pi;
    }
    else if(userRole && userRole === 'CO_INVESTIGATOR'){
        img = coinv;
    }
    else if(userRole && userRole === 'INVESTIGATOR'){
        img = inv;
    }

    const handleMoreIconClick = () => {
        let title = "Edit Comment", content = reviewComment, visitId = rxdVisitId, role=userRole;
        openModal({ name: 'EditCommentDialog', props: { title, content, visitId, role, resetComment, ek12Determination, ek12Diagnosis } });
    };

    const handleSendMailClick = () => {
        let title = "Share Comment", content = reviewComment, visitId = rxdVisitId, role=userRole;
        openModal({ name: 'SendMailDialog', props: { title, content, visitId, role, resetComment } });
    };

    // const handleClose = () => {
    //     setOpen(false);
    // };

    // const handleMoreIconClick = () => {
    //     setOpen(true);
    // };

    console.log('ek12Determination: ',ek12Determination)

    return (
        <div className='comment-card-main'>

            <Card sx={{ width: '100%' }}>
                <CardHeader
                    avatar={
                        <Avatar src={img} sx={{ bgcolor: 'transparent' }} aria-label="recipe">
                        </Avatar>
                    }
                    action={
                        displayMore ?
                            // <IconButton aria-label="settings">
                            //     <MoreHorizIcon onClick={handleMoreIconClick} />
                            // </IconButton>
                            <MorePopover handleEditComment={handleMoreIconClick} handleSendMail={handleSendMailClick}  commentShared={commentShared}  />
                            : null
                    }
                    title={userName ? userName : ''}
                    subheader={ userRole ?
                        userRole === 'INVESTIGATOR' ? 'Investigator' : '' ||
                        userRole === 'CO_INVESTIGATOR' ? 'Co-Principal Investigator' : '' ||
                        userRole === 'REGIONAL_INVESTIGATOR' ? 'Regional Principal Investigator' : '' ||
                        userRole === 'PRINCIPAL_INVESTIGATOR' ? 'Principal Investigator' : ''  : '' }
                />
                <CardContent>
                    <Typography variant="body2" >
                    {ek12Diagnosis ?  <span><span style={{color:'#1e85b2', fontFamily: 'sans-serif'}} >Kardia Analysis: </span>{Array.isArray(ek12Diagnosis) ? ek12Diagnosis.map(({...x}) => {return x.name}).join("_"): [ek12Diagnosis].map(({...x}) => {return x.name}).join("_") }</span> : ""}
                    </Typography>
                    <Typography variant="body2" >
                    {ek12Determination ?  <span><span style={{color:'#1e85b2', fontFamily: 'sans-serif'}} >EK12 Expanded Analysis: </span>{ Array.isArray(ek12Determination) ?  ek12Determination.map(({...x}) => {return x.name}).join("_") : [ek12Determination].map(({...x}) => {return x.name}).join("_")}</span> : ""}
                    </Typography>
                    <Typography variant="body2" >
                    {reviewComment ?  <span><span style={{color:'#1e85b2', fontFamily: 'sans-serif'}} >Comment: </span>{reviewComment}</span> : ""}
                    </Typography>
                </CardContent>

            </Card>
        </div>
    );
}