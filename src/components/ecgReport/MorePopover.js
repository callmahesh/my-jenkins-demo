import * as React from 'react';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
// import DeleteIcon from '@mui/icons-material/Delete';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

export default function MorePopover(props) {

  let user_details = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (itemClicked) => {
    // console.log('handleClose-> ', itemClicked);
    setAnchorEl(null);
    if (itemClicked === 'edit') {
      props.handleEditComment();
    } else if (itemClicked === 'share') {
      props.handleSendMail();
    }
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  // console.log(props.commentShared);

  return (
    <div>

      <IconButton aria-label="settings" onClick={handleClick}>
        <MoreHorizIcon  />
      </IconButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          {
            props.commentShared === null || props.commentShared === false ?
              <MenuItem onClick={() => handleClose('edit')}>Edit</MenuItem>
              :
              ''
          }
          {
            user_details.roles[0] === 'INVESTIGATOR' ?
              ''
              :
              <MenuItem onClick={() => handleClose('share')}>Share</MenuItem>
          }

        </Menu>
      </Popover>
    </div>
  );
}
