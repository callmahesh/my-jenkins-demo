import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import CloseIcon from '@mui/icons-material/Close';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import { Divider } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import { addComment } from '../../redux/apiCall/commentsAPIs';
import { reset_UpdateComment, error_UpdateComment } from '../../redux/reducers/commentsSlice';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { MenuItem, Grid } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';


export default function EditCommentDialog({ closeModal, ...props }) {

    let { title, content, visitId, role, resetComment, ek12Determination, ek12Diagnosis } = props.props

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    const updateCommentSel = useSelector((state) => state.comment.updateComment);
    const eK12DetMaster = useSelector((state) => state.comment.getEK12DetMaster.value);
    const ek12DiagMaster = useSelector((state) => state.comment.getEK12DiagMaster.value);

    const [editedEk12Diagnosis, setEditEk12Diagnosis] = React.useState([]);
    const [editedEk12Determination, setEditEk12Determination] = React.useState([]);

    const [ek12DiagnosisList, setEk12DiagnosisList] = React.useState([]);
    const [ek12DeterminationList, setEk12DeterminationList] = React.useState([]);

    const [comment, setComment] = React.useState(content);

    let jwt_token = sessionStorage.getItem('eCRF_token');

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    React.useEffect(() => {
        if (eK12DetMaster && Object.keys(eK12DetMaster).length > 0) {
            setEk12DeterminationList(eK12DetMaster);
            if (ek12Determination != null || ek12Determination != undefined) {
                // setEditEk12Determination(ek12Determination.id);                
                setEditEk12Determination(Array.isArray(ek12Determination) ? ek12Determination : [ek12Determination]);
            }
            // else{
            //     setEditEk12Determination(0);
            // }
        }
    }, [eK12DetMaster])

    React.useEffect(() => {
        if (ek12DiagMaster && Object.keys(ek12DiagMaster).length > 0) {
            setEk12DiagnosisList(ek12DiagMaster);
            if (ek12Diagnosis != null || ek12Diagnosis != undefined) {
                // setEditEk12Diagnosis(ek12Diagnosis.id);
                setEditEk12Diagnosis(Array.isArray(ek12Diagnosis) ? ek12Diagnosis : [ek12Diagnosis]);
            }
            // else{
            //     setEditEk12Diagnosis(0);
            // } 
        }
    }, [ek12DiagMaster])

    const handleClose = () => {
        closeModal();
        dispatch(reset_UpdateComment());
        resetComment();
    };

    /* Handler for writing comment */
    const handleSendComment = () => {

        if (visitId === '') {
            dispatch(error_UpdateComment('Visit Id is empty!'));
        } else {
            let reqObj = {
                "role": role,
                "userName": "string"
            }
            if (comment != null && comment != undefined && comment != '') {
                Object.assign(reqObj,
                    {
                        "reviewComment": comment,
                    })
            }
            if (editedEk12Determination != 0) {
                Object.assign(reqObj,
                    {
                        // "ek12Determination": {
                        "ek12ExpandedAnalysisData": {
                            "id": editedEk12Determination.map(({ ...x }) => { return x.id }).join(",")
                        }
                    })
            } else {
                dispatch(error_UpdateComment('Selecting EK12 Expanded Analysis Mandatory!'));
                setTimeout(function () {
                    dispatch(reset_UpdateComment());
                }, 3000);
                return;
            }
            if (editedEk12Diagnosis != 0) {
                Object.assign(reqObj,
                    {
                        // "ek12Diagnosis": {
                        "kardiaAnalysisData": {
                            "id": editedEk12Diagnosis.map(({ ...x }) => { return x.id }).join(",")
                        }
                    })
            } else {
                dispatch(error_UpdateComment('Selecting Kardia Analysis Mandatory!'));
                setTimeout(function () {
                    dispatch(reset_UpdateComment());
                }, 3000);
                return;
            }
            // console.log('handleSendComment--> ', reqObj, jwt_token);
            dispatch(addComment(reqObj, visitId, jwt_token, true));
        }
    };

    // const handleEk12DiagnosisChange = (e) => {
    //     setEditEk12Diagnosis(e.target.value);

    // };

    // const handleEk12DeterminationChange = (e) => {
    //     setEditEk12Determination(e.target.value);

    // };

    const DropDowns = () => {

        return <Grid item xs={12} sx={{ paddingLeft: 0, marginTop: 2, marginLeft: 0.3, marginRight: 0.3, marginBottom: 1 }}>
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {/* <TextField select label="Kardia Analysis"
                    style={{ width: '100%' }}
                    SelectProps={{
                        value: editedEk12Diagnosis,
                        onChange: handleEk12DiagnosisChange
                    }}
                >
                    {
                        ek12DiagnosisList.map((option) => (
                            <MenuItem key={option.name} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))
                    }
                </TextField>
                <TextField select label="EK12 Expanded Analysis"
                    style={{ width: '100%', marginTop: 13 }}
                    SelectProps={{
                        value: editedEk12Determination,
                        onChange: handleEk12DeterminationChange
                    }}
                >
                    {
                        ek12DeterminationList.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))
                    }
                </TextField> */}
                <Autocomplete
                    multiple
                    disablePortal
                    id="combo-box-demo"
                    options={ek12DiagnosisList}
                    style={{ width: '100%' }}
                    value={editedEk12Diagnosis}
                    renderInput={(params) => <TextField {...params} label="Kardia Analysis" placeholder='Search' />}
                    getOptionLabel={option => option.name}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    onChange={(_event, newTeam) => {
                        // console.log("Autocomplete-> ", newTeam);
                        setEditEk12Diagnosis(newTeam);
                    }}
                />
                <Autocomplete
                    multiple
                    disablePortal
                    id="combo-box-demo"
                    options={ek12DeterminationList}
                    style={{ width: '100%', marginTop: 13 }}
                    value={editedEk12Determination}
                    renderInput={(params) => <TextField {...params} label="EK12 Expanded Analysis" placeholder='Search' />}
                    getOptionLabel={option => option.name}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    onChange={(_event, newTeam) => {
                        // console.log("Autocomplete-> ", newTeam);
                        setEditEk12Determination(newTeam);
                    }}
                />
            </div>
        </Grid>
    }

    return (
        <div>
            {/* <Button variant="outlined" onClick={handleClickOpen}>
        Open responsive dialog
      </Button> */}
            <Dialog
                // fullScreen={fullScreen}
                open={true}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title">
                    {title}
                </DialogTitle>
                <Divider />
                <DialogContent>
                    {
                        updateCommentSel.isStarted ?
                            // true?
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                <CircularProgress size={30} />
                                <p>Updating comment...</p>
                            </Box>
                            :
                            updateCommentSel.isError ?
                                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                    {/* <CircularProgress size={30} /> */}
                                    <CloseIcon sx={{ fontSize: 60, color: 'red' }} />
                                    <p style={{ color: '#2b2b2b' }} >{updateCommentSel.error}</p>
                                    {/* <Button onClick={handleOpenForm} variant='contained'>Retry</Button> */}
                                </Box>
                                : updateCommentSel.value !== '' ?
                                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                        {/* <CircularProgress size={30} /> */}
                                        <CheckCircleOutlineIcon sx={{ fontSize: 60, color: 'green' }} />
                                        <h4 style={{ color: '#2b2b2b' }}>{updateCommentSel.value}</h4>
                                    </Box>
                                    :
                                    <>
                                        <DropDowns />
                                        <TextField
                                            id="standard-read-only-input"
                                            // label='Comment'
                                            multiline
                                            // defaultValue={comment}
                                            style={{ marginTop: 10 }}
                                            value={comment}
                                            InputLabelProps={{ shrink: comment ? true : false }}
                                            onChange={event => setComment(event.target.value)}
                                            InputProps={{
                                                // readOnly: true,
                                            }}
                                            fullWidth
                                        />
                                    </>
                    }



                </DialogContent>
                <DialogActions>
                    {
                        updateCommentSel.isStarted || updateCommentSel.isSuccess ?
                            ''
                            :
                            <Button onClick={handleSendComment} autoFocus>
                                Save
                            </Button>
                    }
                    <Button autoFocus onClick={handleClose}>
                        Close
                    </Button>

                </DialogActions>
            </Dialog>
        </div>
    );
}