import * as React from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getOwnComment, addComment, getAllComment, getEK12DeterminationMaster, getEK12DiagnosisMaster } from '../../redux/apiCall/commentsAPIs';
import { reset_PostComment, reset_GetOwnComment, reset_GetAllComment, error_PostComment } from '../../redux/reducers/commentsSlice';
import { set_CommentReload } from '../../redux/reducers/employeeSlice';
import { GET_PATIENT_ECG_PDF_URL } from '../../config/urlConfig';
import { Grid, TextField, Typography } from '@mui/material';
import ImpactSnackBar from '../snackbar/ImpactSnackBar';
import LoadingButton from '@mui/lab/LoadingButton';
import SaveIcon from '@mui/icons-material/Save';
import './EcgReport.scss';
import CommentsCard from './CommentsCard';
import { useNavigate } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import MenuItem from '@mui/material/MenuItem';
import Autocomplete from '@mui/material/Autocomplete';

const EcgReport = (props) => {

    const { visitId, patId, uName } = useParams();

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();
    const navigate = useNavigate();

    /* Redux State Selector Hooks */
    const selfComment = useSelector((state) => state.comment.ownComment.value);
    const getAllVisitComment = useSelector((state) => state.comment.getAllComment.value);
    const addCommentSel = useSelector((state) => state.comment.addComment);
    const lastVisitStatus = useSelector((state) => state.patient.patientDetails.value.lastVisitStatus);
    const eK12DetMaster = useSelector((state) => state.comment.getEK12DetMaster.value);
    const ek12DiagMaster = useSelector((state) => state.comment.getEK12DiagMaster.value);
    let isCommentEnabled = lastVisitStatus != 'NOACTION';

    const [comment, setComment] = React.useState('');
    const [pdfFile, setPdfFile] = React.useState('');
    const [mobileScreen, setMobileScreen] = React.useState('');

    const [ek12Diagnosis, setEk12Diagnosis] = React.useState([]);
    const [ek12Determination, setEk12Determination] = React.useState([]);

    const [ek12DiagnosisList, setEk12DiagnosisList] = React.useState([]);
    const [ek12DeterminationList, setEk12DeterminationList] = React.useState([]);

    let jwt_token = sessionStorage.getItem('eCRF_token');


    // console.log('EcgReport-> ', getAllVisitComment);

    let user_details = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));

    const isMobile = useMediaQuery({ query: `(max-width: 760px)` });

    /* Get Self Comment if posted by logged in user & ecg pdf report */
    React.useEffect(() => {
        if (visitId && jwt_token) {
            dispatch(getOwnComment(visitId, jwt_token));

            dispatch(getAllComment(visitId, jwt_token));

            setMobileScreen(isMobile);

            if (patId && visitId) {
                let newURL = GET_PATIENT_ECG_PDF_URL.replace("{patientId}", patId);
                newURL = newURL.replace("{visitId}", visitId);
                var apiUrl = new URL(newURL);
                fetch(apiUrl, {
                    method: "GET",
                    // responseType: "blob",
                    headers: {
                        'Authorization': `Bearer ${jwt_token}`, // notice the Bearer before your token
                    },
                }).then(response => {
                    if (response.status === 200) {
                        return response.json();
                    }
                    // return response.blob();
                })
                    .then(data => {
                        setPdfFile(data);

                        // window.open(file);
                    })
                    .catch(error => {
                        //if(error!="TypeError: Failed to execute 'createObjectURL' on 'URL': Overload resolution failed."){
                        console.error("Exception Login API: ", error)
                        alert(`Exception: ${error}`);
                        // }

                    });
            }
            dispatch(getEK12DeterminationMaster(jwt_token));
            dispatch(getEK12DiagnosisMaster(jwt_token));
        }
        return () => {
            // console.log('cleaned up');
            dispatch(reset_GetOwnComment());
            dispatch(reset_GetAllComment());
        };
    }, [])

    /* Store comment in React hook state */
    React.useEffect(() => {
        if (selfComment && Object.keys(selfComment).length > 0) {
            setComment(selfComment.reviewComment);
        }
    }, [selfComment])

    React.useEffect(() => {
        if (eK12DetMaster && Object.keys(eK12DetMaster).length > 0) {
            setEk12DeterminationList(eK12DetMaster);
            // setEk12Determination(0);
        }
    }, [eK12DetMaster])

    React.useEffect(() => {
        if (ek12DiagMaster && Object.keys(ek12DiagMaster).length > 0) {
            setEk12DiagnosisList(ek12DiagMaster);
            // setEk12Diagnosis(0);
        }
    }, [ek12DiagMaster])

    /* Handler for writing comment */
    const handleSendComment = () => {
        console.log('handleSendComment--> ',);
        // if (comment.replace(/ /g, '') === '') {
        //     dispatch(error_PostComment('Comment field cannot be empty!'));
        // } else 
        if (visitId === '') {
            dispatch(error_PostComment('Visit Id is empty!'));
        } else {
            let reqObj = {
                "role": selfComment.role,
                "userName": "string"
            }
            if (comment != null && comment != undefined && comment != '') {
                Object.assign(reqObj,
                    {
                        "reviewComment": comment,
                    })
            }
            if (ek12Determination.length > 0) {
                Object.assign(reqObj,
                    {
                        // "ek12Determination": {
                        "ek12ExpandedAnalysisData": {
                            "id": ek12Determination.map(({ ...x }) => { return x.id }).join(",")
                        }
                    })
            } else {
                dispatch(error_PostComment('Selecting EK12 Expanded Analysis Mandatory!'));
                setTimeout(function () {
                    handleToastClose()
                }, 3000);
                return;
            }
            if (ek12Diagnosis.length > 0) {
                Object.assign(reqObj,
                    {
                        // "ek12Diagnosis": {
                        "kardiaAnalysisData": {
                            "id": ek12Diagnosis.map(({ ...x }) => { return x.id }).join(",")
                        }
                    })
            } else {
                dispatch(error_PostComment('Selecting Kardia Analysis Mandatory!'));
                setTimeout(function () {
                    handleToastClose()
                }, 3000);
                return;
            }
            // console.log('AddComments: ',reqObj);
            dispatch(addComment(reqObj, visitId, jwt_token));
            dispatch(set_CommentReload(true));
        }
    };

    const handleToastClose = () => {
        // setToastData({ open: false, data: { message: '', severity: '' } });
        dispatch(reset_PostComment());
    }
    const resetCommentBlock = () => {
        // setToastData({ open: false, data: { message: '', severity: '' } });
        dispatch(getAllComment(visitId, jwt_token));
    }

    const handleSuccess = () => {
        dispatch(getAllComment(visitId, jwt_token));
        dispatch(getOwnComment(visitId, jwt_token));
        dispatch(reset_PostComment());
    }

    // const handleEk12DiagnosisChange = (e) => {
    //     setEk12Diagnosis(e.target.value);
    // };

    // const handleEk12DeterminationChange = (e) => {
    //     setEk12Determination(e.target.value);
    // };

    const DropDowns = () => {

        return <Grid item xs={12} sx={{ paddingLeft: 0, marginTop: 2, marginLeft: 0.3, marginRight: 0.3, marginBottom: 1 }}>
            <div style={{ display: 'flex' }}>
                {/* <TextField select label="Kardia Analysis"
                    style={{ width: '50%', marginRight: '15px' }}
                    SelectProps={{
                        value: ek12Diagnosis,
                        onChange: handleEk12DiagnosisChange
                    }}
                >
                    {
                        ek12DiagnosisList.map((option) => (
                            <MenuItem key={option.name} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))
                    }
                </TextField> */}
                {/* <TextField select label="EK12 Expanded Analysis"
                    style={{ width: '50%' }}
                    SelectProps={{
                        value: ek12Determination,
                        onChange: handleEk12DeterminationChange
                    }}
                >
                    {
                        ek12DeterminationList.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))
                    }
                </TextField> */}
                <Autocomplete
                    multiple
                    disablePortal
                    id="combo-box-demo"
                    options={ek12DiagnosisList}
                    style={{ width: '50%', marginRight: '15px' }}
                    value={ek12Diagnosis}
                    renderInput={(params) => <TextField {...params} label="Kardia Analysis" placeholder='Search' />}
                    getOptionLabel={option => option.name}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    // isOptionEqualToValue={(option, value) => option.id === value.id}
                    onChange={(_event, newTeam) => {
                        // console.log("Autocomplete-> ", newTeam);
                        setEk12Diagnosis(newTeam);
                    }}
                />
                <Autocomplete
                    multiple
                    disablePortal
                    id="combo-box-demo"
                    options={ek12DeterminationList}
                    style={{ width: '50%' }}
                    value={ek12Determination}
                    renderInput={(params) => <TextField {...params} label="EK12 Expanded Analysis" placeholder='Search' />}
                    getOptionLabel={option => option.name}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    // isOptionEqualToValue={(option, value) => option.id === value.id}
                    onChange={(_event, newTeam) => {
                        // console.log("Autocomplete-> ", newTeam);
                        setEk12Determination(newTeam);
                    }}
                />
            </div>
        </Grid>
    }

    // const SaveCommentTextBox = () => {
    //     return <>
    //         <div style={{ borderStyle: 'solid', borderColor: '#1e85b2', borderRadius: '10px', width: '100%', height: 'flex', padding: 5, margin: '10px 0px 10px 15px' }}>
    //             <DropDowns />
    //             <Grid item xs={12} style={{ paddingLeft: 0, marginTop: 2, marginLeft: 2, marginRight: 2 }} rowSpacing={2}>
    //                 <TextField
    //                     id="standard-read-only-input"
    //                     label='Comment'
    //                     multiline
    //                     style={{ fontFamily: 'sans-serif' }}
    //                     value={comment}
    //                     InputLabelProps={{ shrink: comment ? true : false }}
    //                     onChange={event => setComment(event.target.value)}
    //                     autoFocus
    //                     fullWidth
    //                 />
    //             </Grid>
    //             <div className='save-btn'>
    //                 <LoadingButton onClick={handleSendComment} variant="contained"
    //                     style={{ textTransform: 'none', marginTop: 10, marginLeft: 4, marginBottom: 20, fontFamily: 'sans-serif' }}
    //                     loading={addCommentSel.isStarted}
    //                     startIcon={addCommentSel.isStarted ? <SaveIcon /> : null}
    //                 >
    //                     Save
    //                 </LoadingButton>
    //             </div>
    //         </div>
    //     </>
    // }


    // console.log('ECGREPORT: ',isCommentEnabled, ", OWN: ",selfComment);

    return (
        <div className='ecg-report-main'>
            {/* <Header /> */}
            <Grid container spacing={2} style={{ marginTop: '64px', padding: '0px 20px', marginBottom: 10 }}>

                {pdfFile != null ?
                    <>
                        <Grid item xs={12} >
                            <span style={{ fontFamily: 'sans-serif', fontSize: '14px', color: '#1e85b2', height: 17, width: 89, cursor: 'default' }}>{uName ? uName : 'NA'}  <span style={{ color: '#000' }}>&gt;</span> <span style={{ color: '#b8b8b8' }} >ECG Report</span> </span>
                            <div style={{ marginTop: '25px' }}>
                                <span style={{ fontFamily: 'sans-serif', fontSize: '25px', fontWeight: 600, color: '#4a4a4a', cursor: 'default' }}>ECG Report</span>
                            </div>
                        </Grid>
                        {
                            mobileScreen ?
                                <Grid item xs={12} sx={{ paddingLeft: 0, marginBottom: '50px', marginTop: '50px' }} >
                                    <a href={pdfFile} target="_blank" rel="noreferrer noopener" download><Typography sx={{ fontFamily: 'sans-serif' }}>Download ECG PDF report</Typography></a>
                                </Grid>
                                :
                                <Grid item xs={12} sx={{ paddingLeft: 0, marginBottom: '25px' }} >
                                    <iframe
                                        src={pdfFile}
                                        type="application/pdf"
                                        height="578px"
                                        width="100%"
                                    ></iframe>
                                </Grid>
                        }
                    </>
                    :
                    <Grid item xs={12} >
                        <span style={{ fontFamily: 'sans-serif', fontSize: '14px', color: '#1e85b2', }}>{uName ? uName : 'NA'}</span>
                        <div style={{ marginTop: 50, marginBottom: 200 }}>
                            <span style={{ fontFamily: 'sans-serif', fontSize: '25px', fontWeight: 600, color: '#4a4a4a' }}>No ECG Report for this Patient</span>
                        </div>
                    </Grid>
                }


                <Grid item xs={12} sx={{ paddingLeft: 0 }} >
                    <div>
                        <span style={{ fontFamily: 'sans-serif', fontSize: '17px', color: '#288ec0', cursor: 'default' }}>INTERPRETATION</span>
                    </div>
                </Grid>


                {isCommentEnabled ?
                    selfComment && Object.keys(selfComment).length > 0 && selfComment.ek12Determination && selfComment.ek12Diagnosis ?
                        getAllVisitComment && Array.isArray(getAllVisitComment) && getAllVisitComment.length > 0 ?
                            getAllVisitComment.map((comment, index) => {
                                return (index === 0 && (user_details.roles[0] !== 'INVESTIGATOR' && comment.shared !== null || comment.shared !== false) ?
                                    <div key={'C' + index} style={{ borderStyle: 'solid', borderColor: '#1e85b2', borderRadius: '10px', width: '100%', padding: 5, height: 'flex', margin: '10px 0px 10px 15px' }}>
                                        <Grid item xs={12} style={{ padding: 0, padding: 20 }} >
                                            <CommentsCard displayMore={true} data={comment} visitId={visitId} resetComment={resetCommentBlock} commentShared={comment.shared} />
                                        </Grid>
                                    </div>
                                    :
                                    <div key={'C' + index} style={{ borderStyle: 'solid', borderColor: '#1e85b2', borderRadius: '10px', width: '100%', padding: 25, height: 'flex', margin: '10px 0px 10px 15px' }}>
                                        <Grid item xs={12} style={{ padding: '0px', marginLeft: 5 }} >
                                            <CommentsCard displayMore={false} data={comment} visitId={visitId} resetComment={resetCommentBlock} />
                                        </Grid>
                                    </div>)
                            }
                            )
                            :
                            <div style={{ borderStyle: 'solid', borderColor: '#1e85b2', borderRadius: '10px', width: '100%', height: 'flex', padding: 5, margin: '10px 0px 10px 15px' }}>
                                <DropDowns />
                                <Grid item xs={12} style={{ paddingLeft: 0, marginTop: 2, marginLeft: 2, marginRight: 2 }} rowSpacing={2}>
                                    <TextField
                                        id="standard-read-only-input"
                                        label='Comment'
                                        multiline
                                        style={{ fontFamily: 'sans-serif' }}
                                        value={comment}
                                        InputLabelProps={{ shrink: comment ? true : false }}
                                        onChange={event => setComment(event.target.value)}

                                        fullWidth
                                    />
                                </Grid>
                                <div className='save-btn'>
                                    <LoadingButton onClick={handleSendComment} variant="contained"
                                        style={{ textTransform: 'none', marginTop: 10, marginLeft: 4, marginBottom: 20, fontFamily: 'sans-serif' }}
                                        loading={addCommentSel.isStarted}
                                        startIcon={addCommentSel.isStarted ? <SaveIcon /> : null}
                                    >
                                        Save
                                    </LoadingButton>
                                </div>
                            </div>
                        :
                        <div style={{ borderStyle: 'solid', borderColor: '#1e85b2', borderRadius: '10px', width: '100%', height: 'flex', padding: 5, margin: '10px 0px 10px 15px' }}>
                            <DropDowns />
                            <Grid item xs={12} style={{ paddingLeft: 0, marginTop: 2, marginLeft: 2, marginRight: 2 }} rowSpacing={2}>
                                <TextField
                                    id="standard-read-only-input"
                                    label='Comment'
                                    multiline
                                    style={{ fontFamily: 'sans-serif' }}
                                    value={comment}
                                    InputLabelProps={{ shrink: comment ? true : false }}
                                    onChange={event => setComment(event.target.value)}

                                    fullWidth
                                />
                            </Grid>
                            <div className='save-btn'>
                                <LoadingButton onClick={handleSendComment} variant="contained"
                                    style={{ textTransform: 'none', marginTop: 10, marginLeft: 4, marginBottom: 20, fontFamily: 'sans-serif' }}
                                    loading={addCommentSel.isStarted}
                                    startIcon={addCommentSel.isStarted ? <SaveIcon /> : null}
                                >
                                    Save
                                </LoadingButton>
                            </div>
                        </div>
                    :
                    <Grid item xs={12} sx={{ paddingLeft: 0 }} >
                        <div>
                            <span style={{ fontFamily: "sans-serif" }}>Awaiting Previous Investigator Comments...</span>
                        </div>
                    </Grid>

                }
                <div className='save-btn'>
                    {/* <Grid  item xs={12} sx={{ paddingLeft: 0 }} > */}
                    <LoadingButton variant="contained"
                        style={{ textTransform: 'none', width: 120, height: 40, marginTop: 10 }}
                        onClick={() => navigate('/home')}
                    >
                        Close
                    </LoadingButton>
                </div>
            </Grid>

            {
                addCommentSel.isError ? <ImpactSnackBar isOpen={true} message={addCommentSel.error} handleClose={handleToastClose} severity='error' />
                    : addCommentSel.value !== '' ? <ImpactSnackBar isOpen={true} message={addCommentSel.value} handleClose={handleToastClose} severity='success' />
                        : null
            }

            {
                addCommentSel.value !== '' ? handleSuccess() : null
            }

        </div>
    );
}


export default EcgReport;
