import React, { useContext } from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from "react-router-dom";
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useNavigate } from 'react-router-dom';
import "./Header.scss";
import coinv from '../../assets/images/coinv.svg';
import investigator from '../../assets/images/investigator.svg';
import pi from '../../assets/images/pi.svg';
import regionalpi from '../../assets/images/regionalpi.svg';
import { set_Investigator, set_CoInvestigator, set_RPInvestigator, set_Toggle, set_CurrentManager } from '../../redux/reducers/employeeSlice';
import { ModalContext } from '../../utils/utilsOfModal/ModalContext';


const dropdownList = [{ name: "My Profile", value: '/profile' }];

const Header = (props) => {

  /* Redux Action Dispatch Hooks */
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const [displaySnackBar, setDisplaySnackBar] = React.useState(false);

  const { setCurrentModal } = useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  const handleBackBtnLogout = () => {
    let title = "Confirm Logout", content = '';
    openModal({ name: 'ConfirmDialog', props: { title, content, handleLogout } });
  };
  /* Redux State Selector Hooks */
  // const userDetails = useSelector((state) => state.employee.user_details.value);  

  const userDetails = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));

  // console.log('Header->', userDetails);

  let fullName = '', role = '', img = '';
  if (userDetails && 'fullName' in userDetails) {
    fullName = userDetails.fullName;
    if (userDetails.roles[0] === 'INVESTIGATOR') {
      role = 'Investigator';
      img = investigator;
    } else if (userDetails.roles[0] === 'CO_INVESTIGATOR') {
      role = 'Co-Principal Investigator';
      img = coinv;
    } else if (userDetails.roles[0] === 'REGIONAL_INVESTIGATOR') {
      role = 'Regional Principal Investigator';
      img = regionalpi;
    } else if (userDetails.roles[0] === 'PRINCIPAL_INVESTIGATOR') {
      role = 'Principal Investigator';
      img = pi;
    }

  }

  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleLogout = () => {
    sessionStorage.removeItem('eCRF_token');
    sessionStorage.removeItem('eCRF_userId');
    sessionStorage.removeItem('eCRF_uDetails');
    dispatch(set_CurrentManager(''));
    dispatch(set_Investigator(''));
    dispatch(set_CoInvestigator(''));
    dispatch(set_RPInvestigator(''));
    dispatch(set_Toggle(false));
    navigate('/login')
  };

  const navigateBack = () => {
    let url = new URL(window.location.href).pathname;
    if (url === '/home' || url === '/ecrfWeb/home') {
      handleBackBtnLogout();
    } else {
      navigate(-1);
    }
  }

  return (
    <>
      <div className='ecrf-header'>
        <AppBar >
          <Container maxWidth="xl" >
            <Toolbar disableGutters>
              {/* <ArrowBackIcon onClick={() => navigateBack()}
                style={{ color: '#189dd4', marginRight: '10px', cursor: 'pointer' }}
              /> */}
              <Typography
                variant="h6"
                style={{ color: '#000000', cursor: 'pointer' }}
                noWrap
                component="div"
                sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}
                onClick={() => navigate('/home')}
              >
                {/* <span style={{ fontWeight: 600, marginRight: 6 }}>{`Kardia `}</span> Pro */}
                <span className='Kardia-Pro'>{`India ECG Registry `} <span style={{ fontSize: 14, fontWeight: 200, letterSpacing: 0.2 }}>An Initiative by Eris</span></span>

              </Typography>

              {/* <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
              {pages.map((page) => (
                <Button
                  key={page}
                  onClick={handleCloseNavMenu}
                  sx={{ my: 2, color: 'white', display: 'block' }}
                >
                  {page}
                </Button>
              ))}
            </Box> */}

              <Box sx={{ flexGrow: 0, width: '30%' }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar sx={{ width: 31, height: 31 }} alt="Lokesh" src={img} />
                    <div className='Ben'>
                      <h6 > {fullName} </h6>
                      <h6 className='role'> {role} </h6>
                    </div>
                    <ExpandMoreIcon fontSize="medium" color="primary"></ExpandMoreIcon>
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: '45px' }}
                  id="menu-appbar"
                  anchorEl={anchorElUser}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                  }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  {
                    dropdownList.map((item) => (
                      <MenuItem key={item.name} onClick={handleCloseUserMenu}>
                        <Typography textAlign="center" >
                          <Link to={item.value} style={{ textDecoration: 'none' }}> {item.name}</Link>
                        </Typography>
                      </MenuItem>
                    ))
                  }
                  <MenuItem onClick={handleLogout} style={{ color: 'blue' }}  >Logout</MenuItem>
                </Menu>
              </Box>

            </Toolbar>
          </Container>
        </AppBar>
      </div>
    </>
  );
};
export default Header;
