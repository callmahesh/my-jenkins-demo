import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import './EcrfHeader.scss';
import MenuItem from '@mui/material/MenuItem';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Menu from '@mui/material/Menu';
import coinv from '../../assets/images/coinv.svg';
import investigator from '../../assets/images/investigator.svg';
import pi from '../../assets/images/pi.svg';
import regionalpi from '../../assets/images/regionalpi.svg';
import logo from '../../assets/images/IECG_logo_new.png';
import { set_Investigator, set_CoInvestigator, set_RPInvestigator, set_ToggleInv, set_ToggleOthers, set_CurrentManager } from '../../redux/reducers/employeeSlice';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export default function EcrfHeader() {

  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const dispatch = useDispatch();

  const navigate = useNavigate();


  const userDetails = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));

  let fullName = '', role = '', img = '';
  if (userDetails && 'fullName' in userDetails) {
    fullName = userDetails.fullName;
    if (userDetails.roles[0] === 'INVESTIGATOR') {
      role = 'Investigator';
      img = investigator;
    } else if (userDetails.roles[0] === 'CO_INVESTIGATOR') {
      role = 'Co-Principal Investigator';
      img = coinv;
    } else if (userDetails.roles[0] === 'REGIONAL_INVESTIGATOR') {
      role = 'Regional Principal Investigator';
      img = regionalpi;
    } else if (userDetails.roles[0] === 'PRINCIPAL_INVESTIGATOR') {
      role = 'Principal Investigator';
      img = pi;
    }

  }

  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleLogout = () => {
    sessionStorage.removeItem('eCRF_token');
    sessionStorage.removeItem('eCRF_userId');
    sessionStorage.removeItem('eCRF_uDetails');
    localStorage.removeItem('patientType');
    dispatch(set_CurrentManager(''));
    dispatch(set_Investigator(''));
    dispatch(set_CoInvestigator(''));
    dispatch(set_RPInvestigator(''));
    dispatch(set_ToggleInv(true));
    dispatch(set_ToggleOthers(false));
    navigate('/login')
  };


  return (
    <div className='imp-header-main'>
      <Box sx={{ flexGrow: 1, boxShadow: 3, elevation: 1 }}>
        <AppBar position="fixed">
          <Toolbar>
            <img aria-label="logo" className='headerLogo' src={logo}></img>
            <Typography variant="h6" component="div" sx={{ marginLeft: '0.5%', flexGrow: 1 }}>
              India ECG Registry<span>An initiative by Eris</span>
            </Typography>
            {/* <Button color="inherit">Login Typography Typography</Button> */}
            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
              <CardHeader
                avatar={
                  <Avatar aria-label="recipe" src={img}></Avatar>
                }

                title={fullName}
                subheader={role}
              />
              <ExpandMoreIcon fontSize="medium" color="primary"></ExpandMoreIcon>
            </IconButton>

            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <MenuItem style={{ color: 'blue', paddingRight: '40px' }} onClick={handleLogout} >Logout</MenuItem>
            </Menu>
          </Toolbar>
        </AppBar>
      </Box>
    </div>
  );
}