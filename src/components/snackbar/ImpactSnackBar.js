import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { SNACKBAR_TIMEOUT } from '../../config/constants';

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


const ImpactSnackBar = (props) => {
    // console.log('ImpactSnackBar: ',props)
    let {message, isOpen, handleClose, severity} = props
    

    return (
        <Snackbar open={isOpen} autoHideDuration={SNACKBAR_TIMEOUT} onClose={handleClose} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
            <Alert onClose={handleClose} severity={severity} sx={{ width: '100%' }}>
                {message}
            </Alert>
        </Snackbar>
    );
}

export default ImpactSnackBar;