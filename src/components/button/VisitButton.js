import * as React from 'react';
import "./VisitButton.scss";
// import data from "./fruits.json";
// import Tag from "./Tag";
import { useState, useRef } from "react";
import gsap from "gsap";
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useSelector } from 'react-redux';

export default function VisitButton(props) {

    // console.log('VisitButton->', props.patData);
    let scrl = useRef(null);
    const [scrollX, setscrollX] = useState(0);
    const [scrolEnd, setscrolEnd] = useState(false);
    const [buttnList, setbuttnList] = useState([]);

    const toggleState = useSelector((state) => state.employee.toggle.investigator);

    React.useEffect(() => {
        let tempArray = [];
        // console.log('useEffect-> ', ('patData' in props.patData), (props.patData.length > 0))
        if (props.patData && props.patData.length > 0) {
            tempArray = props.patData.filter(function (patData) {
                return patData.ecgReportAvailable === true || new Date(patData.visitDateTime) < new Date("2022-06-17T18:00:00.000+05:30") ;
            }).map((item, i) => {
                // tempArray = fruitsJSON.map((item, i) => {
                if (i === 0) {
                    return { name: "Visit", value: getDate(item.visitDateTime), status: true, id: item.id, rawData: item }
                } else {
                    return { name: "Visit", value: getDate(item.visitDateTime), status: false, id: item.id, rawData: item }
                }

            });
        }
        // console.log('buttnList->', tempArray)
        setbuttnList(tempArray);
    }, [])

    function getDate(newDate) {
        var date = new Date(newDate);
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hr = String(date.getHours()).padStart(2, "0");
        var min = String(date.getMinutes()).padStart(2, "0");
        var sec = date.getSeconds();

        return `${d}/${m + 1}/${y} | ${hr}:${min}`;
    }

    const btn_selected_css = { background: '#288ec0', color: 'white', fontFamily: 'sans-serif', letterSpacing: 0.5 };
    const btn_initial_css = { background: 'white', color: '#24252d', fontFamily: 'sans-serif', letterSpacing: 0.5 };

    //Slide click
    const slide = (shift) => {
        scrl.current.scrollLeft += shift;
        setscrollX(scrollX + shift);

        if (
            Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
            scrl.current.offsetWidth
        ) {
            setscrolEnd(true);
        } else {
            setscrolEnd(false);
        }
    };

    //Anim
    const anim = (e) => {
        gsap.from(e.target, { scale: 1 });
        gsap.to(e.target, { scale: 1.5 });
    };
    const anim2 = (e) => {
        gsap.from(e.target, { scale: 1.5 });
        gsap.to(e.target, { scale: 1 });
    };

    const scrollCheck = () => {
        setscrollX(scrl.current.scrollLeft);
        if (
            Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
            scrl.current.offsetWidth
        ) {
            setscrolEnd(true);
        } else {
            setscrolEnd(false);
        }
    };

    const handleClick = (e) => {
        // console.log("this is working fine");
        // console.log('handleClick-> ', e.currentTarget.id);
        // let copy = [...buttnList];
        let result = buttnList.map(el => el.id + '' === e.currentTarget.id ? { ...el, status: true } : { ...el, status: false });
        let redCars = result.filter(car => car.id + '' === e.currentTarget.id);
        // console.log('redCars->', redCars);
        setbuttnList(result);
        props.handleBtnClick(redCars[0].rawData);
    }

    return (
        toggleState || buttnList <= 3  ?
            // <>
            //     <div className="scroll-btn">
            //         <ul ref={scrl} onScroll={scrollCheck} style={{ margin: '2px' }}>
            //             {buttnList.map((d, i) => (
            //                 //   <Tag data={d} key={i} />
            //                 <li onClick={handleClick} key={d.id} id={d.id} style={d.status ? btn_selected_css : btn_initial_css}>{d.name}
            //                     <p>{d.value}</p>
            //                 </li>
            //             ))}
            //         </ul>
            //     </div>
            // </>
            <>
                <div className="scroll-btn">
                    {scrollX !== 0 && (
                        <button
                            style={{ cursor: 'pointer' }}
                            className="prev"
                            onClick={() => slide(-50)}
                            onMouseEnter={(e) => anim(e)}
                            onMouseLeave={(e) => anim2(e)}
                        >
                            {/* <i className="fa fa-angle-left"></i> */}
                            <ArrowBackIosIcon />
                        </button>
                    )}
                    <ul ref={scrl} onScroll={scrollCheck} style={{ margin: '2px' }}>
                        {buttnList.map((d, i) => (
                            //   <Tag data={d} key={i} />
                            <li onClick={handleClick} key={d.id} id={d.id} style={d.status ? btn_selected_css : btn_initial_css}>{d.name}
                                <p>{d.value}</p>
                            </li>
                        ))}
                    </ul>
                    {!scrolEnd && (
                        <button
                            style={{ cursor: 'pointer' }}
                            className="next"
                            onClick={() => slide(+50)}
                            onMouseEnter={(e) => anim(e)}
                            onMouseLeave={(e) => anim2(e)}
                        >
                            {/* <i className="fa fa-angle-right"></i> */}
                            <ArrowForwardIosIcon />
                        </button>
                    )}
                </div>
            </>
            :
            <>
                <div className="scroll-btn">
                    {scrollX !== 0 && (
                        <button
                            style={{ cursor: 'pointer' }}
                            className="prev"
                            onClick={() => slide(-50)}
                            onMouseEnter={(e) => anim(e)}
                            onMouseLeave={(e) => anim2(e)}
                        >
                            {/* <i className="fa fa-angle-left"></i> */}
                            <ArrowBackIosIcon />
                        </button>
                    )}
                    <ul ref={scrl} onScroll={scrollCheck} style={{ margin: '2px' }}>
                        {buttnList.map((d, i) => (
                            //   <Tag data={d} key={i} />
                            <li onClick={handleClick} key={d.id} id={d.id} style={d.status ? btn_selected_css : btn_initial_css}>{d.name}
                                <p>{d.value}</p>
                            </li>
                        ))}
                    </ul>
                    {!scrolEnd && (
                        <button
                            style={{ cursor: 'pointer' }}
                            className="next"
                            onClick={() => slide(+50)}
                            onMouseEnter={(e) => anim(e)}
                            onMouseLeave={(e) => anim2(e)}
                        >
                            {/* <i className="fa fa-angle-right"></i> */}
                            <ArrowForwardIosIcon />
                        </button>
                    )}
                </div>
            </>
    );
}

const fruitsJSON = [
    { name: "Visit", value: '16/04/2022', status: true, id: 1 },
    { name: "Visit", value: '16/04/2022', status: false, id: 2 },
    { name: "Visit", value: '16/04/2022', status: false, id: 3 },
    { name: "Visit", value: '16/04/2022', status: false, id: 4 },
    { name: "Visit", value: '16/04/2022', status: false, id: 5 },
    { name: "Visit", value: '16/04/2022', status: false, id: 6 },
    { name: "Visit", value: '16/04/2022', status: false, id: 7 },
    { name: "Visit", value: '16/04/2022', status: false, id: 8 },
    { name: "Visit", value: '16/04/2022', status: false, id: 9 },
    { name: "Visit", value: '16/04/2022', status: false, id: 10 },
    { name: "Visit", value: '16/04/2022', status: false, id: 11 },
    { name: "Visit", value: '16/04/2022', status: false, id: 12 },
    { name: "Visit", value: '16/04/2022', status: false, id: 13 },
    { name: "Visit", value: '16/04/2022', status: false, id: 14 },
    { name: "Visit", value: '16/04/2022', status: false, id: 15 },
    { name: "Visit", value: '16/04/2022', status: false, id: 16 },
    { name: "Visit", value: '16/04/2022', status: false, id: 17 },
]