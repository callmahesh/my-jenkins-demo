import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { Button, Divider } from '@mui/material';
import { initiateReset } from '../../redux/apiCall/authAPIs';
import { error_InitiateResetPassword, reset_InitiateResetPassword } from '../../redux/reducers/authSlice';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
// import './Dialog.scss'
// import ImpactSnackBar from '../../components/common/snackbar/ImpactSnackBar';


export default function ForgotPwdDialog({ closeModal, ...props }) {



    let { title, content } = props.props;
    // let { resetPage } = content.data;

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const initiateResetPassword = useSelector((state) => state.auth.initiateResetPassword);

    const [UserName, setUserName] = React.useState('');
    console.info("ForgotPwdDialog: ", props);

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleDialogClose = () => {
        // dispatch(reset_PostHub());
        // resetPage();
        closeModal();
        dispatch(reset_InitiateResetPassword());
    };

    // React.useEffect(() => {
    //     /* Fetch Action Item for Phase2 */
    //     let reqObj = `?hub_id=${hubId}&phase_id=${phaseId}&limit=${10000}&resource_id=${resourceId}&sort=${'updated_on'}&sortOrder=${-1}`; //&sort=${sortBy}&page=${1}
    //     dispatch(getAllComments(reqObj));
    // }, []);

    


    const handleSubmit = e => {
        e.preventDefault();
        // if(FullName === '' || Email === '' || UserName === '' || Role === '' || Region === '' ){
        if(UserName.replace(/ /g,'') === '' ){
            dispatch(error_InitiateResetPassword('Please fill User ID field!!'));
        }
        // console.log('handleSubmit-> ', FullName, Email, UserName, Role)
        // let reqObj = {
        //     "id": UserName,          
        // }
        dispatch(initiateReset(UserName));
        // dispatch(error_PostHub('Go to Helll!'))
    }    

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={true}
                onClose={handleDialogClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title" style={{ fontFamily:'Roboto', color: '#0F52BA' }}>
                    {title ? title : 'Title'}
                    <IconButton
                        aria-label="close"
                        onClick={handleDialogClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Divider />
                </DialogTitle>
                <DialogContent>                    
                    {
                        initiateResetPassword.isStarted ?
                            // true?
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                <CircularProgress size={30} />
                                <p style={{fontFamily:'Roboto'}}>Sending Reset Email...</p>
                            </Box>
                            :
                            initiateResetPassword.isError ?
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                {/* <CircularProgress size={30} /> */}
                                <CloseIcon sx={{fontSize:60, color:'red'}} />
                                <p style={{fontFamily:'Roboto', color:'#2b2b2b'}} >{initiateResetPassword.error}</p>
                                {/* <Button onClick={handleOpenForm} variant='contained'>Retry</Button> */}
                            </Box>
                            : initiateResetPassword.value !== '' ?
                                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '30vh' }}>
                                    {/* <CircularProgress size={30} /> */}
                                    <CheckCircleOutlineIcon sx={{fontSize:60, color:'green'}} />
                                    <h4 style={{fontFamily:'Roboto', color:'#2b2b2b'}}>{initiateResetPassword.value}</h4>
                                </Box>
                                
                            :
                            <>
                                <form className="hub-form"
                                    onSubmit={handleSubmit}
                                >
                            
                                    <div className="input-group">
                                        <label htmlFor="text">User ID</label>
                                        <input type="text" name="text" placeholder=""
                                            onChange={e => setUserName(e.target.value)} />
                                    </div>
                                                                       

                                    <button className="primary simple-button">Submit</button>
                                </form>
                            </>

                    }


                </DialogContent>
                <DialogActions>

                    {/* <Button onClick={handleDialogClose} autoFocus>
                        Close
                    </Button> */}
                </DialogActions>
            </Dialog>
            {/* {
                requestAccessSel.isError ? <ImpactSnackBar isOpen={true} message={createHubSel.error} handleClose={handleToastClose} severity='error' />
                    : createHubSel.value !== '' ? <ImpactSnackBar isOpen={true} message={createHubSel.value} handleClose={handleToastClose} severity='success' />
                        : null
            } */}
            {
                
            }
        </div>
    );
}
