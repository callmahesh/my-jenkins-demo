import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Divider } from '@mui/material';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import { error_RequestAccess, reset_RequestAccess } from '../../redux/reducers/authSlice';
import { requestAccess } from '../../redux/apiCall/authAPIs';


export default function RequestAccessDialog({ closeModal, ...props }) {



    let { title, content } = props.props;
    // let { resetPage } = content.data;

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const requestAccessSel = useSelector((state) => state.auth.requestAccess);

    const [email, setEmail] = React.useState('');
    const [docCode, setDocCode] = React.useState('');
    const [reason, setReason] = React.useState('');

    // console.info("RequestAccessDialog: ", props);
    
    React.useEffect(() => {
        dispatch(reset_RequestAccess());
    }, []);

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleDialogClose = () => {
        dispatch(reset_RequestAccess());
        // resetPage();
        closeModal();
    };

    // React.useEffect(() => {
    //     /* Fetch Action Item for Phase2 */
    //     let reqObj = `?hub_id=${hubId}&phase_id=${phaseId}&limit=${10000}&resource_id=${resourceId}&sort=${'updated_on'}&sortOrder=${-1}`; //&sort=${sortBy}&page=${1}
    //     dispatch(getAllComments(reqObj));
    // }, []);




    const handleSubmit = e => {
        e.preventDefault();
        // if(FullName === '' || Email === '' || UserName === '' || Role === '' || Region === '' ){
        if (docCode.replace(/ /g,'') === '' || email.replace(/ /g,'') === '' || reason.replace(/ /g,'') === '') {
            dispatch(error_RequestAccess('Please fill all mandatory fields!!!'));
            setTimeout(
                function() {
                    dispatch(reset_RequestAccess());
                }
                .bind(this),
                3000
            );
            return;
        }
        // console.log('handleSubmit-> ', FullName, Email, UserName, Role)
        let reqObj = {
            "email": email,
            "docCode": docCode,
            "reason": reason
        }
        dispatch(requestAccess(reqObj));
    }

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={true}
                onClose={handleDialogClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title" style={{ fontFamily: 'sans-serif', color: '#0F52BA' }}>
                    {title ? title : 'Title'}
                    <IconButton
                        aria-label="close"
                        onClick={handleDialogClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Divider />
                </DialogTitle>
                <DialogContent>
                    {
                        requestAccessSel.isStarted ?
                            // true?
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '50vh', width: '50vh' }}>
                                <CircularProgress size={30} />
                                <p style={{ fontFamily: 'sans-serif', color: '#2b2b2b' }}>Requesting Access...</p>
                            </Box>
                            :
                            requestAccessSel.isError ?
                                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '50vh', width: '50vh' }}>
                                    {/* <CircularProgress size={30} /> */}
                                    <p style={{ fontFamily: 'sans-serif', color: '#2b2b2b' }} >{requestAccessSel.error}</p>
                                    {/* <Button onClick={handleOpenForm} variant='contained'>Retry</Button> */}
                                </Box>
                                : requestAccessSel.value !== '' ?
                                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '50vh', width: '50vh' }}>
                                        {/* <CircularProgress size={30} /> */}
                                        <h4 style={{ fontFamily: 'sans-serif', color: '#2b2b2b' }}>{requestAccessSel.value}</h4>
                                    </Box>

                                    :
                                    <>
                                        <form className="hub-form"
                                            onSubmit={handleSubmit}
                                        >
                                            <div style={{ fontFamily: 'sans-serif' }} className="input-group">
                                                <label htmlFor="text">Email<span style={{ color: 'red' }}>*</span></label>
                                                <input type="text" name="email" placeholder=""
                                                    onChange={e => setEmail(e.target.value)} />
                                            </div>
                                            <div style={{ fontFamily: 'sans-serif' }} className="input-group">
                                                <label htmlFor="text">Doctor Code<span style={{ color: 'red' }}>*</span></label>
                                                <input type="text" name="docCode" placeholder=""
                                                    onChange={e => setDocCode(e.target.value)} />
                                            </div>
                                            <div style={{ fontFamily: 'sans-serif' }} className="input-group">
                                                <label htmlFor="text">Reason<span style={{ color: 'red' }}>*</span></label>
                                                <input type="text" name="reason" placeholder=""
                                                    onChange={e => setReason(e.target.value)} />
                                            </div>
                                            <div style={{ fontFamily: 'sans-serif' }}>
                                                <button className="primary simple-button">Submit</button>
                                            </div>
                                        </form>
                                    </>

                    }


                </DialogContent>
                <DialogActions>

                    {/* <Button onClick={handleDialogClose} autoFocus>
                        Close
                    </Button> */}
                </DialogActions>
            </Dialog>
            {/* {
                requestAccessSel.isError ? <ImpactSnackBar isOpen={true} message={createHubSel.error} handleClose={handleToastClose} severity='error' />
                    : createHubSel.value !== '' ? <ImpactSnackBar isOpen={true} message={createHubSel.value} handleClose={handleToastClose} severity='success' />
                        : null
            } */}
            {

            }
        </div>
    );
}
