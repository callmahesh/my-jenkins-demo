import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Divider } from '@mui/material';
// import { reset_PostHub, error_PostHub } from '../../redux/reducers/hubSlice';
// import { postHub } from '../../redux/apiCall/hubAPIs';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
// import './Dialog.scss'
// import ImpactSnackBar from '../../components/common/snackbar/ImpactSnackBar';


export default function ConfirmDialog({ closeModal, ...props }) {



    let { title, content, handleLogout } = props.props;
    // let { resetPage } = content.data;

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const requestAccessSel = useSelector((state) => state.auth.requestAccess);

    
    console.info("ConfirmDialog: ", props);

    window.history.pushState(null, null, window.location.pathname);

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const handleDialogClose = () => {
        // dispatch(reset_PostHub());
        // resetPage();
        closeModal();
    };

    const handleLogoutBtn = () =>{
        closeModal();
        handleLogout();
    }

    // React.useEffect(() => {
    //     /* Fetch Action Item for Phase2 */
    //     let reqObj = `?hub_id=${hubId}&phase_id=${phaseId}&limit=${10000}&resource_id=${resourceId}&sort=${'updated_on'}&sortOrder=${-1}`; //&sort=${sortBy}&page=${1}
    //     dispatch(getAllComments(reqObj));
    // }, []);

    


    const handleSubmit = e => {
        e.preventDefault();
        
    }    

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={true}
                onClose={handleDialogClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title" style={{fontFamily:'Roboto', color: '#0F52BA' }}>
                    {title ? title : 'Title'}
                    <IconButton
                        aria-label="close"
                        onClick={handleDialogClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Divider />
                </DialogTitle>
                <DialogContent>                    
                    
                   
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '20vh', width: '40vh' }}>
                                <h3 style={{fontFamily:'Roboto'}}> Do you want to Logout ?</h3>
                                <p style={{fontFamily:'Roboto', color:'red'}}>Your Session will be closed</p>
                            </Box>
                          

                    


                </DialogContent>
                <DialogActions>

                    {/* <Button onClick={handleDialogClose} autoFocus>
                        Close
                    </Button> */}
                    <Button onClick={handleLogoutBtn} autoFocus>
                        Logout
                    </Button>
                </DialogActions>
            </Dialog>
            {
                
            }
        </div>
    );
}
