import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Button, Divider } from '@mui/material';
// import { reset_PostHub, error_PostHub } from '../../redux/reducers/hubSlice';
// import { postHub } from '../../redux/apiCall/hubAPIs';
import { error_SendCommentEmail, reset_SendCommentEmail } from '../../redux/reducers/commentsSlice';
import { sendCommentEmail, getAllComment } from '../../redux/apiCall/commentsAPIs';
import { useSelector, useDispatch } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';
import './dialog.scss'


export default function SendMailDialog({ closeModal, ...props }) {



    let { title, content, visitId, role, resetComment } = props.props;
    // let { resetPage } = content.data;

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const sendCommentEmailSel = useSelector((state) => state.comment.sendCommentEmail);

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    let jwt_token = sessionStorage.getItem('eCRF_token');

    const handleDialogClose = () => {
        dispatch(reset_SendCommentEmail());
        resetComment();
        // resetPage();
        closeModal();
    };

    const [checked, setChecked] = React.useState([false, false, false]);

    const handleChange1 = (event) => {
        setChecked([event.target.checked, checked[1], checked[2]]);
    };

    const handleChange2 = (event) => {
        setChecked([checked[0], event.target.checked, checked[2]]);
    };

    const handleChange3 = (event) => {
        setChecked([checked[0], checked[1], event.target.checked]);
    };






    const handleSubmit = e => {
        e.preventDefault();
        if (!checked[0] && !checked[1] && !checked[2]) {
            dispatch(error_SendCommentEmail('Please choose at least one recipient...'));
            return;
        }

        let reqObj = [];
        if (checked[0]) {
            reqObj.push("INVESTIGATOR");
        }
        if (checked[1]) {
            reqObj.push("CO_INVESTIGATOR");
        }
        if (checked[2]) {
            reqObj.push("REGIONAL_INVESTIGATOR");
        }
        dispatch(sendCommentEmail(reqObj, visitId, jwt_token));
    }

    return (
        <div>
            <Dialog
                fullScreen={fullScreen}
                open={true}
                onClose={handleDialogClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="responsive-dialog-title" style={{ color: '#0F52BA' }}>
                    {title ? title : 'Title'}
                    <IconButton
                        aria-label="close"
                        onClick={handleDialogClose}
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Divider />
                </DialogTitle>
                <DialogContent>
                    {
                        sendCommentEmailSel.isStarted ?
                            // true?
                            <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '25vh', width: '35vh' }}>
                                <CircularProgress size={30} />
                                <p style={{ fontFamily: 'Roboto' }}>Sending email notification...</p>
                            </Box>
                            :
                            sendCommentEmailSel.isError ?
                                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '25vh', width: '35vh' }}>
                                    {/* <CircularProgress size={30} /> */}
                                    <p style={{ color: '#2b2b2b', fontFamily: 'Roboto' }} >{sendCommentEmailSel.error}</p>
                                    {/* <Button onClick={handleOpenForm} variant='contained'>Retry</Button> */}
                                </Box>
                                : sendCommentEmailSel.value !== '' ?
                                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '25vh', width: '35vh' }}>
                                        {/* <CircularProgress size={30} /> */}
                                        <h4 style={{ color: '#2b2b2b', fontFamily: 'Roboto' }}>{sendCommentEmailSel.value}</h4>
                                    </Box>

                                    :
                                    <>
                                        <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: 'flex', width: '50vh', fontFamily:'Roboto' }}>
                                            <FormGroup>
                                                <FormControlLabel control={<Checkbox checked={checked[0]} checkedIcon={<CheckBoxOutlinedIcon />} onChange={handleChange1} />} label="Investigator" />
                                                {role === 'REGIONAL_INVESTIGATOR' || role === 'PRINCIPAL_INVESTIGATOR' ?
                                                    <FormControlLabel control={<Checkbox checked={checked[1]} checkedIcon={<CheckBoxOutlinedIcon />} onChange={handleChange2} />} label="Co Principal Investigator" />
                                                    :
                                                    null
                                                }
                                                {role === 'PRINCIPAL_INVESTIGATOR' ?
                                                    <FormControlLabel control={<Checkbox checked={checked[2]} checkedIcon={<CheckBoxOutlinedIcon />} onChange={handleChange3} />} label="Regional Principal Investigator" />
                                                    :
                                                    null
                                                }

                                                <Button sx={{ textTransform: 'none' , marginTop:3, backgroundColor:'#1e85b2', fontFamily: 'Roboto'}} onClick={handleSubmit} variant="contained">Send Mail</Button>
                                            </FormGroup>
                                        </Box>
                                    </>

                    }


                </DialogContent>
                <DialogActions>

                    {/* <Button onClick={handleDialogClose} autoFocus>
                        Close
                    </Button> */}
                </DialogActions>
            </Dialog>
            {/* {
                sendCommentEmailSel.isError ? <ImpactSnackBar isOpen={true} message={createHubSel.error} handleClose={handleToastClose} severity='error' />
                    : createHubSel.value !== '' ? <ImpactSnackBar isOpen={true} message={createHubSel.value} handleClose={handleToastClose} severity='success' />
                        : null
            } */}
            {

            }
        </div>
    );
}
