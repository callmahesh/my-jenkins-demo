import * as React from 'react';
import logo from '../../assets/images/logo.jpg';
import regular from '../../assets/images/regular.svg';
import enrolled from '../../assets/images/enrolled.svg';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import "./PatientDetails.scss";
import { fontSize } from '@mui/system';
import { useSelector, useDispatch } from 'react-redux';



const PatientDetails = (props) => {

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const patientData = useSelector((state) => state.patient.patientDetails.value);
    const toggleState = useSelector((state) => state.employee.toggle.current);

    if(patientData && Object.keys(patientData).length > 0){
        sessionStorage.setItem('patientData',JSON.stringify(patientData));
    }
    
    
    // console.log('PatientDetails--> ', patientData);

    let userName = '', age = '', phone = '', address = '', city = '', state = '';
    if (patientData && Object.keys(patientData).length > 0) {
        userName = patientData.username;
        age = `${patientData.age}/${patientData.gender}`;
        phone = patientData.mobileNo;
        address = patientData.address;
        city = patientData.city;
        state = patientData.state;
    }else{
        let tempPatData = JSON.parse(sessionStorage.getItem('patientData'));
        userName = tempPatData.username;
        age = `${tempPatData.age}/${tempPatData.gender}`;
        phone = tempPatData.mobileNo;
        address = tempPatData.address;
        city = tempPatData.city;
        state = tempPatData.state;
    }


    return (
        <div className="mainContainer2">
            <Grid container>
                <Grid item xs={1} sm={2} md={1}>
                    <Avatar alt="Lokesh" src={toggleState?regular:enrolled} />
                </Grid>

                <Grid item xs={5} sm={6} md={3}>
                    <div>
                        {userName?userName:'NA'}
                    </div>
                </Grid>
                <Grid item xs={6} sm={4} md={2}>
                    <div>
                        <label >AGE/GENDER:</label>
                        <label > {age? age:'NA'} </label>
                    </div>
                </Grid>
                <Grid item xs={6} sm={8} md={2}>
                    <div>
                        <label >MOBILE NO.:</label>
                        <label> {phone? phone:'NA'} </label>
                    </div>
                </Grid>
                <Grid item xs={6} sm={4} md={4}>
                    <div>
                        <label >ADDRESS:</label>
                        <label> {address ? address : city + ',' + state} </label>
                    </div>
                </Grid>
                {/* <Stack spacing={1} style={{ alignItems: 'baseline', marginLeft: '50px' }}>
                    <label > {'userName'} </label>
                </Stack>


                <Stack spacing={1} style={{ alignItems: 'baseline', marginLeft: '50px' }}>
                    <label style={{ color: '#a8a8a8', fontSize: '14px', opacity: '0.9', marginTop: '35px' }}>Age/Gender:</label>
                    <label style={{ color: '#010304', fontSize: '16px', opacity: '0.9', }}> {age} </label>
                </Stack>

                <Stack spacing={1} style={{ alignItems: 'baseline', marginLeft: '70px' }}>
                    <label style={{ color: '#a8a8a8', fontSize: '14px', opacity: '0.9', marginTop: '35px' }}>Mobile No.:</label>
                    <label style={{ color: '#010304', fontSize: '16px', opacity: '0.9' }}> {phone} </label>
                </Stack>

                <Stack spacing={1} style={{ alignItems: 'baseline', marginLeft: '80px' }}>
                    <label style={{ color: '#a8a8a8', fontSize: '14px', opacity: '0.9', marginTop: '35px' }}>Address:</label>
                    <label style={{ color: '#010304', fontSize: '16px', opacity: '0.9' }}> {address} </label>
                </Stack> */}

            </Grid>
        </div>
    );
}

export default PatientDetails;
