import React, { useContext } from 'react';
import './EcrfTable.scss';
import { Switch, Button, Stack, Pagination, Box, Grid, CircularProgress, Typography } from '@mui/material';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { Paper, InputBase, IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import RestartAltRoundedIcon from '@mui/icons-material/RestartAltRounded';
import { useSelector, useDispatch } from 'react-redux';
import { getPatientByInv } from '../../redux/apiCall/patientAPIs';
import { set_PatientDetails } from '../../redux/reducers/patientSlice';
import { set_Investigator, set_CoInvestigator, set_RPInvestigator, set_ToggleInv, set_CommentReload, set_CurrentManager } from '../../redux/reducers/employeeSlice';
import { useNavigate } from 'react-router-dom';
import EcrfHeader from '../header/EcrfHeader';
import regular from '../../assets/images/regular.svg';
import enrolled from '../../assets/images/enrolled.svg';
import { ModalContext } from '../../utils/utilsOfModal/ModalContext';


const InvestigatorTable = props => {

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const patientInvData = useSelector((state) => state.patient.patientsByInv);

    const toggleState = useSelector((state) => state.employee.toggle.investigator);
    const commentReload = useSelector((state) => state.employee.firstload.comment);

    let { value: patientInv, pagination } = patientInvData;

    const { setCurrentModal } = useContext(ModalContext);

    let jwt_token = sessionStorage.getItem('eCRF_token');
    sessionStorage.setItem('patientData', '');

    const [page, setPage] = React.useState(pagination && 'currentPage' in pagination ? pagination.currentPage : 0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [totalPages, setTotalPages] = React.useState(pagination && 'totalPages' in pagination ? pagination.totalPages : 1);
    // const [itemCount, setItemCount] = React.useState(pagination && 'totalRows' in pagination ? pagination.totalRows : 0);
    const [serachBy, setSerachBy] = React.useState('');
    const [checked, setChecked] = React.useState(true);
    const [finishStatus, setfinishStatus] = React.useState(false);

    const navigate = useNavigate();

    // console.log('InvestigatorTable: ', pagination, page);



    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleLogout = () => {
        sessionStorage.removeItem('eCRF_token');
        sessionStorage.removeItem('eCRF_userId');
        sessionStorage.removeItem('eCRF_uDetails');
        dispatch(set_CurrentManager(''));
        dispatch(set_Investigator(''));
        dispatch(set_CoInvestigator(''));
        dispatch(set_RPInvestigator(''));
        dispatch(set_ToggleInv(false));
        navigate('/login')
    };

    const onBackButtonEvent = (e) => {
        e.preventDefault();

        if (!finishStatus) {
            setfinishStatus(true);
            let title = "Confirm Logout", content = '';
            openModal({ name: 'ConfirmDialog', props: { title, content, handleLogout } });
            props.history.push("/");
        } else {
            window.history.pushState(null, null, window.location.pathname);
            setfinishStatus(false)
        }

    }


    React.useEffect(() => {
        window.history.pushState(null, null, window.location.pathname);
        window.addEventListener('popstate', onBackButtonEvent);
        return () => {
            window.removeEventListener('popstate', onBackButtonEvent);
        };
    }, []);

    React.useEffect(() => {
        if (jwt_token) {
            setChecked(toggleState);
            dispatch(getPatientByInv(rowsPerPage, page, jwt_token, 'id', 'DESC', '', toggleState ? 'REGULAR' : 'REGISTRY'));
            if (commentReload) {
                let newPage = page === 0 ? 0 : page;
                dispatch(getPatientByInv(rowsPerPage, newPage, jwt_token, 'id', 'DESC', serachBy, toggleState ? 'REGULAR' : 'REGISTRY'));
                dispatch(set_CommentReload(false));
            }
        } else {
            navigate('/login');
        }
        localStorage.setItem("dontShowMedLab", true);
        if(!localStorage.getItem('patientType')) localStorage.setItem('patientType','REGULAR');
    }, [])

    // Set total pages
    React.useEffect(() => {
        /* Fetch Action Item for Phase3 */
        setTotalPages(pagination && 'totalPages' in pagination ? pagination.totalPages : 1);
        setPage(pagination && 'currentPage' in pagination ? pagination.currentPage : page);
    }, ['totalPages' in pagination ? pagination.totalPages : pagination]);

    // Set Current page
    React.useEffect(() => {
        /* Fetch Action Item for Phase3 */
        setPage(pagination && 'currentPage' in pagination ? pagination.currentPage : page);
    }, ['currentPage' in pagination ? pagination.currentPage : page]);

    let patientData = [];
    if (patientInv.length > 0) {
        // patientData = data.user_hubs.map(({ first_name,last_name,user_image, following }) => {
        patientData = patientInv.map((item) => (
            {
                subject: item.username, age: `${item.age}/${item.gender}`,
                location: item.city, visit: item.lastVisitDateTime,
                patName: item.fullName, region: item.state, status: item.lastVisitStatus,
                patientObj: item
            }
        ));
    }

    function getDateTime(newDate) {
        var date = new Date(newDate);
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hr = String(date.getHours()).padStart(2, "0");
        var min = String(date.getMinutes()).padStart(2, "0");
        var sec = date.getSeconds();

        return `${d}/${m + 1}/${y} | ${hr}:${min}`;
    }

    // const { setCurrentModal } = useContext(ModalContext);
    // const openModal = ({ name, props }) => setCurrentModal({ name, props });


    const handleReset = () => {

        let newPage = 0;
        setPage(0);
        setSerachBy('');
        setChecked(true);

        dispatch(set_ToggleInv(true));
        dispatch(getPatientByInv(rowsPerPage, newPage, jwt_token, 'id', 'DESC', '', toggleState ? 'REGULAR' : 'REGISTRY'));
        localStorage.setItem('patientType','REGULAR');
    }

    const handleViewVisit = (patObj) => {
        // console.log('handleViewVisit:', patObj)
        // window.location = "/visit";
        dispatch(set_PatientDetails(patObj));
        if (patObj && 'id' in patObj) {
            navigate(`/visit/${patObj.id}/${patObj.username}`);
        }
    };

    /* Handler for TablePagination next page change request */
    const handleChangePage = (event, newPage) => {
        // console.log('handleChangePage:', newPage);
        if (newPage === 1 & page !== 0) {
            setPage(0);
        } else {
            setPage(newPage - 1);
        }
        if (jwt_token) {
            dispatch(getPatientByInv(rowsPerPage, newPage - 1, jwt_token, 'id', 'DESC', serachBy, checked ? 'REGULAR' : 'REGISTRY'));
            // dispatch(getMyProfile(2,jwt_token));
        } else {
            navigate('/login');
        }
        // setPage(newPage);
        // let nextPage = newPage + 1;
        // // console.log('Phase2: handleChangePage--> ', newPage, ', oldPage: ', page);
        // if (allResourcesSel.phase === 'phase3') {
        //     let reqObj = `?hub_id=${userSelectedHub.id}&phase_id=${3}&limit=${rowsPerPage}&sort=${sortBy}&sortOrder=${sortOrder}&page=${nextPage}`;
        //     dispatch(getAllResources(reqObj, 'phase3'));
        // }
    };
    console.log('INV-Table: ShowFamHistory: ', localStorage.getItem("patientType"));

    /* Hamdler for Investigator dropdown change */
    const handleSearchByChange = (event) => {
        event.preventDefault();
        // console.log('handleMenuItemChange-> ', event.target.value)
        setSerachBy(event.target.value);
        // let newPage = page === 0? 0 : page-1;
        // dispatch(getPatientByInvId(rowsPerPage, newPage , jwt_token, 'id', 'DESC', event.target.value, serachBy));
    };

    /* Hamdler for Investigator dropdown change */
    const handleSerachClick = (e) => {
        e.preventDefault();
        // console.log('handleSerachClick-> ')        
        let newPage = 0;
        setPage(0);
        dispatch(getPatientByInv(rowsPerPage, newPage, jwt_token, 'id', 'DESC', serachBy, checked ? 'REGULAR' : 'REGISTRY'));
    };

    const handleToggleChange = () => {
        setChecked(!checked);
        localStorage.setItem("dontShowMedLab", !checked);
        localStorage.setItem('patientType',!checked ? 'REGULAR' : 'REGISTRY');
        let newPage = 0;
        setPage(0);
        dispatch(getPatientByInv(rowsPerPage, newPage, jwt_token, 'id', 'DESC', serachBy, !checked ? 'REGULAR' : 'REGISTRY'));
    }

    const onKeyDownHandler = e => {

        if (e.keyCode === 13) {
            e.preventDefault();
            let newPage = 0;
            setPage(0);
            dispatch(getPatientByInv(rowsPerPage, newPage, jwt_token, 'id', 'DESC', serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        }
    };

    return (
        <>
            <EcrfHeader />
            <div className='table-filter-div-main'>
                <Grid container>
                    <Grid item xs={12} sm={4} >
                        <img src={enrolled} style={{ height: 30, weight: 30, marginRight: 5 }} alt='registry' />
                        <Typography style={{ fontFamily: 'sans-serif' }}>Registry</Typography>
                        <Switch checked={checked} onChange={handleToggleChange} />
                        <img src={regular} style={{ height: 30, weight: 30, marginRight: 5 }} alt='regular' />
                        <Typography style={{ fontFamily: 'sans-serif' }}>Regular Screening</Typography>
                    </Grid>
                    <Grid item xs={12} sm={8}>

                        <Paper
                            component="form"
                            sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 200 }}
                        >
                            <InputBase
                                sx={{ ml: 1, flex: 1, fontFamily: 'sans-serif', fontSize: 15 }}
                                placeholder="Search"
                                inputProps={{ 'aria-label': 'search google maps' }}
                                onChange={(e) => { handleSearchByChange(e) }}
                                onKeyDown={(e) => { onKeyDownHandler(e) }}
                                value={serachBy}
                            />
                            <IconButton type="submit" sx={{ p: '10px' }} aria-label="search" onClick={(e) => { handleSerachClick(e) }}>
                                <SearchIcon />
                            </IconButton>
                        </Paper>
                        <RestartAltRoundedIcon style={{ color: '#1e85b2', cursor: 'pointer' }} onClick={handleReset} />
                    </Grid>
                </Grid>
            </div>
            <div className='ecrf-table-main'>
                <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer >

                        {
                            patientInvData.isStarted ?
                                <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100vh' }}>
                                    <CircularProgress size={30} />
                                    <p style={{ color: 'white' }}>Loading...</p>
                                </Box>
                                :
                                patientData.length > 0 ?
                                    <>
                                        <Table aria-label="simple table" style={{ marginBottom: 10 }}>
                                            <TableHead>
                                                <TableRow key='inv-header' >
                                                    <TableCell align='left'>SUBJECT ID</TableCell>
                                                    <TableCell align="left">AGE/GENDER</TableCell>
                                                    <TableCell align="left">LOCATION/CITY</TableCell>
                                                    <TableCell align="left">VISIT DATE/TIME</TableCell>
                                                    <TableCell align="left">PATIENT NAME</TableCell>
                                                    <TableCell align="left">REGION</TableCell>
                                                    <TableCell align="left">STATUS</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {patientData.map((row) => (
                                                    <TableRow key={row.subject}>
                                                        <TableCell component="th" scope="row" sx={{ opacity: 0.6 }}>{row.subject}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.age}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.location}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{getDateTime(row.visit)}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.patName}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.region}</TableCell>
                                                        <TableCell align="left">
                                                            <Button variant="contained" style={row.status === 'NEW' ?
                                                                { cursor: 'default', textTransform: 'none', marginRight: 37, backgroundColor: '#00ac0a', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #00ac0a', borderRadius: 5, padding: '9px 20px 7px' }
                                                                :
                                                                row.status === 'NOACTION' ?
                                                                    { cursor: 'default', textTransform: 'none', marginRight: 37, backgroundColor: '#cb4154', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #cb4154', borderRadius: 5, padding: '9px 20px 7px' }
                                                                    :
                                                                    { cursor: 'default', border: '1px solid #b7b7b7', textTransform: 'none', marginRight: 37, backgroundColor: 'white', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#9a9a9d', border: 'solid 1px #9a9a9d', borderRadius: 5, padding: '9px 8px 7px 9px' }}
                                                            >{row.status === 'NEW' ? 'New' : ''}{row.status === 'NOACTION' ? 'Initiated' : ''}{row.status === 'REVIEWED' ? 'Reviewed' : ''}</Button>
                                                            <Button variant="contained" style={{ width: 52, height: 30, textTransform: 'none', backgroundColor: '#288ec0', fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #288ec0f', borderRadius: 5, padding: '9x 15px 7px' }} onClick={() => { dispatch(set_ToggleInv(checked)); handleViewVisit(row.patientObj) }}>
                                                                View
                                                            </Button>

                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>

                                    </>
                                    :
                                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100vh' }}>
                                        <h3 > No data !!</h3>
                                    </div>
                        }
                        {/* <Table sx={{ minWidth: 650 }} aria-label="simple table"> */}

                    </TableContainer>
                    <Stack spacing={2} style={{ margin: '10px 0px' }}>
                        <Pagination count={totalPages} defaultPage={page + 1} color="primary" onChange={handleChangePage}
                            key={`slider-${page}`} />
                    </Stack>
                </Paper>
            </div>
        </>

    )


}

export default InvestigatorTable;