import * as React from 'react';
import InvestigatorTable from './InvestigatorTable';
import PItable from './PItable';


export default function SelectTableByRole(props) {
  let signedInUser = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));
  const [role, setRole] = React.useState('CO_INVESTIGATOR');

  const handleChange = (event) => {
    setRole(event.target.value);
  };

  return (
    <div>
        {
          signedInUser && 'roles' in signedInUser?
          signedInUser.roles[0] === 'CO_INVESTIGATOR'?
            <PItable />
            : 
            signedInUser.roles[0] === 'INVESTIGATOR'?
            <InvestigatorTable />
            :
            signedInUser.roles[0] === 'REGIONAL_INVESTIGATOR'?
            <PItable />
            :
            signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR'?
            <PItable />
            :
            null
            :
            null
        }
    </div>
  );
}