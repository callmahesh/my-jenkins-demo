import React, { useContext } from 'react';
import './EcrfTable.scss';
import EcrfHeader from '../header/EcrfHeader';
import { Switch, Button, Stack, Pagination, Box, Grid, CircularProgress, Typography } from '@mui/material';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import { MenuItem, Select, InputLabel, FormControl, Paper, InputBase, IconButton } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import SendRoundedIcon from '@mui/icons-material/SendRounded';
import RestartAltRoundedIcon from '@mui/icons-material/RestartAltRounded';
import { useSelector, useDispatch } from 'react-redux';
import { getPatientByInvId } from '../../redux/apiCall/patientAPIs';
import { getPInvReportee, getCoInvReportee, getRPInvReportee } from '../../redux/apiCall/employeeAPIs'
import { set_Investigator, set_CoInvestigator, set_RPInvestigator, set_ToggleOthers, set_FirstLoad, set_CommentReload, set_CurrentManager } from '../../redux/reducers/employeeSlice';
import { set_PatientDetails } from '../../redux/reducers/patientSlice';
import { useNavigate } from 'react-router-dom';
import { ModalContext } from '../../utils/utilsOfModal/ModalContext';


const PItable = props => {

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const pInvReportee = useSelector((state) => state.employee.pInvReportee.value);
    const rPInvReportee = useSelector((state) => state.employee.rPInvReportee.value);
    const coInvReportee = useSelector((state) => state.employee.coInvReportee.value);

    const currentRpi = useSelector((state) => state.employee.pInvReportee.current);
    const currentCopi = useSelector((state) => state.employee.rPInvReportee.current);
    const currentInvestigator = useSelector((state) => state.employee.coInvReportee.current);
    const currentManagerRed = useSelector((state) => state.employee.manager.current);

    const toggleState = useSelector((state) => state.employee.toggle.others);
    // const patientInvData = useSelector((state) => state.patient.patientsByInv);
    const patientInvData = useSelector((state) => state.patient.patientsByInvId);
    let { value: patientInv, pagination } = patientInvData;

    let jwt_token = sessionStorage.getItem('eCRF_token');
    let signedInUser = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));
    sessionStorage.setItem('patientData', '');

    const [page, setPage] = React.useState(pagination && 'currentPage' in pagination ? pagination.currentPage : 0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [totalPages, setTotalPages] = React.useState(pagination && 'totalPages' in pagination ? pagination.totalPages : 1);
    const [serachBy, setSerachBy] = React.useState('');
    // const [itemCount, setItemCount] = React.useState(pagination && 'totalRows' in pagination ? pagination.totalRows : 0);
    const [rPiList, setRPiList] = React.useState([]);
    const [coPiList, setCoPiList] = React.useState([]);
    const [invList, setInvList] = React.useState([]);
    const [rPInvestigator, setrPInvestigator] = React.useState('');
    const [coInvestigator, setCoInvestigator] = React.useState('');
    const [investigator, setInvestigator] = React.useState('');
    const [currentManager, setCurrentManager] = React.useState('');

    const [checked, setChecked] = React.useState(false);
    const [locationKeys, setLocationKeys] = React.useState([]);

    const firstLoad = useSelector((state) => state.employee.firstload.current);
    const commentReload = useSelector((state) => state.employee.firstload.comment);



    const navigate = useNavigate();

    // console.log('PItable: ', patientInv);


    const [finishStatus, setfinishStatus] = React.useState(false);
    const { setCurrentModal } = useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleLogout = () => {
        sessionStorage.removeItem('eCRF_token');
        sessionStorage.removeItem('eCRF_userId');
        sessionStorage.removeItem('eCRF_uDetails');
        dispatch(set_CurrentManager(''));
        dispatch(set_Investigator(''));
        dispatch(set_CoInvestigator(''));
        dispatch(set_RPInvestigator(''));
        dispatch(set_ToggleOthers(false));
        navigate('/login')
    };

    const onBackButtonEvent = (e) => {
        e.preventDefault();

        if (!finishStatus) {
            setfinishStatus(true);
            let title = "Confirm Logout", content = '';
            openModal({ name: 'ConfirmDialog', props: { title, content, handleLogout } });
            props.history.push("/");
        } else {
            window.history.pushState(null, null, window.location.pathname);
            setfinishStatus(false);
        }
    }


    React.useEffect(() => {
        window.history.pushState(null, null, window.location.pathname);
        window.addEventListener('popstate', onBackButtonEvent);
        return () => {
            window.removeEventListener('popstate', onBackButtonEvent);
        };
    }, []);


    React.useEffect(() => {
        if (jwt_token && signedInUser && signedInUser.id) {
            // dispatch(getPatientByInv(rowsPerPage, page, jwt_token, 'id', 'DESC'));
            if (signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR') {
                dispatch(getPInvReportee(signedInUser.id, jwt_token));
            }
            else if (signedInUser.roles[0] === 'REGIONAL_INVESTIGATOR') {
                dispatch(getRPInvReportee(signedInUser.id, jwt_token));
            }
            else if (signedInUser.roles[0] === 'CO_INVESTIGATOR') {
                dispatch(getCoInvReportee(signedInUser.id, jwt_token));
            }

            setChecked(toggleState);
            if (currentRpi != '') {
                setrPInvestigator(currentRpi);
            }
            if (currentCopi != '') {
                setCoInvestigator(currentCopi);
            }
            if (currentInvestigator != '') {
                setInvestigator(currentInvestigator);
            }
            if (currentManagerRed != '') {
                let newPage = page === 0 ? 0 : page;
                setCurrentManager(currentManagerRed);
                dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManagerRed, serachBy, toggleState ? 'REGULAR' : 'REGISTRY'));
            }
            else {
                let newPage = 0;
                setCurrentManager(signedInUser.id);
                dispatch(set_CurrentManager(signedInUser.id));
                dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', signedInUser.id, serachBy, toggleState ? 'REGULAR' : 'REGISTRY'));
            }
            if (commentReload) {
                let newPage = page === 0 ? 0 : page;
                dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManagerRed, serachBy, toggleState ? 'REGULAR' : 'REGISTRY'));
                dispatch(set_CommentReload(false));
            }
        } else {
            navigate('/login');
        }
    }, []);


    React.useEffect(() => {
        // console.log('RPItable-1>')
        if (Array.isArray(pInvReportee) && pInvReportee.length > 0) {
            let tempArray = pInvReportee.map((item) => (
                { invId: item.id, invUsername: item.username, invFullname: item.fullname }
            )
            );
            setRPiList(tempArray);

            // if (firstLoad == false) {
            //     // setrPInvestigator(tempArray[0].invId);
            //     dispatch(getRPInvReportee(tempArray[0].invId, jwt_token));
            // }
        }
    }, [pInvReportee]);

    React.useEffect(() => {
        // console.log('RPItable-2>')
        if (Array.isArray(rPInvReportee) && rPInvReportee.length > 0) {
            let tempArray = rPInvReportee.map((item) => (
                { invId: item.id, invUsername: item.username, invFullname: item.fullname }
            )
            );
            setCoPiList(tempArray);
            // let newPage = page === 0 ? 0 : page - 1;
            // if (firstLoad == false) {
            //     // setCoInvestigator(tempArray[0].invId);
            //     dispatch(getCoInvReportee(tempArray[0].invId, jwt_token));
            // }
        }
    }, [rPInvReportee]);

    React.useEffect(() => {
        // console.log('RPItable-3>')
        if (Array.isArray(coInvReportee) && coInvReportee.length > 0) {
            let tempArray = coInvReportee.map((item) => (
                { invId: item.id, invUsername: item.username, invFullname: item.fullname }
            )
            )
            setInvList(tempArray);
            // if (firstLoad == false) {
            //     // setInvestigator(tempArray[0].invId);
            //     dispatch(set_FirstLoad(true));
            //     // let newPage = page === 0 ? 0 : page - 1;
            //     // dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', tempArray[0].invId, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
            // }
        }
    }, [coInvReportee]);


    // Set total pages
    React.useEffect(() => {
        /* Fetch Action Item for Phase3 */
        setTotalPages(pagination && 'totalPages' in pagination ? pagination.totalPages : 1);
        setPage(pagination && 'currentPage' in pagination ? pagination.currentPage : page);
    }, ['totalPages' in pagination ? pagination.totalPages : pagination]);

    // Set Current page
    React.useEffect(() => {
        /* Fetch Action Item for Phase3 */
        setPage(pagination && 'currentPage' in pagination ? pagination.currentPage : page);
    }, ['currentPage' in pagination ? pagination.currentPage : page]);

    let patientData = [];
    if (patientInv.length > 0) {
        // patientData = data.user_hubs.map(({ first_name,last_name,user_image, following }) => {
        patientData = patientInv.map((item) => (
            {
                subject: item.username, age: `${item.age}/${item.gender}`,
                location: item.city, visit: item.lastVisitDateTime,
                patName: item.fullName, region: item.state, status: item.lastVisitStatus,
                patientObj: item
            }
        ));
    };

    function getDateTime(newDate) {
        var date = new Date(newDate);
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var hr = String(date.getHours()).padStart(2, "0");
        var min = String(date.getMinutes()).padStart(2, "0");
        var sec = date.getSeconds();

        return `${d}/${m + 1}/${y} | ${hr}:${min}`;
    };

    const handleReset = () => {
        if (signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR') {
            dispatch(getPInvReportee(signedInUser.id, jwt_token));
        }
        else if (signedInUser.roles[0] === 'REGIONAL_INVESTIGATOR') {
            dispatch(getRPInvReportee(signedInUser.id, jwt_token));
        }
        else if (signedInUser.roles[0] === 'CO_INVESTIGATOR') {
            dispatch(getCoInvReportee(signedInUser.id, jwt_token));
        }

        let newPage = 0;
        setPage(0);
        setCurrentManager(signedInUser.id);
        setCoInvestigator('');
        setInvestigator('');
        setrPInvestigator('');
        setSerachBy('');
        setChecked(false);

        dispatch(set_Investigator(''));
        dispatch(set_CoInvestigator(''));
        dispatch(set_RPInvestigator(''));
        dispatch(set_ToggleOthers(false));
        dispatch(set_CurrentManager(signedInUser.id));
        dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', signedInUser.id, '', toggleState ? 'REGULAR' : 'REGISTRY'));
    }

    const handleViewVisit = (patObj) => {
        // console.log('handleViewVisit:', patObj)
        // window.location = "/visit";
        dispatch(set_PatientDetails(patObj));
        if (patObj && 'id' in patObj) {
            navigate(`/visit/${patObj.id}/${patObj.username}`);
        }

    };

    /* Handler for TablePagination next page change request */
    const handleChangePage = (event, newPage) => {
        // console.log('handleChangePage:', newPage);
        if (newPage === 1 & page !== 0) {
            setPage(0);
        } else {
            setPage(newPage - 1);
        }
        if (jwt_token) {
            // dispatch(getPatientByInv(rowsPerPage, newPage - 1, jwt_token, 'id', 'DESC'));        
            dispatch(getPatientByInvId(rowsPerPage, newPage - 1, jwt_token, 'id', 'DESC', currentManager, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        } else {
            navigate('/login');
        }
    };


    /* Hamdler for Investigator dropdown change */
    const handleMenuItemChange = (event) => {
        event.preventDefault();

        setInvestigator(event.target.value);
        setCurrentManager(event.target.value);

        if (serachBy !== '') {
            let newPage = page === 0 ? 0 : page - 1;
            dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', event.target.value, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        }


        // dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', event.target.value, serachBy, checked ? 'REGULAR' : 'REGISTRY'));


    };

    /* Handler for Co Investigator dropdown change */
    const handleCoPIMenuItemChange = (event) => {
        event.preventDefault();

        setCoInvestigator(event.target.value);
        setCurrentManager(event.target.value);

        setInvList([]);

        dispatch(getCoInvReportee(event.target.value, jwt_token));
        dispatch(set_Investigator(''));

        setInvestigator('');

        if (serachBy !== '') {
            let newPage = page === 0 ? 0 : page - 1;
            dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', event.target.value, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        }
    };

    /* Handler for RPInvestigator dropdown change */
    const handleRPIMenuItemChange = (event) => {
        event.preventDefault();

        setrPInvestigator(event.target.value);
        setCoPiList([]);
        setInvList([]);

        dispatch(getRPInvReportee(event.target.value, jwt_token));
        dispatch(set_CoInvestigator(''));
        dispatch(set_Investigator(''));

        setCoInvestigator('');
        setInvestigator('');
        setCurrentManager(event.target.value);

        if (serachBy !== '') {
            let newPage = page === 0 ? 0 : page - 1;
            dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', event.target.value, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        }
    };

    /* Hamdler for Investigator dropdown change */
    const handleSearchByChange = (event) => {
        event.preventDefault();
        // console.log('handleMenuItemChange-> ', event.target.value)
        setSerachBy(event.target.value);
        // let newPage = page === 0? 0 : page-1;
        // dispatch(getPatientByInvId(rowsPerPage, newPage , jwt_token, 'id', 'DESC', event.target.value, serachBy, checked?'REGISTRY':'REGULAR'));
    };

    /* Hamdler for Investigator dropdown change */
    const handleSerachClick = (event) => {
        event.preventDefault();
        // console.log('handleSerachClick-> ')        
        let newPage = 0;
        setPage(0);
        dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManager, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
    };

    const onKeyDownHandler = e => {

        if (e.keyCode === 13) {
            e.preventDefault();
            let newPage = 0;
            setPage(0);
            dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManager, serachBy, checked ? 'REGULAR' : 'REGISTRY'));
        }
    };

    const handleToggleChange = () => {
        if (checked) {
            setChecked(false);
        } else {
            setChecked(true);
        }
        let newPage = 0;
        setPage(0);
        dispatch(set_ToggleOthers(!checked));
        dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManager, serachBy, !checked ? 'REGULAR' : 'REGISTRY'));
    }

    const handleSubmit = () => {
        let newPage = 0;

        dispatch(set_CurrentManager(currentManager));
        dispatch(set_Investigator(investigator));
        dispatch(set_CoInvestigator(coInvestigator));
        dispatch(set_RPInvestigator(rPInvestigator));
        dispatch(getPatientByInvId(rowsPerPage, newPage, jwt_token, 'id', 'DESC', currentManager, serachBy, checked ? 'REGULAR' : 'REGISTRY'));

    }


    let investList = [];
    // if (patVisit_format && ('occurrence' in patVisit_format) && patVisit_format.occurrence.length > 0) {
    investList = invList.map((item) => {
        if (item) {
            // if()
            return <MenuItem value={item.invId}>{item.invFullname}<span style={{ fontSize: '0.6em', marginLeft: '0.5em' }} >{'(' + item.invUsername + ')'}</span></MenuItem>
        }
    });

    let coPiInvList = [];
    // if (patVisit_format && ('occurrence' in patVisit_format) && patVisit_format.occurrence.length > 0) {
    coPiInvList = coPiList.map((item) => {
        if (item) {
            // if()
            return <MenuItem value={item.invId}>{item.invFullname}<span style={{ fontSize: '0.6em', marginLeft: '0.5em' }} >{'(' + item.invUsername + ')'}</span></MenuItem>
        }
    });

    let rPInvList = [];
    // if (patVisit_format && ('occurrence' in patVisit_format) && patVisit_format.occurrence.length > 0) {
    rPInvList = rPiList.map((item) => {
        if (item) {
            // if()
            return <MenuItem value={item.invId}>{item.invFullname}<span style={{ fontSize: '0.6em', marginLeft: '0.5em' }} >{'(' + item.invUsername + ')'}</span></MenuItem>
        }
    });


    // }

    return (
        <>
            <EcrfHeader />
            <div className='table-filter-div-main'>
                <Grid container style={{ marginBottom: '20px' }} >
                    <Grid item xs={12} justifyContent='flex-end'>

                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>

                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <Paper
                                    component="form"
                                    style={{ display: 'flex', alignItems: 'center', width: 200, height: 37 }}
                                >
                                    <InputBase
                                        sx={{ ml: 1, flex: 1, fontFamily: 'sans-serif', fontSize: 17 }}
                                        placeholder="Search "
                                        inputProps={{ 'aria-label': 'search google maps' }}
                                        // onChange={handleSearchByChange}
                                        onChange={(e) => { handleSearchByChange(e) }}
                                        onKeyDown={(e) => { onKeyDownHandler(e) }}
                                        value={serachBy}
                                    />
                                    <IconButton type="submit" sx={{ p: '10px' }} aria-label="search">
                                        <SearchIcon
                                            onClick={(e) => { handleSerachClick(e) }}
                                        />
                                    </IconButton>
                                </Paper>
                            </div>


                            {
                                signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR' ?
                                    <FormControl sx={{ m: 1, minWidth: 265 }} size="small">
                                        <InputLabel id="demo-select-small">Regional Principal Investigator</InputLabel>
                                        <Select
                                            labelId="demo-select-small"
                                            id="demo-select-small"
                                            value={rPInvestigator}
                                            label="Regional Principal Investigator"
                                            // onChange={handleMenuItemChange}
                                            onChange={(e) => { handleRPIMenuItemChange(e) }}
                                        >

                                            {rPInvList}
                                        </Select>
                                    </FormControl>
                                    :
                                    ''
                            }

                            {
                                signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR' || signedInUser.roles[0] === 'REGIONAL_INVESTIGATOR' ?
                                    <FormControl sx={{ m: 1, minWidth: 220 }} size="small">
                                        <InputLabel id="demo-select-small">Co-Principal Investigator</InputLabel>
                                        <Select
                                            labelId="demo-select-small"
                                            id="demo-select-small"
                                            value={coInvestigator}
                                            label="Co-Principal Investigator"
                                            // onChange={handleMenuItemChange}
                                            onChange={(e) => { handleCoPIMenuItemChange(e) }}
                                        >
                                            {coPiInvList}
                                        </Select>
                                    </FormControl>
                                    :
                                    ''
                            }

                            {
                                signedInUser.roles[0] === 'PRINCIPAL_INVESTIGATOR' || signedInUser.roles[0] === 'REGIONAL_INVESTIGATOR' || signedInUser.roles[0] === 'CO_INVESTIGATOR' ?
                                    <FormControl sx={{ m: 1, minWidth: 130 }} size="small">
                                        <InputLabel id="demo-select-small">Investigator</InputLabel>
                                        <Select
                                            labelId="demo-select-small"
                                            id="demo-select-small"
                                            value={investigator}
                                            label="Investigator"
                                            // onChange={handleMenuItemChange}
                                            onChange={(e) => { handleMenuItemChange(e) }}
                                        >
                                            {investList}
                                        </Select>
                                    </FormControl>
                                    :
                                    ''
                            }
                        </div>

                        <RestartAltRoundedIcon style={{ color: '#1e85b2', cursor: 'pointer' }} onClick={handleReset} />
                        <SendRoundedIcon style={{ color: '#1e85b2', cursor: 'pointer' }} onClick={handleSubmit} />
                    </Grid>
                    {/* <Grid item xs={1}> */}
                    {/* <SendRoundedIcon onClick={handleSubmit} /> */}
                    {/* </Grid> */}
                </Grid>
            </div>
            <div className='ecrf-table-main'>
                <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                    <TableContainer style={{}}>

                        {
                            patientInvData.isStarted ?
                                <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100vh' }}>
                                    <CircularProgress size={30} />
                                    <p style={{ color: 'white' }}>Loading...</p>
                                </Box>
                                :
                                patientData.length > 0 ?
                                    <>
                                        <Table aria-label="simple table" style={{ marginBottom: 20 }} >
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell align='left'>SUBJECT ID</TableCell>
                                                    <TableCell align="left">AGE/GENDER</TableCell>
                                                    <TableCell align="left">LOCATION/CITY</TableCell>
                                                    <TableCell align="left">VISIT DATE/TIME</TableCell>
                                                    <TableCell align="left">PATIENT NAME</TableCell>
                                                    <TableCell align="left">REGION</TableCell>
                                                    <TableCell align="left">STATUS</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {patientData.map((row) => (
                                                    <TableRow
                                                        key={row.name}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <TableCell component="th" scope="row">
                                                            {row.subject}
                                                        </TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.age}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.location}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{getDateTime(row.visit)}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.patName}</TableCell>
                                                        <TableCell align="left" sx={{ opacity: 0.6 }}>{row.region}</TableCell>
                                                        <TableCell align="left">
                                                            <Button variant="contained" style={row.status === 'NEW' ?
                                                                { cursor: 'default', textTransform: 'none', marginRight: 37, backgroundColor: '#00ac0a', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #00ac0a', borderRadius: 5, padding: '9px 20px 7px' }
                                                                :
                                                                row.status === 'NOACTION' ?
                                                                    { cursor: 'default', textTransform: 'none', marginRight: 37, backgroundColor: '#cb4154', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #cb4154', borderRadius: 5, padding: '9px 20px 7px' }
                                                                    :
                                                                    { cursor: 'default', border: '1px solid #b7b7b7', textTransform: 'none', marginRight: 37, backgroundColor: 'white', width: 60, height: 30, fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#9a9a9d', border: 'solid 1px #9a9a9d', borderRadius: 5, padding: '9px 8px 7px 9px' }}
                                                            >{row.status === 'NEW' ? 'New' : ''}{row.status === 'NOACTION' ? 'Initiated' : ''}{row.status === 'REVIEWED' ? 'Reviewed' : ''}</Button>
                                                            <Button variant="contained" style={{ width: 52, height: 30, textTransform: 'none', backgroundColor: '#288ec0', fontFamily: 'sans-serif', fontSize: 10, fontWeight: 700, fontStretch: 'normal', color: '#fff', border: 'solid 1px #288ec0f', borderRadius: 5, padding: '9x 15px 7px' }} onClick={() => { dispatch(set_ToggleOthers(checked)); handleViewVisit(row.patientObj) }}>
                                                                View
                                                            </Button>

                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>

                                    </>
                                    :
                                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100vh' }}>
                                        <h3 > No data !!</h3>
                                    </div>
                        }
                        {/* <Table sx={{ minWidth: 650 }} aria-label="simple table"> */}

                    </TableContainer>
                    <Stack spacing={2} style={{ margin: '10px 0px' }}>
                        <Pagination count={totalPages} defaultPage={page + 1} color="primary" onChange={handleChangePage}
                            key={`slider-${page}`} />
                    </Stack>
                </Paper>
            </div>
        </>

    )


}

export default PItable;