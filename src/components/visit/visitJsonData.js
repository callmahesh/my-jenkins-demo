import { height } from "@mui/system";

export function formatVisitDetails(visitArray){
    // console.log('formatVisitDetails->',visitArray);
    let visit = visitArray;
    let respJson = {};
    let ek12Determination = visit.analysisEkDetermination ? getEkDetermination(visit.analysisEkDetermination) : '';
    let basicVitals = [
        { label: 'HEIGHT (IN CM)', value: visit.height ? visit.height: '00.0' },
        { label: 'WEIGHT (IN KG)', value: visit.weight ? visit.weight: '00.0' },
        { label: 'BMI (KG/M2)', value: visit.bmi ? visit.bmi: '00.0' },
        { label: 'WAIST CIRCUMFERENCE (CM)', value: visit.waistCircumference ? visit.waistCircumference: '00.0' },
    ];
    let lifestyle = [
        { label: 'Alcohol Consumption', value: visit.alcoholConsumption ? visit.alcoholConsumption: 'NA' ,periodValue: visit.alcoholConsumptionSince?visit.alcoholConsumptionSince:'',qtyValue: visit.alcoholConsumptionQty?visit.alcoholConsumptionQty:'', freq: visit.alcoholConsumptionFreq},
        { label: 'Tobacco Consumption', value: visit.tobacoConsumption ? visit.tobacoConsumption: 'NA', periodValue: visit.tobacoConsumptionSince, freq: '' },
        { label: 'Smoking', value: visit.smoking ? visit.smoking: 'NA', periodValue:visit.smokingSince?visit.smokingSince:'' , freq: 'daily', qtyValue: visit.smokingQtyPerDay?visit.smokingQtyPerDay+' cigs':'NA' },
        { label: 'Dietary Habits', value: visit.dietaryHabbit ? visit.dietaryHabbit: 'NA' } 
    ];
    let familyHistory = [
        { label: 'Cardiovascular events', value: visit.cardioEventFamily ? visit.cardioEventFamily: 'NA' },
        { label: 'Hypertension', value: visit.hypTensionFamily ? visit.hypTensionFamily: 'NA' },
        { label: 'Dyslipidemia', value: visit.dysLipidemiaFamily ? visit.dysLipidemiaFamily: 'NA' },
        { label: 'Diabetes', value: visit.diabetesFamily ? visit.diabetesFamily: 'NA' },
        { label: 'Any Other History', value: visit.anyOtherFamilyHist ? visit.anyOtherFamilyHist: 'NA' }
    ];
    let medical = [
        { label: 'Diabetes Mellitus', value: visit.diabetesSelf ? visit.diabetesSelf : 'No', periodValue: visit.diabetesSelfSince ? visit.diabetesSelfSince : ' ',
         onTreatmentValue: visit.diabetesFamilyOnTreatment ? visit.diabetesFamilyOnTreatment : 'No' },
        { label: 'Hypertension', value: visit.hypTensionSelf ? visit.hypTensionSelf : 'No' , periodValue: visit.hypTensionSelfSince ? visit.hypTensionSelfSince : ' ',
        onTreatmentValue: visit.hypTensionFamilyOnTreatmen ? visit.hypTensionFamilyOnTreatmen : 'No' },
        { label: 'Dyslipidemia', value: visit.dysLipidemiaSelf ? visit.dysLipidemiaSelf : 'No', periodValue: visit.dysLipidemiaSelfSince ? visit.dysLipidemiaSelfSince : ' ',
        onTreatmentValue: visit.dysLipidemiaFamilyOnTreatment ? visit.dysLipidemiaFamilyOnTreatment : 'No'},
        { label: 'Obesity', value: visit.obesitySelf ? visit.obesitySelf : 'No' , periodValue: visit.obesitySelfSince ? visit.obesitySelfSince : ' ',
        onTreatmentValue: visit.obesityOnTreatment ? visit.obesityOnTreatment : 'No'}, 
        { label: 'Chronic Kidney Disease', value: visit.kidneyDiseaseSelf ? visit.kidneyDiseaseSelf : 'No' , periodValue: visit.kidneyDiseaseSelfSince ? visit.kidneyDiseaseSelfSince : ' ',
        onTreatmentValue: visit.kidneyDiseaseOnTreatment ? visit.kidneyDiseaseOnTreatment : 'No'},
        { label: 'Coronary Heart Disease', value: visit.coronaryHeartDiseaseSelf ? visit.coronaryHeartDiseaseSelf : 'No', periodValue: visit.coronaryHeartDiseaseSelfSince ? visit.coronaryHeartDiseaseSelfSince : ' ',
        onTreatmentValue: visit.coronaryHeartDiseaseOnTreatment ? visit.coronaryHeartDiseaseOnTreatment : 'No' },
        { label: 'Arrhythmias', value: visit.arythmiaSelf ? visit.arythmiaSelf : 'No' , periodValue: visit.arythmiaSelfSince ? visit.arythmiaSelfSince : ' ',
        onTreatmentValue: visit.arythmiaOnTreatment ? visit.arythmiaOnTreatment : 'No'},
        { label: 'Heart Failure', value: visit.heartFailureSelf ? visit.heartFailureSelf : 'No' , periodValue: visit.heartFailureSelfSince ? visit.heartFailureSelfSince : ' ',
        onTreatmentValue: visit.heartFailureOnTreatment ? visit.heartFailureOnTreatment : 'No'},
        { label: 'Rheumatoid Arthritis', value: visit.arthritisSelf ? visit.arthritisSelf : 'No', periodValue: visit.arthritisSelfSince ? visit.arthritisSelfSince : ' ',
        onTreatmentValue: visit.arthritisOnTreatment ? visit.arthritisOnTreatment : 'No' },
        { label: 'Any Other Medical History', value: visit.anyOtherMedicalHistory ? visit.anyOtherMedicalHistory : 'NA' }
    ];
    let medicalLab = [
        { label: 'Diabetes Mellitus', value: visit.diabetesHbA1cLab ? `HBA1c    ${visit.diabetesHbA1cLab}%` : 'NA' },
        { label: 'Hypertension', value: visit.hypertensionSBPLab ? `${visit.hypertensionSBPLab}/${visit.hypertensionDBPLab}`: 'NA' },
        { label: 'Dyslipidemia', value: `${visit.dyslipidemiaTCLab?'TC - '+visit.dyslipidemiaHDLCLab+' mg/dl':'NA'} ${visit.dyslipidemiaHDLCLab?'↵HDL - C - '+visit.dyslipidemiaHDLCLab+' mg/dl':''} ${visit.dyslipidemiaLDLCLab?'↵LDL - C - '+visit.dyslipidemiaLDLCLab+' mg/dl':''} ${visit.triglyceridesLab?'↵TG - '+visit.triglyceridesLab+' mg/dl':''}`},
        { label: 'Obesity', value: visit.bmi ? visit.bmi+"" : 'NA' },
        { label: 'Afib', value: visit.afibLab != undefined ? (visit.afibLab ? 'Yes' : 'No') : 'NA' },
        { label: 'Heart Rate', value: visit.heartRateLab ? visit.heartRateLab+" BPM" : 'NA' }
    ];
    let symptomatic = [
        { label: 'Chest pain', value: visit.symptomaticChestPain? visit.symptomaticChestPain :false },
        { label: 'Syncope', value: visit.symptomtomaticSyncope? visit.symptomtomaticSyncope :false },
        { label: 'Palpitation', value: visit.symptomtomaticPalpitation? visit.symptomtomaticPalpitation :false },
        { label: 'Shortness of breath', value: visit.symptomtomaticShortnessOofBreath? visit.symptomtomaticShortnessOofBreath :false },
        { label: 'Dizziness or Light headedness', value: visit.symptomtomaticDizziness? visit.symptomtomaticDizziness :false },
        { label: 'Weakness or Fatigue', value: visit.symptomtomaticWeakness? visit.symptomtomaticWeakness :false },
        { label: 'Nocturnal Symptoms (Palpitation, Shortness of Breath, Chest Pain)', value: visit.symptomtomaticNocternal? visit.symptomtomaticNocternal :false },
        { label: 'Post Prandial', value: visit.symptomtomaticPostPrandial? visit.symptomtomaticPostPrandial :false },
        { label: 'Diaphoresis', value: visit.symptomtomaticDiaphoresis? visit.symptomtomaticDiaphoresis :false },
        { label: visit.symptomtomaticOther ? visit.symptomtomaticOther : '' ,value: (visit.symptomtomaticOther!='') && (visit.symptomtomaticOther!=null) && (!visit.symptomtomaticOther.includes("false")) ? true : false},
        { label: visit.aSymptomaticOther ? visit.aSymptomaticOther : '' ,value: ((visit.aSymptomaticOther!='') && (visit.aSymptomaticOther!=null) && !visit.aSymptomaticOther.includes("false")) ? true : false}
    ];
    let occurrence = [
        { label: 'Stable Angina', value: visit.eventStableAnginia? visit.eventStableAnginia :false  },
        { label: 'MI', value: visit.eventMI? visit.eventMI :false  },
        { label: 'Stroke', value: visit.eventStroke? visit.eventStroke :false  },
        { label: 'Left Ventricular Dysfunction', value: visit.eventLeftVentricularDysfunction? visit.eventLeftVentricularDysfunction :false  },
        { label: 'No Event', value: visit.eventNoEvent? visit.eventNoEvent :false  },
        { label: visit.eventOther, value: visit.eventOther? true :false  }
    ]
    let analysis = [
        { label: '6Lead Analysis', value: visit.analysis6L? ecgAnalysisFormatter(visit.analysis6L) : '' },
        { label: 'V1 Analysis', value: visit.analysisV1? ecgAnalysisFormatter(visit.analysisV1) : '' },
        { label: 'V6 Analysis', value: visit.analysisV6? ecgAnalysisFormatter(visit.analysisV6) : '' }
    ]
    let ek12Analysis = [
        { label: 'Noise', value: visit.analysisEk12Noise ? visit.analysisEk12Noise : '' },
        { label: 'Afib', value: visit.analysisEk12Afib ? visit.analysisEk12Afib : '' },
        { label: 'Tachy', value: visit.analysisEk12Tachy ? visit.analysisEk12Tachy : '' },
        { label: 'Brady', value: visit.analysisEk12Brady ? visit.analysisEk12Brady : '' },
        { label: 'Pause', value: visit.analysisEk12Pause ? visit.analysisEk12Pause : '' },
        { label: 'Vt', value: visit.analysisEk12Vt ? visit.analysisEk12Vt : '' },
        { label: 'Pace', value: visit.analysisEk12Pace ? visit.analysisEk12Pace : '' },
        { label: 'Sinus', value: visit.analysisEk12Sinus ? visit.analysisEk12Sinus : '' },
        { label: 'Other Comments', value: visit.analysisEkComments ? visit.analysisEkComments : '' },
        { label: 'Error', value: visit.analysisEk12Comments ? visit.analysisEk12Comments : '' }
    ]
    let qtAnalysis = [
        { label: 'RR', value: visit.analysisQtRr ? visit.analysisQtRr : '' },
        { label: 'QT', value: visit.analysisQtQt ? visit.analysisQtQt : '' },
        { label: 'QTcF', value: visit.analysisQtQtcf ? visit.analysisQtQtcf : '' },
        { label: 'QTcB', value: visit.analysisQtQtcb ? visit.analysisQtQtcb : '' },
        { label: 'Error', value: visit.analysisEk12Comments ? visit.analysisEk12Comments : '' }
    ]
    respJson = { basicVitals, lifestyle, medical, medicalLab, symptomatic, occurrence, analysis, ek12Analysis, qtAnalysis, ek12Determination, familyHistory };
    return respJson;
}

///////////////////////////////

function ecgAnalysisFormatter(ecgAnalysis){
    switch(ecgAnalysis.toUpperCase()){
        case "NORMAL" : return "Normal"; break;
        case "AFIB" : return "Afib"; break;
        case "UNCLASSIFIED" : return "Unclassified"; break;
        case "TACHYCARDIA" : return "Tachycardia"; break;
        case "BRADYCARDIA" : return "Bradycardia"; break;
        case "SHORT" : return "Short"; break;
        case "LONG" : return "Long"; break;
        case "UNREADABLE" : return "Unreadable"; break;
        case "NO_ANALYSIS" : return "No Analysis"; break;
        default: return ecgAnalysis;
    }
}

function getEkDetermination(ekArray){
    const myArray = ekArray.split("_");
    let newArray = [];
    for(let element of myArray){
        newArray.push({label:element,value: element? true:false});
    }
    return newArray;
}


export const basicViatals = [
    { label: 'HEIGHT (IN CM)', value: '00.0' },
    { label: 'WEIGHT (IN KG)', value: '00.0' },
    { label: 'BMI (KG/M2)', value: '00.0' },
    { label: 'WAIST CIRCUMFERENCE (CM)', value: '00.0' },
]

export const lifestyle = [
    { label: 'Alcohol Consumption', value: 'Yes' },
    { label: 'Tobacco Consumption', value: 'No' },
    { label: 'Smoking', value: 'No' },
    { label: 'Dietary Habits', value: 'Vegetarian' },
    { label: 'Dietary Habits', value: 'Vegetarian' },   
]

// export const familyHistory = [
//     { label: 'Cardiovascular events', value: 'Yes' },
//     { label: 'Hypertension', value: 'No' },
//     { label: 'Dyslipidemia', value: 'No' },
//     { label: 'Diabetes', value: 'Vegetarian' }, 
// ]

export const medical = [
    { label: 'Diabetes Mellitus', value: 'HBA1c    7.1%' },
    { label: 'Hypertension', value: '120/80' },
    { label: 'Dyslipidemia', value: 'TC - HDL - C -mg/dl LDL - C -mg/dl' },
    { label: 'Obesity', value: 'No' },
    { label: 'Chronic Kidney Disease', value: 'No' },
    { label: 'Coronary Heart Disease', value: 'No' },
    { label: 'Arrhythmias', value: 'No' },
    { label: 'Heart Failure', value: 'No' },
    { label: 'Rheumatoid Arthritis', value: 'No' }
]

export const symptomatic = [
    { label: 'Chest pain', value: '' },
    { label: 'Syncope', value: '' },
    { label: 'Palpitation', value: '' },
    { label: 'Shortness of breath', value: '' },
    { label: 'Dizziness or Light headedness', value: '' },
    { label: 'Weakness or Fatigue', value: '' },
    { label: 'Nocturnal Symptoms (Palpitation, Shortness of Breath, Chest Pain)', value: '' },
]

export const occurrence = [
    { label: 'Stable Angina', value: '' },
    { label: 'MI', value: '' },
    { label: 'Stroke', value: '' },
    { label: 'Left Ventricular Dysfunction', value: '' },
]