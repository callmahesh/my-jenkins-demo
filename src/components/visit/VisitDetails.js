import * as React from 'react';
import "./VisitDetails.scss";
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import { formatVisitDetails } from './visitJsonData'
import PatientDetails from '../patient/PatientDetails';
import EcrfHeader from '../header/EcrfHeader';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getPatientVisits } from '../../redux/apiCall/patientAPIs';
import { reset_GetPatientVisits } from '../../redux/reducers/patientSlice'
import pdfIconSvg from '../../assets/images/pdfIcon.svg';
import { useNavigate } from 'react-router-dom';
import VisitButton from '../button/VisitButton';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';
import { Typography } from '@mui/material';

const VisitDetails = (props) => {

    const { visitId, uName } = useParams();
    // let {basicViatals} = vsitJsonData;

    const navigate = useNavigate();

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    // const userDetails = useSelector((state) => state.employee.user_details.value);
    const patientVisitsData = useSelector((state) => state.patient.patientVisits);

    const toggleStateInv = useSelector((state) => state.employee.toggle.investigator);

    // const toggleState = useSelector((state) => state.employee.toggle.others);

    let { value: patientVisits } = patientVisitsData;

    const [patVisit_format, setPatVisit_format] = React.useState({});
    const [patientVisitId, setPatientVisitId] = React.useState(0);
    const [ecgAvailable, setEcgAvailable] = React.useState(true);

    const [dontShowMedLab, setDontShowMedLab] = React.useState(false);
    const [dontShowFamHistory, setDontShowFamHistory] = React.useState(false);

    const userDetails = JSON.parse(sessionStorage.getItem('eCRF_uDetails'));

    let jwt_token = sessionStorage.getItem('eCRF_token');

    // console.log('VisitDetails-> ', visitId, uName);

    React.useEffect(() => {
        console.log(localStorage.getItem("dontShowMedLab"));
        if (jwt_token) {
            if (visitId) {
                dispatch(getPatientVisits(visitId, jwt_token));
            } else {
                navigate(`/home`);
            }

        } else {
            navigate(`/login`);
        }
        return () => {
            // console.log('cleaned up');
            dispatch(reset_GetPatientVisits());
        };
    }, [])


    React.useEffect(() => {

        setDontShowMedLab(localStorage.getItem("dontShowMedLab") == 'true');
        setDontShowFamHistory(localStorage.getItem("patientType") ? localStorage.getItem("patientType") === 'REGULAR' ? true : false : false);

    }, [])

    console.log('VISIT: dntShowFamHistory: ', dontShowFamHistory, ', PatType: ', localStorage.getItem("patientType"));

    // let patVisit_format = {};

    React.useEffect(() => {
        let tempObj = {};
        // console.log('useEffect-> ', ('patData' in props.patData ), ( props.patData.length > 0))
        if (patientVisits.length > 0) {
            tempObj = formatVisitDetails(patientVisits[0]);
            // console.log('patVisit_format-->1', tempObj, patVisit_format);
            setPatientVisitId(patientVisits[0].id);
            setEcgAvailable(patientVisits[0].ecgReportAvailable);
            // console.log('patVisit_format-->2', patVisit_format ,('basicVitals' in patVisit_format),  patVisit_format.basicVitals.length > 0)
        }
        setPatVisit_format(tempObj);

    }, [patientVisits])

    const [symptomaticList, setSymptomaticList] = React.useState([]);
    const [minOneSymptom, setMinOneSymptom] = React.useState(false);

    React.useEffect(() => {
        if (patVisit_format && ('symptomatic' in patVisit_format) && patVisit_format.symptomatic.length > 0) {
            //if (symtomatic.length > 0) {
            let symptomaticList1 = patVisit_format.symptomatic.map((item) => {

                if (item.value) {
                    setMinOneSymptom(true);
                    return <Grid item xs={6} sm={4} md={3} key={item.label}>
                        <h3 style={{ padding: "0px", margin: '5px', fontWeight: 400 }}>{item.label}</h3>
                    </Grid>
                }


            });
            setSymptomaticList(symptomaticList1);
        }

    }, [patVisit_format])


    let basicList = [];
    if (patVisit_format && ('basicVitals' in patVisit_format) && patVisit_format.basicVitals.length > 0) {
        basicList = patVisit_format.basicVitals.map((item) =>
            <Grid item xs={6} sm={4} md={3} key={item.label}>
                <TextField
                    id="standard-read-only-input"
                    label={item.label}
                    value={item.value}
                    style={{ marginTop: '10px', marginLeft: '0.2em' }}
                    InputProps={{
                        readOnly: true,
                    }}
                    variant="standard"
                />
            </Grid>
        );
    }

    const LifeStyleListExpanded = (props) => {
        if (props.expand === 'Yes') {

            return (
                <Tooltip
                    title={<>

                        <p style={{ whiteSpace: 'pre-line', color: 'rgba(0, 0, 0, 0.6)', marginLeft: '0.8em', textTransform: 'uppercase' }}>SINCE WHEN:     &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;  {props.freq}:</p>
                        <p style={{ whiteSpace: 'pre-line', fontSize: '2em', marginLeft: '0.3em' }}>{props.periodValue} {props.periodValue > 1 ? 'Years' : 'Year'}    &emsp; &emsp; {props.qtyValue}</p>
                    </>}
                    componentsProps={{
                        tooltip: {
                            sx: {
                                color: "black",
                                backgroundColor: "white"
                            }
                        }
                    }}
                    arrow>
                    <TextField
                        id="standard-read-only-input"
                        label={props.label}
                        multiline
                        value={props.value}
                        style={{ marginTop: '10px', marginLeft: '0.2em' }}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"

                    />
                </Tooltip>
            )
        } else {
            return (
                <TextField
                    id="standard-read-only-input"
                    label={props.label}
                    multiline
                    style={{ marginTop: '10px', marginLeft: '0.2em' }}
                    value={props.value}
                    InputProps={{
                        readOnly: true,
                    }}
                    variant="standard"
                />
            )
        }
    }

    let lifestyleList = [];
    if (patVisit_format && ('lifestyle' in patVisit_format) && patVisit_format.lifestyle.length > 0) {
        //if (lifestyle.length > 0) {
        lifestyleList = patVisit_format.lifestyle.map((item) =>
            <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: '5px' }} >
                <LifeStyleListExpanded expand={item.value} label={item.label} value={item.value} freq={item.freq} qtyValue={item.qtyValue} periodValue={item.periodValue} />
            </Grid>
        );
    }
    let familyHistoryList = [];
    if (patVisit_format && ('familyHistory' in patVisit_format) && patVisit_format.familyHistory.length > 0) {
        //if (familyHistory.length > 0) {
        familyHistoryList = patVisit_format.familyHistory.map((item) => {
            if (item.value != 'NA') {
                return <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: 0 }}>
                    <TextField
                        id="standard-read-only-input"
                        label={item.label}
                        value={item.value}
                        style={{ marginTop: '10px', marginLeft: '4px' }}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"
                    />
                </Grid>
            }

        });
        familyHistoryList = familyHistoryList.filter(item=>{
            if(item) return item;
        })
    }

    const MedicalListExpanded = (props) => {
        if (props.expand === 'Yes') {

            return (
                <Tooltip
                    title={<>

                        <p style={{ whiteSpace: 'pre-line', color: 'rgba(0, 0, 0, 0.6)', marginLeft: '0.8em' }}>SINCE WHEN:                   &emsp;  ON GOING TREATMENT:</p>
                        <p style={{ whiteSpace: 'pre-line', fontSize: '2em', marginLeft: '0.3em' }}>{props.periodValue}  {props.periodValue > 1 ? 'Years' : 'Year'}   &emsp; &emsp; {props.treatmentValue}</p>
                    </>}
                    componentsProps={{
                        tooltip: {
                            sx: {
                                color: "black",
                                backgroundColor: "white"
                            }
                        }
                    }}
                    arrow>
                    <TextField
                        id="standard-read-only-input"
                        label={props.label}
                        multiline
                        value={props.value}
                        style={{ marginTop: '10px', marginLeft: '0.2em' }}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"

                    />
                </Tooltip>
            )
        } else {
            return (
                <TextField
                    id="standard-read-only-input"
                    label={props.label}
                    multiline
                    style={{ marginTop: '10px', marginLeft: '0.2em' }}
                    value={props.value}
                    InputProps={{
                        readOnly: true,
                    }}
                    variant="standard"
                />
            )
        }
    }

    let medicalList = [];
    if (patVisit_format && ('medical' in patVisit_format) && patVisit_format.medical.length > 0) {
        //if (medical.length > 0) {
        medicalList = patVisit_format.medical.map((item) => {
            if (item.value != 'NA') {
                return <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: 0 }}>
                    <MedicalListExpanded expand={item.value} label={item.label} value={item.value} periodValue={item.periodValue} treatmentValue={item.onTreatmentValue} labLabel={item.labLabel} labValue={item.labValue} />
                </Grid>
            }

        });

    }

    let medicalLabList = [];
    if (patVisit_format && ('medicalLab' in patVisit_format) && patVisit_format.medicalLab.length > 0) {
        medicalLabList = patVisit_format.medicalLab.map((item) =>
            <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: 0 }}>
                <TextField
                    id="standard-read-only-input"
                    label={item.label}
                    value={item.value.replace(/↵/g, "\n")}
                    multiline
                    style={{ marginTop: '10px', marginLeft: '0.2em' }}
                    InputProps={{
                        readOnly: true,
                    }}
                    variant="standard"
                />
            </Grid>
        );
    }



    let analysisList = []; let analysisPresent = false;
    if (patVisit_format && ('analysis' in patVisit_format) && patVisit_format.analysis.length > 0) {
        //if (symtomatic.length > 0) {
        analysisList = patVisit_format.analysis.map((item) => {

            if (item.value) {
                analysisPresent = true;
                return <Grid item xs={6} sm={4} md={3} key={item.label}>
                    <TextField
                        id="standard-read-only-input"
                        label={item.label}
                        multiline
                        style={{ marginTop: '10px', marginLeft: '0.2em' }}
                        value={item.value}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"
                    />
                </Grid>
            }
        });
    }

    let ek12AnalysisList = []; let ek12AnalysisPresent = false;
    if (patVisit_format && ('ek12Analysis' in patVisit_format) && patVisit_format.ek12Analysis.length > 0) {
        //if (symtomatic.length > 0) {
        ek12AnalysisList = patVisit_format.ek12Analysis.map((item) => {

            if (item.value) {
                ek12AnalysisPresent = true;
                return <Grid item xs={6} sm={4} md={3} key={item.label}>
                    <TextField
                        id="standard-read-only-input"
                        label={item.label}
                        multiline
                        style={{ marginTop: '10px', marginLeft: '0.2em' }}
                        value={item.value}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"
                    />
                </Grid>
            }
        });
    }

    let qtAnalysisList = []; let qtAnalysisPresent = false;
    if (patVisit_format && ('qtAnalysis' in patVisit_format) && patVisit_format.qtAnalysis.length > 0) {
        //if (symtomatic.length > 0) {
        qtAnalysisList = patVisit_format.qtAnalysis.map((item) => {

            if (item.value) {
                qtAnalysisPresent = true;
                return <Grid item xs={6} sm={4} md={3} key={item.label}>
                    <TextField
                        id="standard-read-only-input"
                        label={item.label}
                        multiline
                        style={{ marginTop: '10px', marginLeft: '0.2em' }}
                        value={item.value}
                        InputProps={{
                            readOnly: true,
                        }}
                        variant="standard"
                    />
                </Grid>
            }
        });
    }

    let ek12DeterminationList = []; let ek12DeterminationPresent = false;
    if (patVisit_format && ('ek12Determination' in patVisit_format) && patVisit_format.ek12Determination.length > 0) {
        //if (symtomatic.length > 0) {
        ek12DeterminationList = patVisit_format.ek12Determination.map((item) => {

            if (item.value) {
                ek12DeterminationPresent = true;
                return <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: 0 }}>
                    <h3 style={{ padding: "0px", margin: '5px', fontWeight: 400 }}>{item.label}</h3>
                </Grid>
            }
        });
    }

    let occurrenceList = []; let occurrence = false;
    if (patVisit_format && ('occurrence' in patVisit_format) && patVisit_format.occurrence.length > 0) {
        occurrenceList = patVisit_format.occurrence.map((item) => {
            if (item.value) {
                occurrence = true;
                return <Grid item xs={6} sm={4} md={3} key={item.label} style={{ paddingTop: 0 }}>
                    <h3 style={{ padding: "0px", margin: '5px', fontWeight: 400 }}>{item.label}</h3>
                </Grid>
            }
        }

        );
    }

    const handleViewVisit = (patObj) => {
        // console.log("this is working fine");
        // console.log('handleViewVisit-> ', patObj.id);
        setPatientVisitId(patObj.id)
        setEcgAvailable(patObj.ecgReportAvailable);
        // console.log('useEffect-> ', ('patData' in props.patData ), ( props.patData.length > 0))
        if (Object.keys(patObj).length > 0) {
            let tempObj = formatVisitDetails(patObj);
            // console.log('patVisit_format-->1', tempObj);
            setPatVisit_format(tempObj);
        }

    }

    /* Handler for View Pdf Report Icon, redirects to ECGReportLayout */
    const handleViewReport = () => {
        // console.log('handleViewReport:')
        // window.location = "/visit";
        // dispatch(set_PatientDetails(patObj));
        // if(patObj && 'id' in patObj){
        navigate(`/report/${patientVisitId}/${visitId}/${uName}`);
        // }

    };

    
    return (
        <div className='VisitDetails' style={{ marginBottom: '20px', marginTop: '58px' }}>
            <div >
                <EcrfHeader />
            </div>

            <div >
                {
                    patientVisitsData.isStarted ?
                        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column', height: '100vh' }}>
                            <CircularProgress size={30} />
                            <p style={{ color: 'white' }}>Loading...</p>
                        </Box>
                        :
                        patientVisits.length > 0 ?

                            <Grid container spacing={2} style={{ margin: '20px 20px 0px 20px' }}>
                                <Grid container item xs={12} sx={{ paddingLeft: 0 }}>
                                    <PatientDetails />
                                </Grid>
                                <Grid item xs={12}
                                    style={{ padding: '0px 30px', margin: '0px', marginLeft: '-45px' }}
                                >
                                    <VisitButton key={patientVisits.id} patData={patientVisits.length > 0 ? patientVisits : []} handleBtnClick={handleViewVisit} />
                                </Grid>
                                <Grid item xs={12} style={{ margin: '0px 20px', padding: '13px 0px' }}>
                                    <div style={{ background: '#f8f9fb' }}>
                                        <span style={{ color: '#288ec0' }}>Demographic Details</span>
                                    </div>
                                </Grid>
                                <Grid container item xs={12} rowSpacing={2} sx={{ paddingLeft: 0 }}>
                                    {basicList}
                                </Grid>
                                <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                    <div style={{ background: '#f8f9fb' }}>
                                        <span style={{ color: '#288ec0' }}>Lifestyle Factors</span>
                                    </div>
                                </Grid>
                                <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}  >
                                    {lifestyleList}
                                </Grid>

                                {
                                    dontShowFamHistory || familyHistoryList.length == 0?
                                    null:
                                    familyHistoryList.length > 0?
                                    <>
                                        <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                            <div style={{ background: '#f8f9fb' }}>
                                                <span style={{ color: '#288ec0' }}>Family History</span>
                                            </div>
                                        </Grid>
                                        <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }} >
                                            {familyHistoryList}
                                        </Grid>
                                    </>
                                    : null
                                }

                                <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                    <div style={{ background: '#f8f9fb' }}>
                                        <span style={{ color: '#288ec0' }}>Medical History</span>
                                    </div>
                                </Grid>
                                <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}>
                                    {medicalList}
                                </Grid>

                                {
                                    dontShowMedLab && userDetails.roles[0] === 'INVESTIGATOR' ?
                                        null
                                        :
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>Medical History Data</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}>
                                                {medicalLabList}
                                            </Grid>
                                        </>
                                }
                                {
                                    symptomaticList.length > 0 && minOneSymptom ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>Symptomatic</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}>
                                                {symptomaticList}
                                            </Grid>
                                        </>
                                        :
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>Symptomatic</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '1%', marginLeft: '0.2%', fontFamily: 'sans-serif' }}>
                                                <Typography>No Symptoms</Typography>
                                            </Grid>
                                        </>
                                }
                                {
                                    analysisList.length > 0 && analysisPresent ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>Kardia Analysis</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ marginTop: '-2%' }}>
                                                {analysisList}
                                            </Grid>
                                        </>
                                        :

                                        null
                                }
                                {
                                    qtAnalysisList.length > 0 && qtAnalysisPresent ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>QT Analysis</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ marginTop: '-2%', marginBottom: '2%' }}>
                                                {qtAnalysisList}
                                            </Grid>
                                        </>
                                        :
                                        null
                                }
                                {/* {
                                    ek12AnalysisList.length > 0 && ek12AnalysisPresent ?
                                        <>
                                            <Grid item xs={12}  style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>EK12 Analysis</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ marginTop:'-2%', marginBottom: '2%' }}>
                                                {ek12AnalysisList}
                                            </Grid>
                                        </>
                                        :
                                        null
                                } */}
                                {
                                    ek12DeterminationList.length > 0 && ek12DeterminationPresent ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>EK12 Determination</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}>
                                                {ek12DeterminationList}
                                            </Grid>
                                        </>
                                        :
                                        null
                                }
                                {
                                    occurrence ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '20px 20px 0px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>Occurrence of any event</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ paddingTop: '5px' }}>
                                                {occurrenceList}
                                            </Grid>
                                        </>
                                        :
                                        ''
                                }
                                {
                                    ecgAvailable ?
                                        <>
                                            <Grid item xs={12} style={{ margin: '0px 20px 20px 20px', padding: '13px 0px' }}>
                                                <div style={{ background: '#f8f9fb' }}>
                                                    <span style={{ color: '#288ec0' }}>ECG Report</span>
                                                </div>
                                            </Grid>
                                            <Grid container item xs={12} rowSpacing={2} style={{ marginBottom: 10 }}>
                                                <img src={pdfIconSvg} style={{ cursor: 'pointer' }} onClick={handleViewReport} alt='pdf' />
                                            </Grid>
                                        </>
                                        :
                                        ''
                                }
                            </Grid>
                            :
                            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '100vh' }}>
                                <h3 > No visit...</h3>
                            </div>
                }
            </div>
        </div>
    );
}


export default VisitDetails;