import React, { useContext } from 'react';
import doctor from '../../assets/images/doctor.png';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import "./SimpleLogin.scss";
import { POST_SIGN_IN } from '../../config/urlConfig';
import { set_UserDetails } from '../../redux/reducers/employeeSlice';
import ImpactSnackBar from '../snackbar/ImpactSnackBar';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { ModalContext } from '../../utils/utilsOfModal/ModalContext';


const SimpleLogin = () => {

    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const jwt_token = useSelector((state) => state.employee.user_details.value);

    const [messageState, setMessageState] = React.useState('');
    const [severityState, setSeverityState] = React.useState('');
    const [displayState, setDisplayState] = React.useState(false);

    const navigate = useNavigate();

    // console.log('jwt_token: ',jwt_token);
    const { setCurrentModal } = useContext(ModalContext);
    const openModal = ({ name, props }) => setCurrentModal({ name, props });

    const handleRequestAccessBtn = () => {
        let title = "Request Access", content = '';
        openModal({ name: 'RequestAccessDialog', props: { title, content } });
    };

    const handleForgotPwdBtn = () => {
        let title = "Forgot Password", content = '';
        openModal({ name: 'ForgotPwdDialog', props: { title, content } });
    };

    const handleSubmitForm = e => {
        // console.log('HELLLOOOO')
        e.preventDefault();
        // console.log('handleSubmitForm-->', e.target.email.value, e.target.password.value);

        if (!e.target.email.value) {
            // alert("Email is required");
            setMessageState('Username is mandatory');
            setSeverityState('error');
            setDisplayState(true);
            // setToastData({ severity: 'error', message: `Email is required` });
            // setOpenToast(true);
        } else if (!e.target.password.value) {
            setMessageState('Password is mandatory');
            setSeverityState('error');
            setDisplayState(true);
            // setToastData({ severity: 'error', message: `Password is required` });
            // setOpenToast(true);
        } else {
            var obj = {
                "username": e.target.email.value, //"mithun.t@viewwiser.com",
                "password": e.target.password.value //"Sharingiscaring@6"
            }
            var apiUrl = new URL(POST_SIGN_IN);
            fetch(apiUrl, {
                // mode: "no-cors",
                // credentials: "include",
                // redirect: "manual",
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify(obj)
            }).then(res => {
                // console.log("RESPONSE: ", res);
                return res.json();

                // else if ( (res.status >= 200 && res.status <= 599)) return res.json();
                // else throw `Server error: [${res.status}] [${res.statusText}] [${res.url}]`;
            }).then(data => {
                // console.log("API RESPONSE postLoginValidation: ", data);
                if (data && 'token' in data) {
                    // console.log("Token: ", data.token);
                    sessionStorage.setItem('eCRF_token', data.token);
                    sessionStorage.setItem('eCRF_userId', data.id);
                    sessionStorage.setItem('eCRF_uDetails', JSON.stringify(data));
                    dispatch(set_UserDetails(data));
                    if(data.passwordChangeRequired){
                        navigate('/auth/resetPassword/'+data.resetToken);
                    }else{
                        navigate('/home');
                    }            
                } else {
                    // console.log("Error: ", data);
                    if (data && 'message' in data) {
                        // alert(`Error: ${data.message}`);
                        setMessageState(`Error: ${data.message}`);
                        setSeverityState('error');
                        setDisplayState(true);
                    } else {
                        setMessageState('Server Error');
                        setSeverityState('error');
                        setDisplayState(true);
                    }
                }
            }).catch(error => {
                console.error("Exception Login API: ", error);
                setMessageState(`Exception: ${error}`);
                setSeverityState('error');
                setDisplayState(true);
                // setToastData({ severity: 'error', message: error });
                // setOpenToast(true);
            });

        }
    };

    const handleToastClose = () => {
        
        setDisplayState(false);
        
    }

    return (
        <>
        <div className="mainContainer">
            <Box
                sx={{
                    boxShadow: 3,
                    width: '100%',
                    height: '100%',
                    margin: '0px',
                    bgcolor: (theme) => (theme.palette.mode === 'dark' ? '#101010' : '#fff'),
                    color: (theme) =>
                        theme.palette.mode === 'dark' ? 'grey.300' : 'grey.800',
                    p: 1,
                    m: 1,
                    borderRadius: 2,
                    textAlign: 'center',
                    fontSize: '0.875rem',
                    fontWeight: '700',
                }}
            >
                <Grid container direction="row" style={{ padding: "0px" }} rowSpacing={3} columnSpacing={{ xs: 6, sm: 6, md: 6 }}>

                    <Grid item xs={6}>
                        <div className="">
                            <img src={doctor} className="login-img" alt="" />
                        </div>
                    </Grid>

                    <Grid item xs={6} style={{ paddingLeft: "0px" }}>
                        <div >
                            <h1 className='header'>India ECG Registry</h1>
                            <h6 className='sub_header'>An Initiative by Eris</h6>
                            <form className="form" onSubmit={handleSubmitForm}>
                                <div className="input-group">
                                    <input type="text" name="email" placeholder="User ID" />
                                </div>
                                <div className="input-group">
                                    <input type="password" name="password" placeholder="Password" />
                                </div>
                                
                                <p className='forgot-password-css' onClick={handleForgotPwdBtn}>Forgot password?</p>
                                <button className="primary simple-button">Submit</button>
                            </form>
                            
                            <h6 className='Dont-have-an-accoun'>Don’t have an account?</h6>
                            <Button className='button-Request-access'
                                onClick={handleRequestAccessBtn}
                                variant="outlined"
                            >
                                Request Access
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </Box>
        </div>
        <ImpactSnackBar isOpen={displayState} message={messageState} handleClose={handleToastClose} severity={severityState} />
        </>
    );
}


export default SimpleLogin;
