import React, { useContext } from "react";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import "./EcrfLogin.scss";
import { POST_SIGN_IN } from "../../config/urlConfig";
import { set_UserDetails } from "../../redux/reducers/employeeSlice";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { ModalContext } from "../../utils/utilsOfModal/ModalContext";
import { Typography } from "@mui/material";
import TextField from "@mui/material/TextField";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import ImpactSnackBar from "../snackbar/ImpactSnackBar";
import logo from "../../assets/images/IECG_logo_new.png";
import { VERSION } from "../../config/constants";

const EcrfLogin = () => {
  /* Redux Action Dispatch Hooks */
  const dispatch = useDispatch();

  /* Redux State Selector Hooks */
  const jwt_token = useSelector((state) => state.employee.user_details.value);

  const [messageState, setMessageState] = React.useState("");
  const [severityState, setSeverityState] = React.useState("");
  const [displayState, setDisplayState] = React.useState(false);

  const navigate = useNavigate();

  const { setCurrentModal } = useContext(ModalContext);
  const openModal = ({ name, props }) => setCurrentModal({ name, props });

  const [values, setValues] = React.useState({
    amount: "",
    userId: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleSubmitForm = (e) => {
    // console.log('HELLLOOOO')
    e.preventDefault();
    // console.log('handleSubmitForm-->', values.userId, values.password);

    if (!values.userId) {
      // alert("Email is required");
      setMessageState("Username is mandatory");
      setSeverityState("error");
      setDisplayState(true);
      // setToastData({ severity: 'error', message: `Email is required` });
      // setOpenToast(true);
    } else if (!values.password) {
      setMessageState("Password is mandatory");
      setSeverityState("error");
      setDisplayState(true);
      // setToastData({ severity: 'error', message: `Password is required` });
      // setOpenToast(true);
    } else {
      var obj = {
        username: values.userId, //"mithun.t@viewwiser.com",
        password: values.password, //"Sharingiscaring@6"
      };
      var apiUrl = new URL(POST_SIGN_IN);
      fetch(apiUrl, {
        // mode: "no-cors",
        // credentials: "include",
        // redirect: "manual",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(obj),
      })
        .then((res) => {
          // console.log("RESPONSE: ", res);
          return res.json();

          // else if ( (res.status >= 200 && res.status <= 599)) return res.json();
          // else throw `Server error: [${res.status}] [${res.statusText}] [${res.url}]`;
        })
        .then((data) => {
          // console.log("API RESPONSE postLoginValidation: ", data);
          if (data && "token" in data) {
            // console.log("Token: ", data.token);
            sessionStorage.setItem("eCRF_token", data.token);
            sessionStorage.setItem("eCRF_userId", data.id);
            sessionStorage.setItem("eCRF_uDetails", JSON.stringify(data));
            dispatch(set_UserDetails(data));
            if (data.passwordChangeRequired) {
              sessionStorage.clear();
              navigate("/auth/resetPassword/" + data.resetToken);
            } else {
              if (data.roles[0] === "ADMIN") {
                sessionStorage.clear();
                sessionStorage.setItem("user", JSON.stringify(data));
                sessionStorage.setItem("token", data.token);
                window.location.href = "/ecrfAdmin/userdashboard";
                return;
              }
              navigate("/home");
            }
          } else {
            // console.log("Error: ", data);
            if (data && "message" in data) {
              // alert(`Error: ${data.message}`);
              setMessageState(`Error: ${data.message}`);
              setSeverityState("error");
              setDisplayState(true);
            } else {
              setMessageState("Server Error");
              setSeverityState("error");
              setDisplayState(true);
            }
          }
        })
        .catch((error) => {
          console.error("Exception Login API: ", error);
          setMessageState(`Exception: ${error}`);
          setSeverityState("error");
          setDisplayState(true);
          // setToastData({ severity: 'error', message: error });
          // setOpenToast(true);
        });
    }
  };

  const handleToastClose = () => {
    setDisplayState(false);
  };

  const handleForgotPwdBtn = () => {
    let title = "Forgot Password",
      content = "";
    openModal({ name: "ForgotPwdDialog", props: { title, content } });
  };

  const handleRequestAccessBtn = () => {
    let title = "Request Access",
      content = "";
    openModal({ name: "RequestAccessDialog", props: { title, content } });
  };

  const onKeyDownHandler = (e) => {
    if (e.keyCode === 13) {
      e.preventDefault();
      handleSubmitForm(e);
    }
  };

  return (
    <>
      <div className="ecrf-login-main">
        <Grid container>
          <Grid item xs={6}></Grid>
          <Grid item xs={6}>
            <div className="form">
              <img aria-label="logo" className="logoClass" src={logo}></img>
              {VERSION ? <div class="bottom-left">v{VERSION}</div> : ""}

              <Typography>India ECG Registry</Typography>
              <Typography>An initiative by Eris</Typography>

              <TextField
                // fullWidth
                id="outlined-basic"
                label="User ID"
                variant="outlined"
                value={values.userId}
                onChange={handleChange("userId")}
              />
              <TextField
                // fullWidth
                id="outlined-basic2"
                label="Password"
                variant="outlined"
                type={values.showPassword ? "text" : "password"}
                value={values.password}
                onChange={handleChange("password")}
                onKeyDown={(e) => {
                  onKeyDownHandler(e);
                }}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge="end"
                      >
                        {values.showPassword ? (
                          <VisibilityOff />
                        ) : (
                          <Visibility />
                        )}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />

              <Typography onClick={handleForgotPwdBtn}>
                Forgot password?
              </Typography>

              <Button
                className=""
                onClick={handleSubmitForm}
                variant="outlined"
              >
                Sign In
              </Button>

              <Typography style={{ cursor: "default" }}>
                Don’t have an account?
              </Typography>

              <Button
                className=""
                onClick={handleRequestAccessBtn}
                variant="outlined"
              >
                Request Access
              </Button>
            </div>
          </Grid>
        </Grid>
      </div>
      <ImpactSnackBar
        isOpen={displayState}
        message={messageState}
        handleClose={handleToastClose}
        severity={severityState}
      />
    </>
  );
};

export default EcrfLogin;
