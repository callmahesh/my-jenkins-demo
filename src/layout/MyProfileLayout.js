import * as React from 'react';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import EcrfHeader from '../components/header/EcrfHeader';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { getOwnComment, addComment } from '../redux/apiCall/commentsAPIs';
// import pdfIconSvg from '../../assets/images/pdfIcon.svg';
import { GET_PATIENT_ECG_PDF } from '../config/urlConfig';
import Button from '@mui/material/Button';

const MyProfileLayout = (props) => {

    const { id, patId } = useParams();
    // let {basicViatals} = vsitJsonData;


    /* Redux Action Dispatch Hooks */
    const dispatch = useDispatch();

    /* Redux State Selector Hooks */
    const selfComment = useSelector((state) => state.comment.ownComment.value);

    const [comment, setComment] = React.useState('');

    let jwt_token = sessionStorage.getItem('eCRF_token');

    const [pdfFile, setPdfFile] = React.useState('');

    // console.log('EcgReportLayout-> ', id, selfComment);

    // React.useEffect(() => {
    //     if (id && jwt_token) {
    //         dispatch(getOwnComment(id, jwt_token));
    //     }
    // }, [])
    // React.useEffect(() => {
    //     if (selfComment) {
    //         setComment(selfComment)
    //     }
    // }, [selfComment])

    

    /* Handler for writing comment */
    const handleSendComment = () => {
        // console.log('handleSendComment--> ',);
        if (comment === '') {
            // dispatch(error_PostComment('Comment field cannot be empty!'));
            alert('Comment field cannot be empty!')
        } else if (id === '') {
            // dispatch(error_PostComment('Comment field cannot be empty!'));
            alert('Visit Id is empty!')
        }else {
            // if (phaseName === 'phase1') {
            // let reqObj = `?hub_id=${props.hubId}&phase_id=${2}&resource_title=${textvalue}`;
            let reqObj = {
                "reviewComment": comment,
                "role": "INVESTIGATOR",
                "userName": "string"
              }
            dispatch(addComment(reqObj, id, jwt_token));
            // }
        }
    };


    // console.log('VitalDetails: ',medicalList);

    return (
        <div >
            <EcrfHeader />
            <Grid container spacing={2} style={{ marginTop: '64px', padding:'0px 20px' }}>

                Coming soon..

                {/* <Grid item xs={12} >
                    <span style={{ fontFamily: 'Roboto', fontSize:'14px', color:'#1e85b2',  }}>{patId? patId: 'NA'}</span>
                    <div style={{ marginTop:'25px' }}>
                        <span style={{ fontFamily: 'Roboto', fontSize:'25px', fontWeight:600, color:'#4a4a4a' }}>ECG Report</span>
                    </div>
                </Grid>
                <Grid item xs={12} sx={{ paddingLeft: 0, marginBottom:'25px' }} >
                    <p onClick={handleDownloadEcgPdf}>PDF report download..</p>
                    <iframe
                        src={pdfFile}
                        type="application/pdf"
                        height="578px"
                        width="100%"
                    ></iframe>
                </Grid>

                <Grid container item xs={12} sx={{ paddingLeft: 0 }} rowSpacing={2}>
                    <TextField
                        id="standard-read-only-input"
                        label='Comment'
                        multiline
                        defaultValue={comment}
                        onChange={event => setComment(event.target.value)}
                        InputProps={{
                            // readOnly: true,
                        }}
                        fullWidth
                    // variant="standard"
                    />
                </Grid>
                <Grid container item xs={12} sx={{ paddingLeft: 0 }} >
                    <Button variant="contained" style={{ textTransform: 'none' }} onClick={handleSendComment}>
                        Update
                    </Button>
                </Grid> */}


            </Grid>

        </div>
    );
}


export default MyProfileLayout;
