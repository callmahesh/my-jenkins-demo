import * as React from 'react';
import EcgReport from '../components/ecgReport/EcgReport';
import EcrfHeader from '../components/header/EcrfHeader';

const EcgReportLayout = (props) => {

    return (
        <div >
            <EcrfHeader />
            
            <EcgReport />
        </div>
    );
}


export default EcgReportLayout;
