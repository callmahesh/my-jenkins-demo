import { GET_PATIENT_FOR_INVESTIGATOR, GET_PATIENT_VISITS, GET_PATIENT_BY_INVESTIGATOR_ID } from '../../config/urlConfig';
import * as PATIENT_ACTION from "../reducers/patientSlice";
// import {isObject} from '../../utils/commonTools';


export const getPatientByInv = (rowsPerPage,page, token,sortBy, sortDirection,searchBy, type) => {
    // console.log("HITTING - getPatientByInv", token);    
    return dispatch => {
        dispatch(PATIENT_ACTION.isStarted_GetPatientsByInv());
        var apiUrl = new URL(GET_PATIENT_FOR_INVESTIGATOR+`?pageNo=${page}&pageSize=${rowsPerPage}&sortBy=${sortBy}&sortDirection=${sortDirection}&searchBy=${searchBy}&type=${type}`);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getPatientByInv: ", data);
            if (data && 'content' in data) {
                if (data.content.length > 0) {
                    let tempPagination = {
                        totalRows: data.totalElements,
                        totalPages: data.totalPages,
                        currentPage:page
                    }                    
                    let temp = { payload: data['content'], pagination: tempPagination };
                    // console.log("Content found: ", temp);
                    return dispatch(PATIENT_ACTION.success_GetPatientsByInv({ ...temp }));
                } else{
                    let temp = { payload: data['content'], pagination: {} };
                    return dispatch(PATIENT_ACTION.success_GetPatientsByInv({ ...temp }));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(PATIENT_ACTION.error_GetPatientsByInv(data.message));
                } else {
                    return dispatch(PATIENT_ACTION.error_GetPatientsByInv('Server error'));
                }
            }
            return dispatch(PATIENT_ACTION.success_GetPatientsByInv(data['content']));
        }).catch(error => {
            //console.error("ERROR IN getPatientByInv API: ", error)
            dispatch(PATIENT_ACTION.error_GetPatientsByInv("Server error"));
        });
    }
}

export const getPatientByInvId = (rowsPerPage,page, token,sortBy, sortDirection, invId, searchBy, type) => {
    // console.log("HITTING - getPatientByInvId", token);
    return dispatch => {
        dispatch(PATIENT_ACTION.isStarted_GetPatientsByInvId());
        let newURL = GET_PATIENT_BY_INVESTIGATOR_ID.replace("{invId}", invId);
        var apiUrl = new URL(newURL+`?pageNo=${page}&pageSize=${rowsPerPage}&sortBy=${sortBy}&sortDirection=${sortDirection}&searchBy=${searchBy}&type=${type}`);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getPatientByInvId: ", data);
            if (data && 'content' in data) {
                if (data.content.length > 0) {
                    let tempPagination = {
                        totalRows: data.totalElements,
                        totalPages: data.totalPages,
                        currentPage:page
                    }                    
                    let temp = { payload: data['content'], pagination: tempPagination };
                    // console.log("Content found: ", temp);
                    return dispatch(PATIENT_ACTION.success_GetPatientsByInvId({ ...temp }));
                } else{
                    let temp = { payload: data['content'], pagination: {} };
                    return dispatch(PATIENT_ACTION.success_GetPatientsByInvId({ ...temp }));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(PATIENT_ACTION.error_GetPatientsByInvId(data.message));
                } else {
                    return dispatch(PATIENT_ACTION.error_GetPatientsByInvId('Server error'));
                }
            }            
        }).catch(error => {
            //console.error("ERROR IN getPatientByInvId API: ", error)
            dispatch(PATIENT_ACTION.error_GetPatientsByInvId("Server error"));
        });
    }
}

export const getPatientVisits = (patId, token) => {
    // console.log("HITTING - getPatientVisits", GET_PATIENT_VISITS, patId);
    let newURL = GET_PATIENT_VISITS.replace("{patientId}", patId);
    // console.log("Modified URL", newURL);
    return dispatch => {
        dispatch(PATIENT_ACTION.isStarted_GetPatientVisits);
        var apiUrl = new URL(newURL);
        fetch(apiUrl, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        })
            .then(data => {
                // console.log("API RESPONSE getPatientVisits: ", data);
                if(Array.isArray(data)){
                    return dispatch(PATIENT_ACTION.success_GetPatientVisits(data))
                }else {
                    if (data && 'message' in data) {
                        //alert(`Error: ${data.message}`);
                        return dispatch(PATIENT_ACTION.error_GetPatientVisits(data.message));
                    } else {
                        return dispatch(PATIENT_ACTION.error_GetPatientVisits('Server error'));
                    }
                }               
            }).catch(error => {
                console.error("ERROR IN getPatientVisits API: ", error)
                dispatch(PATIENT_ACTION.error_GetPatientVisits("Server error"));
            });
    }
}
