import { getPrivacyPolicyStarted, getPrivacyPolicyFailed, getPrivacyPolicySuccess } from "../../reducers/common/privacyPolicySlice";
// import { useDispatch } from "react-redux";
import { GET_PRIVACY_POLICY_EP } from '../../../config/urlConfig';



export const getPrivacyPolicy = (obj) => {

    // console.log("getPrivacyPolicy:",obj)
    return dispatch => {
        dispatch(getPrivacyPolicyStarted());
        fetch(GET_PRIVACY_POLICY_EP, {
            method: "GET",
        })
            .then(data1 => data1.json())
            .then(data => {
                // console.log("getPrivacyPolicy-1 ",data)
                if (!data.error) {
                    // console.log("getPrivacyPolicy-2")
                    dispatch(getPrivacyPolicySuccess(data.message));
                    //console.log("getPrivacyPolicy-2")
                } else {
                    let error = data.message;
                    // console.log("getPrivacyPolicy-3")
                    dispatch(getPrivacyPolicyFailed(error));
                }
            })
            .catch(error =>
                dispatch(getPrivacyPolicyFailed(error))
            )
    }
};