import { POST_COMMENT, GET_OWN_COMMENT, GET_ALL_COMMENT, SEND_COMMENTS_MAIL, GET_EK12_DROPDOWN_MASTERS } from '../../config/urlConfig';
import * as COMMENT_ACTION from "../reducers/commentsSlice";


export const getOwnComment = (visitId, token,) => {
    // console.log("HITTING - getOwnComment", token);
    return dispatch => {
        dispatch(COMMENT_ACTION.isStarted_GetOwnComment());
        let newURL = GET_OWN_COMMENT.replace("{visitId}", visitId);
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getOwnComment: ", data);
            if (data && 'reviewComment' in data) {

                return dispatch(COMMENT_ACTION.success_GetOwnComment(data));

            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(COMMENT_ACTION.error_GetOwnComment(data.message));
                } else {
                    return dispatch(COMMENT_ACTION.error_GetOwnComment('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN getOwnComment API: ", error)
            dispatch(COMMENT_ACTION.error_GetOwnComment("Server error2"));
        });
    }
}

export const getAllComment = (visitId, token,) => {
    // console.log("HITTING - getAllComment", token);
    return dispatch => {
        dispatch(COMMENT_ACTION.isStarted_GetAllComment());
        let newURL = GET_ALL_COMMENT.replace("{visitId}", visitId);
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getAllComment: ", data);
            if (data) {
                if (data.length > 0) {
                    return dispatch(COMMENT_ACTION.success_GetAllComment(data.reverse()));
                } else {
                    return dispatch(COMMENT_ACTION.success_GetAllComment([]));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(COMMENT_ACTION.error_GetAllComment(data.message));
                } else {
                    return dispatch(COMMENT_ACTION.error_GetAllComment('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN getAllComment API: ", error)
            dispatch(COMMENT_ACTION.error_GetAllComment("Server error2"));
        });
    }
}

export const addComment = (reqObj, visitId, token, update = false) => {
    // console.log("HITTING - addComment", token);
    // let {visitId, role, userName, reviewComment } = reqObj;
    return dispatch => {
        if (update) {
            dispatch(COMMENT_ACTION.isStarted_UpdateComment());
        } else {
            dispatch(COMMENT_ACTION.isStarted_PostComment());
        }

        let newURL = POST_COMMENT.replace("{visitId}", visitId);
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "POST",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqObj)
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE addComment: ", data);
            if (data && 'reviewComment' in data) {
                if (update) {
                    dispatch(COMMENT_ACTION.success_UpdateComment('Successfully updated..'));
                } else {
                    return dispatch(COMMENT_ACTION.success_PostComment('Successfully updated..'));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    if (update) {
                        dispatch(COMMENT_ACTION.error_UpdateComment(data.message));
                    } else {
                        return dispatch(COMMENT_ACTION.error_PostComment(data.message));
                    }

                } else {
                    if (update) {
                        dispatch(COMMENT_ACTION.error_UpdateComment('Server error1'));
                    } else {
                        return dispatch(COMMENT_ACTION.error_PostComment('Server error1'));
                    }

                }
            }
        }).catch(error => {
            //console.error("ERROR IN addComment API: ", error)
            if (update) {
                dispatch(COMMENT_ACTION.isStarted_UpdateComment());
            } else {
                dispatch(COMMENT_ACTION.error_PostComment("Server error2"));
            }

        });
    }
}

export const sendCommentEmail = (reqObj, visitId, token) => {
    // console.log("HITTING - addComment", token);
    // let {visitId, role, userName, reviewComment } = reqObj;
    return dispatch => {
        dispatch(COMMENT_ACTION.isStarted_SendCommentEmail());
        // console.log(JSON.stringify(reqObj));

        let newURL = SEND_COMMENTS_MAIL.replace("{visitId}", visitId);
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "POST",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqObj)
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res;
            }
        }).then(data => {
            // console.log("API RESPONSE sendEmailComment: ", data);
            if (data && data.status === 200) {
                dispatch(COMMENT_ACTION.success_SendCommentEmail('Successfully sent email..'));
            }
            else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    dispatch(COMMENT_ACTION.error_SendCommentEmail(data.message));
                } else {
                    dispatch(COMMENT_ACTION.error_SendCommentEmail('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN addComment API: ", error)
            dispatch(COMMENT_ACTION.error_SendCommentEmail('Error'));

        });
    }
}


export const getEK12DeterminationMaster = (token) => {
    // console.log("HITTING - getAllComment", token);
    return dispatch => {
        dispatch(COMMENT_ACTION.isStarted_GetEk12DetMaster());
        let newURL = GET_EK12_DROPDOWN_MASTERS.replace("{type}", "determination");
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getAllComment: ", data);
            if (data) {
                if (data.content.ek12Masters.length > 0) {
                    // const blankArray = [{name: 'Select', id: 0}];
                    // const finalArray = blankArray.concat(data.content.ek12Masters)
                    const finalArray = data.content.ek12Masters;
                    return dispatch(COMMENT_ACTION.success_GetEk12DetMaster(finalArray));
                } else {
                    return dispatch(COMMENT_ACTION.success_GetEk12DetMaster([]));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(COMMENT_ACTION.error_GetEk12DetMaster(data.message));
                } else {
                    return dispatch(COMMENT_ACTION.error_GetEk12DetMaster('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN getAllComment API: ", error)
            dispatch(COMMENT_ACTION.error_GetEk12DetMaster("Server error2"));
        });
    }
}

export const getEK12DiagnosisMaster = (token) => {
    // console.log("HITTING - getAllComment", token);
    return dispatch => {
        dispatch(COMMENT_ACTION.isStarted_GetEk12DetMaster());
        let newURL = GET_EK12_DROPDOWN_MASTERS.replace("{type}", "diagnosis");
        var apiUrl = new URL(newURL);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE getAllComment: ", data);
            if (data) {
                if (data.content.ek12Masters.length > 0) {
                    // const blankArray = [{name: 'Select', id: 0}];
                    // const finalArray = blankArray.concat(data.content.ek12Masters)
                    const finalArray = data.content.ek12Masters;
                    return dispatch(COMMENT_ACTION.success_GetEk12DiagMaster(finalArray));
                } else {
                    return dispatch(COMMENT_ACTION.success_GetEk12DiagMaster([]));
                }
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                    return dispatch(COMMENT_ACTION.error_GetEk12DiagMaster(data.message));
                } else {
                    return dispatch(COMMENT_ACTION.error_GetEk12DiagMaster('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN getAllComment API: ", error)
            dispatch(COMMENT_ACTION.error_GetEk12DiagMaster("Server error2"));
        });
    }
}