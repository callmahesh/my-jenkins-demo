import { GET_EMPL_MY_PROFILE, GET_REPORTEE_BY_MANAGER_ID } from '../../config/urlConfig';
import * as EMPLOYEE_ACTION from "../reducers/employeeSlice";

// import {isObject} from '../../utils/commonTools';

export const getMyProfile = (uId, token) => {
    // console.log("HITTING - getMyProfile", obj);
    return dispatch => {
        dispatch(EMPLOYEE_ACTION.isStarted_GetMyProfile());
        var apiUrl = new URL(GET_EMPL_MY_PROFILE +'/'+uId);
        //apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl,{
            // credentials:"include",
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if(res.status === 401  ){
                window.location = "/login";
            } else {
                return res.json();
            }           
        })
        .then(data => {            
            // console.log("API RESPONSE getMyProfile: ", data);
            // if(!isObject(data) || !data.hasOwnProperty('status') || data.status == 'error' || data.status['error'] == true){
            //     if(isObject(data['status'])){
            //         return dispatch(EMPLOYEE_ACTION.error_GetUserProfile(data['status']['message']));
            //     }
            //     return dispatch(EMPLOYEE_ACTION.error_GetUserProfile(data['error']));
            // }
            return dispatch(EMPLOYEE_ACTION.success_GetMyProfile(data));
        }).catch(error =>{
            //console.error("ERROR IN getMyProfile API: ", error)
            dispatch(EMPLOYEE_ACTION.error_GetMyProfile("Server error"));
        });
    }
}

export const getCoInvReportee = (managerId, token) => {
    // console.log("HITTING - getCoInvReportee", GET_PATIENT_VISITS, managerId);
    let newURL = GET_REPORTEE_BY_MANAGER_ID.replace("{managerId}", managerId);
    // console.log("Modified URL", newURL);
    return dispatch => {
        dispatch(EMPLOYEE_ACTION.isStarted_GetCoInvReportee());
        var apiUrl = new URL(newURL);
        fetch(apiUrl, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        })
            .then(data => {
                // console.log("API RESPONSE getCoInvReportee: ", data);
                if(Array.isArray(data)){
                    return dispatch(EMPLOYEE_ACTION.success_GetCoInvReportee(data))
                } else {
                    if (data && 'message' in data) {
                        //alert(`Error: ${data.message}`);
                        return dispatch(EMPLOYEE_ACTION.error_GetCoInvReportee(data.message));
                    } else {
                        return dispatch(EMPLOYEE_ACTION.error_GetCoInvReportee('Server error1'));
                    }
                }                
            }).catch(error => {
                console.error("ERROR IN getCoInvReportee API: ", error)
                dispatch(EMPLOYEE_ACTION.error_GetCoInvReportee("Server error2"));
            });
    }
}

export const getRPInvReportee = (managerId, token) => {
    // console.log("HITTING - getCoInvReportee", GET_PATIENT_VISITS, managerId);
    let newURL = GET_REPORTEE_BY_MANAGER_ID.replace("{managerId}", managerId);
    // console.log("Modified URL", newURL);
    return dispatch => {
        dispatch(EMPLOYEE_ACTION.isStarted_GetRPInvReportee());
        var apiUrl = new URL(newURL);
        fetch(apiUrl, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        })
            .then(data => {
                // console.log("API RESPONSE getRPInvReportee: ", data);
                if(Array.isArray(data)){
                    return dispatch(EMPLOYEE_ACTION.success_GetRPInvReportee(data))
                } else {
                    if (data && 'message' in data) {
                        //alert(`Error: ${data.message}`);
                        return dispatch(EMPLOYEE_ACTION.error_GetRPInvReportee(data.message));
                    } else {
                        return dispatch(EMPLOYEE_ACTION.error_GetRPInvReportee('Server error1'));
                    }
                }                
            }).catch(error => {
                console.error("ERROR IN getRPInvReportee API: ", error)
                dispatch(EMPLOYEE_ACTION.error_GetRPInvReportee("Server error2"));
            });
    }
}

export const getPInvReportee = (managerId, token) => {
    // console.log("HITTING - getCoInvReportee", GET_PATIENT_VISITS, managerId);
    let newURL = GET_REPORTEE_BY_MANAGER_ID.replace("{managerId}", managerId);
    // console.log("Modified URL", newURL);
    return dispatch => {
        dispatch(EMPLOYEE_ACTION.isStarted_GetPInvReportee());
        var apiUrl = new URL(newURL);
        fetch(apiUrl, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${token}`, // notice the Bearer before your token
            },
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        })
            .then(data => {
                // console.log("API RESPONSE getPInvReportee: ", data);
                if(Array.isArray(data)){
                    return dispatch(EMPLOYEE_ACTION.success_GetPInvReportee(data))
                } else {
                    if (data && 'message' in data) {
                        //alert(`Error: ${data.message}`);
                        return dispatch(EMPLOYEE_ACTION.error_GetPInvReportee(data.message));
                    } else {
                        return dispatch(EMPLOYEE_ACTION.error_GetPInvReportee('Server error1'));
                    }
                }                
            }).catch(error => {
                console.error("ERROR IN getRPInvReportee API: ", error)
                dispatch(EMPLOYEE_ACTION.error_GetPInvReportee("Server error2"));
            });
    }
}