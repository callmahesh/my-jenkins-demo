import { RESET_PASSWORD, INITIATE_RESET_PASSWORD, REQUEST_ACCESS } from '../../config/urlConfig';
import * as AUTH_ACTION from "../reducers/authSlice";




export const resetPassword = (reqObj, resetToken, setSubmitting, setAlertMessage, setPassChangeSuccess, resetForm) => {
    // console.log("HITTING - resetPassword", resetToken);
    
    // let {visitId, role, userName, reviewComment } = reqObj;
    return dispatch => {
        
        dispatch(AUTH_ACTION.isStarted_ResetPassword());

        let newURL = RESET_PASSWORD.replace("{token}", resetToken);
        let apiUrl = new URL(newURL);
        // apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "POST",
            headers: {
                // 'Authorization': `Bearer ${token}`, // notice the Bearer before your token
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqObj)
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE resetPassword: ", data);
            if (data && data.message === 'Password Reset Completed!') {
                    dispatch(AUTH_ACTION.success_ResetPassword('Successfully changed password..'));
                    setSubmitting(false);
                    setAlertMessage('Password Changed Successfully, click on close button to redirect to login page...');
                    setPassChangeSuccess(true);
                    resetForm();
            } else {
                // console.log("Error: ", data);
                setSubmitting(false);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                        dispatch(AUTH_ACTION.error_ResetPassword(data.message));
                        setAlertMessage('Password Change Unsuccessful, try again or contact your administrator...');
                        setPassChangeSuccess(true);
                } else {
                        dispatch(AUTH_ACTION.error_ResetPassword('Server error1'));
                        setAlertMessage('Password Change Unsuccessful, try again or contact your administrator...');
                        setPassChangeSuccess(true);
                }
            }
        }).catch(error => {
            //console.error("ERROR IN addComment API: ", error)
                dispatch(AUTH_ACTION.error_ResetPassword(error));
        });
    }
}

export const initiateReset = (userName) => {
    // console.log("HITTING - initiateReset", userName);
    
    if(userName==undefined || userName == null || userName == ''){
        // userName='PIECG_29';
    }
    // let {visitId, role, userName, reviewComment } = reqObj;
    return dispatch => {
        
        dispatch(AUTH_ACTION.isStarted_InitiateResetPassword());

        let newURL = INITIATE_RESET_PASSWORD.replace("{userID}", userName);
        let apiUrl = new URL(newURL);
        // apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "POST",
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE resetPassword: ", data);
            if (data && 'message' in data) {
                    dispatch(AUTH_ACTION.success_InitiateResetPassword(data.message));
            } else {
                // console.log("Error: ", data);
                if (data && 'message' in data) {
                    //alert(`Error: ${data.message}`);
                        dispatch(AUTH_ACTION.error_InitiateResetPassword(data.message));
                } else {
                        // dispatch(AUTH_ACTION.error_InitiateResetPassword('Server error1'));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN addComment API: ", error)
                dispatch(AUTH_ACTION.error_InitiateResetPassword(error));
        });
    }
}

export const requestAccess = (reqObj) => {
    // console.log("HITTING - requestAccess", reqObj);

    return dispatch => {
        
        dispatch(AUTH_ACTION.isStarted_RequestAccess());

        // let newURL = REQUEST_ACCESS.replace("{userID}", userName);
        let apiUrl = new URL(REQUEST_ACCESS);
        // apiUrl.search = new URLSearchParams(obj).toString();
        fetch(apiUrl, {
            // credentials:"include",
            method: "POST",
            headers: {
                // 'Authorization': `Bearer ${token}`, // notice the Bearer before your token
                "Content-Type": "application/json"
            },
            body: JSON.stringify(reqObj)
        }).then(res => {
            if (res.status === 401) {
                window.location = "/login";
            } else {
                return res.json();
            }
        }).then(data => {
            // console.log("API RESPONSE resetPassword: ", data);
            if (data && 'message' in data) {
                    dispatch(AUTH_ACTION.success_RequestAccess(data.message));
            } else {
                // console.log("Error: ", data);
                if (data && 'exception' in data && data.exception.length > 0) {
                    //alert(`Error: ${data.message}`);
                    dispatch(AUTH_ACTION.error_RequestAccess(data.exception));
                }
                else if(data && 'message' in data && data.message.length > 0){
                    dispatch(AUTH_ACTION.error_RequestAccess(data.message));
                }
            }
        }).catch(error => {
            //console.error("ERROR IN addComment API: ", error)
                dispatch(AUTH_ACTION.error_RequestAccess(error));
        });
    }
}
