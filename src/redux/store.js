import { configureStore } from '@reduxjs/toolkit';
// import publicSlice from './reducers/publicSlice';
// import userSlice from './reducers/userSlice';
// import hubSlice from './reducers/hubSlice';
// import resourceSlice from './reducers/resourceSlice';
import patientSlice from './reducers/patientSlice';
import employeeSlice from './reducers/employeeSlice';
import commentsSlice from './reducers/commentsSlice';
import authSlice from './reducers/authSlice';

export default configureStore({
  reducer: {
    // public: publicSlice,
    // user: userSlice,
    // hub: hubSlice,
    // resource: resourceSlice,
    patient: patientSlice,
    employee: employeeSlice,
    comment: commentsSlice,
    auth: authSlice,
  },
})