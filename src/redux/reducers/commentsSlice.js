import { createSlice } from '@reduxjs/toolkit'

export const commentsSlice = createSlice({
    name: `comment`,
    initialState: {
        getAllComment: {
            pagination: {},
            isStarted: false,
            isError: false,
            error: '',
            value: []
        },
        addComment: {
            isStarted: false,
            isError: false,
            isSuccess:false,
            error: '',
            value: ''
        },
        updateComment: {
            isStarted: false,
            isError: false,
            error: '',
            value: ''
        },
        ownComment: {
            isStarted: false,
            isError: false,
            error: '',
            value: {}
        },
        sendCommentEmail: {
            isStarted: false,
            isError: false,
            error: '',
            value: ''
        },
        getEK12DetMaster: {
            isStarted: false,
            isError: false,
            error: '',
            value: ''
        },
        getEK12DiagMaster: {
            isStarted: false,
            isError: false,
            error: '',
            value: ''
        }
    },
    reducers: {            
        isStarted_GetAllComment: (state) => {
            state.getAllComment.isStarted = true;
            state.getAllComment.isError = false;
        },
        reset_GetAllComment: (state) => {
            // console.log("reset_GetOwnComment: ")
            state.getAllComment.isStarted = false;
            state.getAllComment.isError = false;
            state.getAllComment.value = [];
            state.getAllComment.error = '';
        },
        success_GetAllComment: (state, action) => {
            // console.log("success_GetUserProfile: ",action)
            let { payload } = action;
            state.getAllComment.isError = false;
            state.getAllComment.isStarted = false;
            state.getAllComment.error = '';
            state.getAllComment.value = payload;
        },
        error_GetAllComment: (state, action) => {
            let { payload } = action;
            state.getAllComment.isError = true;
            state.getAllComment.isStarted = false;
            state.getAllComment.error = payload;
        },
        isStarted_PostComment: (state) => {
            // console.log("isStarted_PostComment: ")
            state.addComment.isStarted = true;
            state.addComment.isError = false;
        },
        reset_PostComment: (state) => {
            // console.log("reset_PostComment: ")
            state.addComment.isStarted = false;
            state.addComment.isError = false;
            state.addComment.value = '';
            state.addComment.error = '';
        },
        success_PostComment: (state, action) => {
            // console.log("success_addComment: ",action)
            let { payload } = action;
            state.addComment.isError = false;
            state.addComment.isStarted = false;
            state.addComment.error = '';
            state.addComment.value = payload;
        },
        error_PostComment: (state, action) => {
            // console.log("error_PostComment: ",action)
            let { payload } = action;
            state.addComment.isError = true;
            state.addComment.isStarted = false;
            state.addComment.error = payload;
            state.addComment.value = '';
        },
        isStarted_UpdateComment: (state) => {
            // console.log("isStarted_UpdateComment: ")
            state.updateComment.isStarted = true;
            state.updateComment.isError = false;
            state.updateComment.isSuccess = false;
        },
        reset_UpdateComment: (state) => {
            // console.log("reset_UpdateComment: ")
            state.updateComment.isStarted = false;
            state.updateComment.isError = false;
            state.updateComment.isSuccess = false;
            state.updateComment.value = '';
            state.updateComment.error = '';
        },
        success_UpdateComment: (state, action) => {
            // console.log("success_updateComment: ",action)
            let { payload } = action;
            state.updateComment.isError = false;
            state.updateComment.isStarted = false;
            state.updateComment.isSuccess = true;
            state.updateComment.error = '';
            state.updateComment.value = payload;
        },
        error_UpdateComment: (state, action) => {
            // console.log("error_UpdateComment: ",action)
            let { payload } = action;
            state.updateComment.isError = true;
            state.updateComment.isStarted = false;
            state.updateComment.error = payload;
            state.updateComment.value = '';
        },
        isStarted_GetOwnComment: (state) => {
            // console.log("isStarted_GetOwnComment: ")
            state.ownComment.isStarted = true;
            state.ownComment.isError = false;
        },
        reset_GetOwnComment: (state) => {
            // console.log("reset_GetOwnComment: ")
            state.ownComment.isStarted = false;
            state.ownComment.isError = false;
            state.ownComment.value = {};
            state.ownComment.error = '';
        },
        success_GetOwnComment: (state, action) => {
            // console.log("success_GetOwnComment: ",action)
            let { payload } = action;
            state.ownComment.isError = false;
            state.ownComment.isStarted = false;
            state.ownComment.error = '';
            state.ownComment.value = payload;
        },
        error_GetOwnComment: (state, action) => {
            // console.log("error_GetOwnComment: ",action)
            let { payload } = action;
            state.ownComment.isError = true;
            state.ownComment.isStarted = false;
            state.ownComment.error = payload;
            state.ownComment.value = {};
        },
        isStarted_SendCommentEmail: (state) => {
            // console.log("isStarted_SendCommentEmail: ")
            state.sendCommentEmail.isStarted = true;
            state.sendCommentEmail.isError = false;
        },
        reset_SendCommentEmail: (state) => {
            // console.log("reset_SendCommentEmail: ")
            state.sendCommentEmail.isStarted = false;
            state.sendCommentEmail.isError = false;
            state.sendCommentEmail.value = '';
            state.sendCommentEmail.error = '';
        },
        success_SendCommentEmail: (state, action) => {
            // console.log("success_SendCommentEmail: ",action)
            let { payload } = action;
            state.sendCommentEmail.isError = false;
            state.sendCommentEmail.isStarted = false;
            state.sendCommentEmail.error = '';
            state.sendCommentEmail.value = payload;
        },
        error_SendCommentEmail: (state, action) => {
            // console.log("error_SendCommentEmail: ",action)
            let { payload } = action;
            state.sendCommentEmail.isError = true;
            state.sendCommentEmail.isStarted = false;
            state.sendCommentEmail.error = payload;
            state.sendCommentEmail.value = '';
        },
        isStarted_GetEk12DetMaster: (state) => {
            // console.log("isStarted_SendCommentEmail: ")
            state.getEK12DetMaster.isStarted = true;
            state.getEK12DetMaster.isError = false;
        },
        reset_GetEk12DetMaster: (state) => {
            // console.log("reset_getEK12DetMaster: ")
            state.getEK12DetMaster.isStarted = false;
            state.getEK12DetMaster.isError = false;
            state.getEK12DetMaster.value = '';
            state.getEK12DetMaster.error = '';
        },
        success_GetEk12DetMaster: (state, action) => {
            // console.log("success_getEK12DetMaster: ",action)
            let { payload } = action;
            state.getEK12DetMaster.isError = false;
            state.getEK12DetMaster.isStarted = false;
            state.getEK12DetMaster.error = '';
            state.getEK12DetMaster.value = payload;
        },
        error_GetEk12DetMaster: (state, action) => {
            // console.log("error_getEK12DetMaster: ",action)
            let { payload } = action;
            state.getEK12DetMaster.isError = true;
            state.getEK12DetMaster.isStarted = false;
            state.getEK12DetMaster.error = payload;
            state.getEK12DetMaster.value = '';
        },
        isStarted_GetEk12DiagMaster: (state) => {
            // console.log("isStarted_SendCommentEmail: ")
            state.getEK12DiagMaster.isStarted = true;
            state.getEK12DiagMaster.isError = false;
        },
        reset_GetEk12DiagMaster: (state) => {
            // console.log("reset_getEK12DiagMaster: ")
            state.getEK12DiagMaster.isStarted = false;
            state.getEK12DiagMaster.isError = false;
            state.getEK12DiagMaster.value = '';
            state.getEK12DiagMaster.error = '';
        },
        success_GetEk12DiagMaster: (state, action) => {
            // console.log("success_getEK12DiagMaster: ",action)
            let { payload } = action;
            state.getEK12DiagMaster.isError = false;
            state.getEK12DiagMaster.isStarted = false;
            state.getEK12DiagMaster.error = '';
            state.getEK12DiagMaster.value = payload;
        },
        error_GetEk12DiagMaster: (state, action) => {
            // console.log("error_getEK12DiagMaster: ",action)
            let { payload } = action;
            state.getEK12DiagMaster.isError = true;
            state.getEK12DiagMaster.isStarted = false;
            state.getEK12DiagMaster.error = payload;
            state.getEK12DiagMaster.value = '';
        }
    }
})

export const {    
    isStarted_GetAllComment, success_GetAllComment, error_GetAllComment, reset_GetAllComment,
    isStarted_PostComment, success_PostComment, error_PostComment, reset_PostComment,
    isStarted_UpdateComment, success_UpdateComment, error_UpdateComment, reset_UpdateComment,
    isStarted_GetOwnComment, success_GetOwnComment, error_GetOwnComment, reset_GetOwnComment,
    isStarted_SendCommentEmail, success_SendCommentEmail, error_SendCommentEmail, reset_SendCommentEmail,
    isStarted_GetEk12DetMaster, reset_GetEk12DetMaster, success_GetEk12DetMaster, error_GetEk12DetMaster,
    isStarted_GetEk12DiagMaster, reset_GetEk12DiagMaster, success_GetEk12DiagMaster, error_GetEk12DiagMaster
} = commentsSlice.actions
export default commentsSlice.reducer