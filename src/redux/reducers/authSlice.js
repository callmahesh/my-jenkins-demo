import { createSlice } from '@reduxjs/toolkit'

export const authSlice = createSlice({
    name: `auth`,
    initialState: {
        requestAccess: {
            isStarted: false,
            isError: false,
            error: '',
            value: '',
        },
        resetPassword: {
            isStarted: false,
            isError: false,
            isSuccess: false,
            error: '',
            value: '',
        },
        initiateResetPassword: {
            isStarted: false,
            isError: false,
            isSuccess: false,
            error: '',
            value: '',
        },
        requestAccess: {
            isStarted: false,
            isError: false,
            error: '',
            value: [],
        }
    },
    reducers: {
        isStarted_PostComment: (state) => {
            // // console.log("isStarted_PostComment: ")
            state.requestAccess.isStarted = true;
            state.requestAccess.isError = false;
        },
        reset_PostComment: (state) => {
            // // console.log("reset_PostComment: ")
            state.requestAccess.isStarted = false;
            state.requestAccess.isError = false;
            state.requestAccess.value = '';
            state.requestAccess.error = '';
        },
        success_PostComment: (state, action) => {
            // // console.log("success_requestAccess: ",action)
            let { payload } = action;
            state.requestAccess.isError = false;
            state.requestAccess.isStarted = false;
            state.requestAccess.error = '';
            state.requestAccess.value = payload;
        },
        error_PostComment: (state, action) => {
            // // console.log("error_PostComment: ",action)
            let { payload } = action;
            state.requestAccess.isError = true;
            state.requestAccess.isStarted = false;
            state.requestAccess.error = payload;
            state.requestAccess.value = '';
        },
        isStarted_ResetPassword: (state) => {
            // // console.log("isStarted_resetPassword: ")
            state.resetPassword.isStarted = true;
            state.resetPassword.isError = false;
            state.resetPassword.isSuccess = false;
        },
        reset_ResetPassword: (state) => {
            // // console.log("reset_resetPassword: ")
            state.resetPassword.isStarted = false;
            state.resetPassword.isError = false;
            state.resetPassword.isSuccess = false;
            state.resetPassword.value = '';
            state.resetPassword.error = '';
        },
        success_ResetPassword: (state, action) => {
            // // console.log("success_resetPassword: ",action)
            let { payload } = action;
            state.resetPassword.isError = false;
            state.resetPassword.isStarted = false;
            state.resetPassword.isSuccess = true;
            state.resetPassword.error = '';
            state.resetPassword.value = payload;
        },
        error_ResetPassword: (state, action) => {
            // // console.log("error_resetPassword: ",action)
            let { payload } = action;
            state.resetPassword.isError = true;
            state.resetPassword.isStarted = false;
            state.resetPassword.error = payload;
            state.resetPassword.value = '';
        },
        isStarted_InitiateResetPassword: (state) => {
            // // console.log("isStarted_resetPassword: ")
            state.initiateResetPassword.isStarted = true;
            state.initiateResetPassword.isError = false;
            state.initiateResetPassword.isSuccess = false;
        },
        reset_InitiateResetPassword: (state) => {
            // // console.log("reset_initiateResetPassword: ")
            state.initiateResetPassword.isStarted = false;
            state.initiateResetPassword.isError = false;
            state.initiateResetPassword.isSuccess = false;
            state.initiateResetPassword.value = '';
            state.initiateResetPassword.error = '';
        },
        success_InitiateResetPassword: (state, action) => {
            // // console.log("success_initiateResetPassword: ",action)
            let { payload } = action;
            state.initiateResetPassword.isError = false;
            state.initiateResetPassword.isStarted = false;
            state.initiateResetPassword.isSuccess = true;
            state.initiateResetPassword.error = '';
            state.initiateResetPassword.value = payload;
        },
        error_InitiateResetPassword: (state, action) => {
            // // console.log("error_initiateResetPassword: ",action)
            let { payload } = action;
            state.initiateResetPassword.isError = true;
            state.initiateResetPassword.isStarted = false;
            state.initiateResetPassword.error = payload;
            state.initiateResetPassword.value = '';
        },
        isStarted_RequestAccess: (state) => {
            // // console.log("isStarted_GetRPInvReportee: ")
            state.requestAccess.isStarted = true;
            state.requestAccess.isError = false;
            state.requestAccess.isSuccess = false;
        },
        reset_RequestAccess: (state) => {
            // // console.log("reset_initiateResetPassword: ")
            state.requestAccess.isStarted = false;
            state.requestAccess.isError = false;
            state.requestAccess.isSuccess = false;
            state.requestAccess.value = '';
            state.requestAccess.error = '';
        },
        success_RequestAccess: (state, action) => {
            // // console.log("success_GetRrequestAccess: ",action)
            let { payload } = action;
            state.requestAccess.isError = false;
            state.requestAccess.isStarted = false;
            state.requestAccess.isSuccess = true;
            state.requestAccess.error = '';
            state.requestAccess.value = payload;
        },
        error_RequestAccess: (state, action) => {
            //// console.log("error_GetRrequestAccess: ",action)
            let { payload } = action;
            state.requestAccess.isError = true;
            state.requestAccess.isStarted = false;
            state.requestAccess.isSuccess = false;
            state.requestAccess.error = payload;
            state.requestAccess.value = [];
        },
    }
        
})

export const { 
    isStarted_PostComment, success_PostComment, error_PostComment, reset_PostComment,
    isStarted_ResetPassword, reset_ResetPassword, success_ResetPassword, error_ResetPassword,
    isStarted_InitiateResetPassword ,reset_InitiateResetPassword ,success_InitiateResetPassword ,error_InitiateResetPassword,
    isStarted_RequestAccess, reset_RequestAccess, success_RequestAccess, error_RequestAccess
    
} = authSlice.actions
export default authSlice.reducer