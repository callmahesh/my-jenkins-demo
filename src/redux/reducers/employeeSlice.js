import { createSlice } from '@reduxjs/toolkit'

export const employeeSlice = createSlice({
    name: `employee`,
    initialState: {
        myProfile: {
            pagination: {},
            isStarted: false,
            isError: false,
            error: '',
            value: {},
        },
        coInvReportee: {
            isStarted: false,
            isError: false,
            error: '',
            value: [],
            current: ''
        },
        rPInvReportee: {
            isStarted: false,
            isError: false,
            error: '',
            value: [],
            current: ''
        },
        pInvReportee: {
            isStarted: false,
            isError: false,
            error: '',
            value: [],
            current: ''
        },
        manager: {
            current: ''
        },
        toggle: {
            investigator: true,
            others: false
        },
        user_details: {         
            value: {},
        },
        firstload: {
            current: false,
            comment: false
        }

    },
    reducers: { 
        set_UserDetails: (state, action) => {
            // console.log("set_UserDetails: ",action)
            let { payload } = action;            
            state.user_details.value = payload;
        },               
        isStarted_GetMyProfile: (state) => {
            // console.log("isStarted_GetMyProfile: ")
            state.myProfile.isStarted = true;
            state.myProfile.isError = false;
        },
        success_GetMyProfile: (state, action) => {
            // console.log("success_myProfile: ",action)
            let { payload } = action;
            state.myProfile.isError = false;
            state.myProfile.isStarted = false;
            state.myProfile.error = '';
            state.myProfile.value = payload;
        },
        error_GetMyProfile: (state, action) => {
            //console.log("error_GetMyProfile: ",action)
            let { payload } = action;
            state.myProfile.isError = true;
            state.myProfile.isStarted = false;
            state.myProfile.error = payload;
            state.myProfile.value = {};
        },
        isStarted_GetCoInvReportee: (state) => {
            // console.log("isStarted_GetCoInvReportee: ")
            state.coInvReportee.isStarted = true;
            state.coInvReportee.isError = false;
        },
        success_GetCoInvReportee: (state, action) => {
            // console.log("success_coInvReportee: ",action)
            let { payload } = action;
            state.coInvReportee.isError = false;
            state.coInvReportee.isStarted = false;
            state.coInvReportee.error = '';
            state.coInvReportee.value = payload;
        },
        error_GetCoInvReportee: (state, action) => {
            //console.log("error_GetCoInvReportee: ",action)
            let { payload } = action;
            state.coInvReportee.isError = true;
            state.coInvReportee.isStarted = false;
            state.coInvReportee.error = payload;
            state.coInvReportee.value = [];
        },
        isStarted_GetRPInvReportee: (state) => {
            // console.log("isStarted_GetRPInvReportee: ")
            state.rPInvReportee.isStarted = true;
            state.rPInvReportee.isError = false;
        },
        success_GetRPInvReportee: (state, action) => {
            // console.log("success_GetRPInvReportee: ",action)
            let { payload } = action;
            state.rPInvReportee.isError = false;
            state.rPInvReportee.isStarted = false;
            state.rPInvReportee.error = '';
            state.rPInvReportee.value = payload;
        },
        error_GetRPInvReportee: (state, action) => {
            //console.log("error_GetRPInvReportee: ",action)
            let { payload } = action;
            state.rPInvReportee.isError = true;
            state.rPInvReportee.isStarted = false;
            state.rPInvReportee.error = payload;
            state.rPInvReportee.value = [];
        },
        isStarted_GetPInvReportee: (state) => {
            // console.log("isStarted_GetRPInvReportee: ")
            state.pInvReportee.isStarted = true;
            state.pInvReportee.isError = false;
        },
        success_GetPInvReportee: (state, action) => {
            // console.log("success_GetRPInvReportee: ",action)
            let { payload } = action;
            state.pInvReportee.isError = false;
            state.pInvReportee.isStarted = false;
            state.pInvReportee.error = '';
            state.pInvReportee.value = payload;
        },
        error_GetPInvReportee: (state, action) => {
            //console.log("error_GetRPInvReportee: ",action)
            let { payload } = action;
            state.pInvReportee.isError = true;
            state.pInvReportee.isStarted = false;
            state.pInvReportee.error = payload;
            state.pInvReportee.value = [];
        },
        set_Investigator: (state, action) => {
            let { payload } = action;
            state.coInvReportee.current = payload;
        },
        set_CoInvestigator: (state, action) => {
            let { payload } = action;
            state.rPInvReportee.current = payload;
        },
        set_RPInvestigator: (state, action) => {
            let { payload } = action;
            state.pInvReportee.current = payload;
        },
        set_CurrentManager: (state, action) => {
            let { payload } = action;
            state.manager.current = payload;
        },
        set_ToggleInv: (state, action) => {
            let { payload } = action;
            state.toggle.investigator = payload;
        },
        set_ToggleOthers: (state, action) => {
            let { payload } = action;
            state.toggle.others = payload;
        },
        set_FirstLoad: (state, action) => {
            let { payload } = action;
            state.firstload.current = payload;
        },
        set_CommentReload: (state, action) => {
            let { payload } = action;
            state.firstload.comment = payload;
        }
    }
})

export const {    
    isStarted_GetMyProfile, success_GetMyProfile, error_GetMyProfile,
    isStarted_GetCoInvReportee, success_GetCoInvReportee, error_GetCoInvReportee,
    isStarted_GetRPInvReportee, success_GetRPInvReportee, error_GetRPInvReportee,
    isStarted_GetPInvReportee, success_GetPInvReportee, error_GetPInvReportee,
    set_UserDetails, set_Investigator, set_CoInvestigator, set_RPInvestigator,
    set_CurrentManager, set_ToggleInv, set_ToggleOthers, set_FirstLoad, set_CommentReload
} = employeeSlice.actions
export default employeeSlice.reducer