import { createSlice } from '@reduxjs/toolkit'

export const patientSlice = createSlice({
    name: `patient`,
    initialState: {
        patientsByInv: {
            pagination: {},
            isStarted: false,
            isError: false,
            error: '',
            value: [],
        },
        patientsByInvId: {
            pagination: {},
            isStarted: false,
            isError: false,
            error: '',
            value: [],
        },
        patientVisits: {
            isStarted: false,
            isError: false,
            error: '',
            value: [],
        },
        ecgReport: {
            isStarted: false,
            isError: false,
            error: '',
            value: '',
        },
        patientDetails: {            
            value: {},
        },
    },
    reducers: {    
        set_PatientDetails: (state, action) => {
            //console.log("success_GetUserProfile: ",action)
            let { payload } = action;            
            state.patientDetails.value = payload;
        }, 
        isStarted_GetPatientsByInv: (state) => {
            state.patientsByInv.isStarted = true;
            state.patientsByInv.isError = false;
        },
        success_GetPatientsByInv: (state, action) => {
            // console.log("success_GetUserProfile: ",action)
            let { payload, pagination } = action.payload;
            state.patientsByInv.isError = false;
            state.patientsByInv.isStarted = false;
            state.patientsByInv.error = '';
            state.patientsByInv.pagination = pagination;
            state.patientsByInv.value = payload;
        },
        error_GetPatientsByInv: (state, action) => {
            let { payload } = action;
            state.patientsByInv.isError = true;
            state.patientsByInv.isStarted = false;
            state.patientsByInv.hasLoadedValue = false;
            state.patientsByInv.error = payload;
            //state.GetUserProfile.value = payload;
        },
        isStarted_GetPatientsByInvId: (state) => {
            state.patientsByInvId.isStarted = true;
            state.patientsByInvId.isError = false;
        },
        success_GetPatientsByInvId: (state, action) => {
            // console.log("success_GetUserProfile: ",action)
            let { payload, pagination } = action.payload;
            state.patientsByInvId.isError = false;
            state.patientsByInvId.isStarted = false;
            state.patientsByInvId.error = '';
            state.patientsByInvId.pagination = pagination;
            state.patientsByInvId.value = payload;
        },
        error_GetPatientsByInvId: (state, action) => {
            let { payload } = action;
            state.patientsByInvId.isError = true;
            state.patientsByInvId.isStarted = false;
            state.patientsByInvId.hasLoadedValue = false;
            state.patientsByInvId.error = payload;
            //state.GetUserProfile.value = payload;
        },
        isStarted_GetPatientVisits: (state) => {
            state.patientVisits.isStarted = true;
            state.patientVisits.isError = false;
        },
        reset_GetPatientVisits: (state) => {
            // console.log("reset_GetPatientVisits: ")
            state.patientVisits.isStarted = false;
            state.patientVisits.isError = false;
            state.patientVisits.value = [];
            state.patientVisits.error = '';
        },
        success_GetPatientVisits: (state, action) => {
            //console.log("success_patientVisits: ",action)
            let { payload } = action;
            state.patientVisits.isError = false;
            state.patientVisits.isStarted = false;
            state.patientVisits.error = '';
            state.patientVisits.hasLoadedValue = true;
            state.patientVisits.value = payload;
        },
        error_GetPatientVisits: (state, action) => {
            let { payload } = action;
            state.patientVisits.isError = true;
            state.patientVisits.isStarted = false;
            state.patientVisits.hasLoadedValue = false;
            state.patientVisits.error = payload;
        },
        isStarted_GetECGPdf: (state) => {
            // console.log("isStarted_GetECGPdf: ")
            state.ecgReport.isStarted = true;
            state.ecgReport.isError = false;
        },
        reset_GetECGPdf: (state) => {
            // console.log("reset_GetECGPdf: ")
            state.ecgReport.isStarted = false;
            state.ecgReport.isError = false;
            state.ecgReport.value = '';
            state.ecgReport.error = '';
        },
        success_GetECGPdf: (state, action) => {
            // console.log("success_ecgReport: ",action)
            let { payload } = action;
            state.ecgReport.isError = false;
            state.ecgReport.isStarted = false;
            state.ecgReport.error = '';
            state.ecgReport.value = payload;
        },
        error_GetECGPdf: (state, action) => {
            //console.log("error_GetECGPdf: ",action)
            let { payload } = action;
            state.ecgReport.isError = true;
            state.ecgReport.isStarted = false;
            state.ecgReport.error = payload;
            state.ecgReport.value = '';
        },
    }
})

export const {    
    isStarted_GetPatientsByInv, success_GetPatientsByInv, error_GetPatientsByInv,
    isStarted_GetPatientsByInvId, success_GetPatientsByInvId, error_GetPatientsByInvId,
    isStarted_GetPatientVisits, success_GetPatientVisits, error_GetPatientVisits, reset_GetPatientVisits,
    set_PatientDetails,
} = patientSlice.actions
export default patientSlice.reducer