export const APP_ROUTE = "/app/";
// export const base_url = "https://impdev2.ddns.net"
//export const base_url = "http://52.4.250.61"

export const base_url =
  process.env.REACT_APP_BASE_URL || "http://ecrfdev.wenzins.live";
// export const base_url = "http://192.168.1.228";
export const port_num = process.env.REACT_APP_API_PORT || "8080";
export const base_route = "/ecrfapi";

export const IMAGE_URL = "https://impdev2.ddns.net";

/*==================== made changes for testing ===================*/

// let URL_1 = `${base_url}:${port_num}${base_route}`;
let URL_1 = process.env.REACT_APP_BASE_URL;

/*==================== end of changes ============================*/

// let URL_1 = `${base_url}${base_route}`;

//post
//export const getPrivacyPolicyEP = "privacy-policy"

//-----------------------

export const POST_SIGN_IN = `${URL_1}/auth/signin`;
export const GET_PATIENT_FOR_INVESTIGATOR = `${URL_1}/patient/getpatientbyinvestigator/`;
export const GET_PATIENT_BY_INVESTIGATOR_ID = `${URL_1}/patient/getpatientbyinvestigator/{invId}`;
export const GET_PATIENT_VISITS = `${URL_1}/patient/{patientId}/visit/getAll`;
export const GET_PATIENT_ECG_PDF = `${URL_1}/patient/{patientId}/visit/{visitId}/dwnldEcgReport`;
export const POST_COMMENT = `${URL_1}/review/{visitId}/addedit`;
export const GET_OWN_COMMENT = `${URL_1}/review/{visitId}/getOwnComment`;
export const GET_ALL_COMMENT = `${URL_1}/review/{visitId}/getAllComment`;
export const GET_EMPL_MY_PROFILE = `${URL_1}/employee/get/{}`;
export const GET_REPORTEE_BY_MANAGER_ID = `${URL_1}/employee/get/reporteedropdown/{managerId}`;
export const RESET_PASSWORD = `${URL_1}/auth/resetPassword/{token}`;
export const INITIATE_RESET_PASSWORD = `${URL_1}/auth/resetPassword/initiate?username={userID}`;
export const SEND_COMMENTS_MAIL = `${URL_1}/review/{visitId}/sendMail`;
export const REQUEST_ACCESS = `${URL_1}/auth/requestaccess`;
export const GET_PATIENT_ECG_PDF_URL = `${URL_1}/patient/{patientId}/visit/{visitId}/EcgReportURL`;
export const GET_EK12_DROPDOWN_MASTERS = `${URL_1}/review/{type}/getEk12Masters`;

//-----------------------
