var pjson = require('../../package.json');
export const SNACKBAR_TIMEOUT = 3000;
export const VERSION = process.env.REACT_APP_VERSION || pjson.version;