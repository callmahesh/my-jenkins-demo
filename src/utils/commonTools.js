// var APP_CONFIG = require("./appconfig");
// var MongoAbstractor = require('./Dbs/DBAbstractor');
// var request = require('request');
// var dotty = require("dotty");

export function getObjInArr(searchArr, searchParam, searchKey){
    for(var i=0;i<searchArr.length;i++) {
        var iam = searchArr[i];
        if(iam[searchParam]==searchKey){
            return iam;
        }
    };
}

export function isArray(obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
}

export function isObject(obj) {
    return typeof obj ==='object' && obj!==null && typeof obj !=='array' && typeof obj !=='date' && obj.toString() !== "";
}

export function isString(word){
    if (typeof word === 'string' || word instanceof String){
        return true
    }
    else{
        return false
    }
}

export function isBoolean(str){
    // console.log(str)
    if(str=="true" || str=="TRUE" || str=="false" ||str=="true" || str=="False" || str=="FALSE" ){
        // console.log('HERE')
        return true
    }else{
        return false
    }
    
}

export function isNumber(num){
    var isNumbah = function isNumbah(num1) {
        return typeof num1 === 'number' && isFinite(num1);
     }
     var isNumberObject = function isNumberObject(num1) {
         return (Object.prototype.toString.apply(num1) === '[object Number]');
     }
     return isNumbah(num) || isNumberObject(num);
}

export function convertToUpperCase(word){
    return word.toUpperCase()
}

export function isInt(num) {
    if (isNaN(num)) {
//        console.log("HERER")
        return false
    }
    else {
//        console.log("nooo")
        return true
    }
}

export function isFunction(obj) {
    return obj && {}.toString.call(obj) === '[object Function]';
}



//https://prod.liveshare.vsengsaas.visualstudio.com/join?E035BBD3B27F844CBC5357D3D273927C0072
export function isValidEmail(em) {
    var re = /\S+@\S+\.\S+/;
    return re.test(em);
} 

export function isValidMobileNumber(number) {
    // console.log("entering isValidMobileNumber ",number)
    if(typeof(number)=="string"){
        number=parseInt(number)
    }
    // console.log("entering isValidMobileNumber ",number)
    var IndNum = /^[0]?[789]\d{9}$/; // TODO other countries
    if(IndNum.test(number)){
        return "IN";
    }
    return false;
}



// export function getPaginationInfo(params){
//     var isPaginated, isSorted, sort, sortOrder, page, limit = null;
//     isPaginated = false;
//     isSorted = false;
//     if(params.hasOwnProperty('sort')){
//         isSorted = true;
//         page = params['sort'];
//     }
//     if(params.hasOwnProperty('sortOrder')){
//         sortOrder = params['sortOrder'];
//     } else {
//         sortOrder = PG_SORT_ORDER_DEFAULT;
//     }

//     if(params.hasOwnProperty('page')){
//         isPaginated = true;
//         page = params['page'];
//     }
//     if(params.hasOwnProperty('limit')){
//         limit = params['limit'];
//     } else {
//         limit = PG_MAX_RESULTS_PER_PAGE;
//     }
//     return { isPaginated, isSorted, sort, sortOrder, page, limit };
// }

export function isEmptyObj(obj) {
    return Object.keys(obj).length === 0;
}
export function isEmptyArr(obj) {
    return obj.length === 0;
}

export function removeEmptyFromObject(obj) {
    Object.keys(obj).forEach(function(key) { //TODO FIX. empty obj and arr not removed
        if(isObject(obj[key])){
            if(isEmptyObj(obj[key])){
                delete obj[key]       
            }
        }
        if(isArray(obj[key])){
            if(isEmptyArr(obj[key])){
                delete obj[key]
            }
        }
        if (obj[key] && typeof obj[key] === 'object') removeEmptyFromObject(obj[key])
        else if (obj[key] == null) delete obj[key]
    });
    return obj;
};  


export function dateToGoodLookinFormat(date, withTime) {
    if(date==null || date==undefined){
        return "NA"
    }
    function padValue(value) {
      return (value < 10) ? "0" + value : value;
    }
    // console.log(date)
    if(isInt(date)){
        // console.log("is not Integer")
        date = new Date(date)
        // date=date.getUTCDate();¿
        // console.log(date.getUTCHours())
    }
    var month_names =["Jan","Feb","Mar",
                      "Apr","May","Jun",
                      "Jul","Aug","Sep",
                      "Oct","Nov","Dec"];
    
    var day = padValue(date.getDate());
    var month_index = date.getMonth();
    var year = date.getFullYear();
    if(!withTime){
      return "" + day + " " + month_names[month_index] + " " + year ;
    }
    // console.log("--commonTools--day-",day)
    // console.log("--commonTools--month_index-",month_index)
    // console.log("--commonTools--year-",year)
    
    var hour = date.getUTCHours()+5;
    // console.log("--commonTools--hour-",hour)
    var min = date.getUTCMinutes()+30
    if(min>=60){
        hour=hour+1
        min=min-60
    }
    var minute = padValue(min);
    // console.log("--commonTools---minute",minute)
    var ampm = "AM";
    var iHourCheck = parseInt(hour);
    // console.log("--commonTools--iHourCheck-",iHourCheck)
    if (iHourCheck > 12) {
      ampm = "PM";
      hour = iHourCheck - 12;
    }
    else if (iHourCheck === 0) {
        hour = "12";
    }
    hour = padValue(hour);
    return "" + day + " " + month_names[month_index] + " " + year + " " + hour + ":" + minute + " " + ampm;
}

// function makeRestCall(method, url, authType, authToken, params, paramType) {
//     return new Promise(async function (resolve, reject) {
//         var authorization = null;
//         var options = options = {
//             method: method,
//             url: url,
//             headers:{},
//             gzip: true,
//             followRedirects: true
//         };
//         if (authType == "Bearer") {
//             if(!authToken){
//                 return reject("bearerTokenNotProvided");
//             }
//             authorization = "Bearer " + authToken;
//             options['headers']["Authorization"] = authorization;
//         } else if(authType == "none"){
//         } else {
//             return reject("authTypeNotSupported");
//         }
//         if(paramType == "application/x-www-form-urlencoded") {
//             options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
//             options['form'] = params;
//         } else if(paramType == "application/json") {
//             options['body'] = params;
//             options['json'] = true;
//             options['headers']['Content-Type'] = 'application/json';
//         } else {
//             if (method == "GET") {
//                 options['qs'] = params;
//             } else {
//                 options['body'] = params;
//                 options['json'] = true;
//                 options['headers']['Content-Type'] = 'application/json';
//             }
//         }
//         // console.log("OPTIONSSSSSSS", options)
//         request(options, function (error, response, body) {
//             // console.log("REQQQQ", {error , body})
//             if (error) {
//                 if(response && response.hasOwnProperty('statusCode')){
//                     return reject({body: error, statusCode: response.statusCode})
//                 } else {
//                     return reject({body: error, statusCode: 500})
//                 }
//             };
//             return resolve({body, statusCode: response.statusCode});
//         });
//     })
// }



export function escapeRegExp(string) {
    return string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

export function replaceAll(str, find, replace) {
    str = str.toString();
    if(str || str === "false" || str === "null" || str === "undefined"){
       return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
    }
    return str;
}

export function cleanArray(array, intParse = false){
    var temp = [];
    for(let i of array){
        i && temp.push(i);
    }
    return temp;
}


export function cleanParams(params, intParse = false){
    for(var key in params){
        var value = params[key];
        if(value === "null"){
            params[key] = null;
        }
        if(intParse && isInt(value)){
            params[key] = parseFloat(value)
        }
    }
    return params;
}

export function getJson(response){
    var retObj = {
        isJson: true,
        error: false,
        body: null
    }
    try{
        if(isObject(response)){
            retObj['body'] = response;
        } else {
            var a = JSON.parse(response);
            retObj['body'] = a;
        }
    } catch(err){
        retObj = {
            isJson: false,
            error: true,
            body: err
        }
    }
    return retObj;
}

export function removeNulls(obj){
    var isArray = obj instanceof Array;
    for (var k in obj){
        if (obj[k]===null || obj[k]===undefined) isArray ? obj.splice(k,1) : delete obj[k];
        else if (typeof obj[k]=="object") removeNulls(obj[k]);
    }
    return obj;
}

export function replaceElementInArray(array, oldItem, newItem, firstOnly = false){
    for(var i =0 ; i<array.length; i++){
        if(array[i] === oldItem){
            array[i] = newItem;
            if(firstOnly){
                break;
            }
        }
    }
    return array;
}
export function removeItemFromArray(array, item){
    array = array.filter(i => i !== item);
    return array;
}



  
// ----------------- Common Tools -----------------------------------
// exports ={
//     getObjInArr : getObjInArr,
//     isArray : isArray,
//     isObject: isObject,
//     isString : isString,
//     isBoolean : isBoolean,
//     isNumber : isNumber,
//     convertToUpperCase : convertToUpperCase,
//     isInt : isInt,
//     isValidEmail: isValidEmail,
//     isValidMobileNumber: isValidMobileNumber,
//     // getPaginationInfo: getPaginationInfo,
//     removeEmptyFromObject: removeEmptyFromObject,
//     removeItemFromArray: removeItemFromArray,
//     dateToGoodLookinFormat : dateToGoodLookinFormat,
//     isFunction : isFunction,
//     // makeRestCall : makeRestCall,
//     // sortObjByProp: sortObjByProp,
//     replaceAll: replaceAll,
//     getJson: getJson,
//     cleanParams: cleanParams,
//     cleanArray: cleanArray,
//     removeNulls: removeNulls,
//     replaceElementInArray: replaceElementInArray,
// }
//------------------ End of common tools ----------------------------
