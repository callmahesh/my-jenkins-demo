import React, { useContext } from "react";
import { ModalContext } from "./ModalContext";
// import FooterDialog from "../../components/footer/FooterDialog";
import EditCommentDialog from '../../components/ecgReport/EditCommentDialog';
import RequestAccessDialog from '../../components/dialog/RequestAccessDialog';
import ConfirmDialog from '../../components/dialog/ConfirmDialog';
import ForgotPwdDialog from '../../components/dialog/ForgotPwdDialog';
import SendMailDialog from '../../components/dialog/SendMailDialog';

const Modals = {
  EditCommentDialog, RequestAccessDialog, ConfirmDialog, ForgotPwdDialog, SendMailDialog
};

const ModalManager = props => {
  const { currentModal, setCurrentModal } = useContext(ModalContext);

  //console.log("ModalManager", currentModal)
  const closeModal = () => setCurrentModal(null);

  if (currentModal) {
    const ModalComponent = Modals[currentModal.name];
    return <ModalComponent closeModal={closeModal} props={currentModal.props} />;
  }
  return null;
};

export default ModalManager;
